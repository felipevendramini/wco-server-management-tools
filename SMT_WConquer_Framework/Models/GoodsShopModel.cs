﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - GoodsShopModel.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

namespace SMT_WConquer_Framework.Models
{
    public sealed class GoodsShopModel
    {
        public int ShopIdentity;
        public int Type;
        public string ShopName;
        public MoneyType Moneytype = MoneyType.Money;
        public List<GoodsItemType> Items = new List<GoodsItemType>();

        public enum MoneyType
        {
            Money,
            ConquerPoints,
            ConquerPointsMono
        }

        public struct GoodsItemType
        {
            public int Type { get; set; }
            public string Name { get; set; }
        }
    }
}