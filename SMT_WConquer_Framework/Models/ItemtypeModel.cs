﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - ItemtypeModel.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace SMT_WConquer_Framework.Models
{
    public class Itemtype5017
    {
        public uint id { get; set; } // 1
        public string name { get; set; } // 2
        public uint req_profession { get; set; } // 3
        public byte req_weaponskill { get; set; } // 4
        public byte req_level { get; set; } // 5
        public byte req_sex { get; set; } // 6
        public ushort req_force { get; set; } // 7
        public ushort req_speed { get; set; } // 8
        public ushort req_health { get; set; } // 9
        public ushort req_soul { get; set; } // 10
        public uint monopoly { get; set; } // 11
        public uint price { get; set; } // 12
        public uint id_action { get; set; } // 13
        public ushort attack_max { get; set; } // 14
        public ushort attack_min { get; set; } // 15
        public short defense { get; set; } // 16
        public short dexterity { get; set; } // 17
        public short dodge { get; set; } // 18
        public short life { get; set; } // 19
        public short mana { get; set; } // 20
        public ushort amount { get; set; } // 21
        public ushort amount_limit { get; set; } // 22
        public byte ident { get; set; } // 23
        public byte gem1 { get; set; } // 24
        public byte gem2 { get; set; } // 25
        public uint magic1 { get; set; } // 26
        public byte magic2 { get; set; } // 27
        public byte magic3 { get; set; } // 28
        public uint data { get; set; } // 29
        public ushort magic_atk { get; set; } // 30
        public ushort magic_def { get; set; } // 31
        public ushort atk_range { get; set; } // 32
        public ushort atk_speed { get; set; } // 33
        public byte fray_mode { get; set; } // 34
        public byte repair_mode { get; set; } // 35
        public byte type_mask { get; set; } // 36
        public uint emoney_price { get; set; } // 37
        public string type_desc { get; set; } // 38
        public string item_desc { get; set; } // 39
    }
}