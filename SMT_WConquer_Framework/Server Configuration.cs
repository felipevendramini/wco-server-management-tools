﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - Server Configuration.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using SMT_WConquer_Framework.Structures;

#endregion

namespace SMT_WConquer_Framework
{
    public class ServerConfiguration
    {
        public static DatabaseServerConfiguration AccountServer;
        public static GameServerConfiguration SelectedServerDatabase;

        public static ConcurrentDictionary<string, GameServerConfiguration> GameServersDict = new ConcurrentDictionary<string, GameServerConfiguration>();

        public ServerConfiguration()
        {
            new ConfigurationBuilder()
                .AddJsonFile("Config.json")
                .Build()
                .Bind(this);

            //var section = builder.GetSection("AccountServer");
            //AccountServer = new DatabaseServerConfiguration
            //{
            //    Hostname = section["Hostname"],
            //    Username = section["Username"],
            //    Password = section["Password"],
            //    Database = section["Database"],
            //    Port = int.Parse(section["Port"])
            //};

            //section = builder.GetSection("GameServers");
            //int serverCount = int.Parse(section["Count"]);
            //for (int i = 0; i < serverCount; i++)
            //{
            //    var subSection = section.GetSection($"Server{i}");
            //    if (subSection == null)
            //        continue;

            //    GameServerConfiguration server = new GameServerConfiguration
            //    {
            //        Hostname = subSection["Hostname"],
            //        Username = subSection["Username"],
            //        Password = subSection["Password"],
            //        Database = subSection["Database"],
            //        Port = int.Parse(subSection["Port"]),
            //        ServerName = subSection["ServerName"]
            //    };

            //    GameServers.TryAdd(server.ServerName, server);
            //}

            GameServersDict = new (GameServers.ToDictionary(x => x.ServerName));
            SelectedServerDatabase = GameServers.FirstOrDefault();
        }

        public List<GameServerConfiguration> GameServers { get; set; }
    }

    public class DatabaseServerConfiguration
    {
        public string Hostname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public int Port { get; set; }
    }

    public class GameServerConfiguration : DatabaseServerConfiguration
    {
        public string ServerName { get; set; }

        public override string ToString()
        {
            return $"{ServerName} [{Hostname}]";
        }
    }
}