﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - Kernel.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Reflection;

#endregion

namespace SMT_WConquer_Framework
{
    public static class Kernel
    {
        public const int MAX_PARAM_LENGTH = 128;

        public static ServerConfiguration Configuration;

        public static readonly string Version;

        static Kernel()
        {
            Version = Assembly.GetExecutingAssembly().GetName().Version?.ToString() ?? "Error";
        }
    }
}