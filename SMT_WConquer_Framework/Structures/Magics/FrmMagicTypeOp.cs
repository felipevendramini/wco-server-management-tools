﻿using SMT_WConquer_Framework.Structures.Leecher;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Emit;
using System.Text;
using System.Windows.Forms;

namespace SMT_WConquer_Framework.Structures.Magics
{
    public partial class FrmMagicTypeOp : Form
    {
        public FrmMagicTypeOp()
        {
            InitializeComponent();
        }

        private void BtnConvert_Click(object sender, EventArgs e)
        {
            if (!File.Exists(txtEncryptedFile.Text))
            {
                MessageBox.Show(@$"The file '{txtEncryptedFile.Text}' does not exist.");
                return;
            }

            var buffer = File.ReadAllBytes(txtEncryptedFile.Text);
            using StreamWriter writer = new StreamWriter(txtDecryptedFile.Text, false, Encoding.ASCII);
            var cipher = new FileCipher(0x2537);
            cipher.Decrypt(ref buffer, buffer.Length);
            writer.Write(Encoding.ASCII.GetString(buffer));
            writer.Close();

            GenerateSql(txtEncryptedFile.Text, buffer);
        }

        private void GenerateSql(string path, byte[] buffer)
        {
            using StreamReader reader = new StreamReader(new MemoryStream(buffer));
            using StreamWriter writer = new StreamWriter(path.Replace(Path.GetExtension(path), ".sql"), false);

            string columns = string.Empty;
            for (int i = 1; i < 61; i++)
            {
                columns += $"`skill_{i}`,";
            }
            columns = columns.Trim(',');

            string row;
            while ((row = reader.ReadLine()) != null)
            {
                string[] magicTypeOp = row.Split(',');
                int rebirthTime = int.Parse(magicTypeOp[1]);
                int professionAgo = int.Parse(magicTypeOp[2]);
                int professionNow = int.Parse(magicTypeOp[3]);
                int magicTypeOperation = int.Parse(magicTypeOp[4]);

                List<string> magicIds = new();
                for (int i = 5; i < 65; i++)
                {
                    if (magicTypeOp.Length < i)
                    {
                        magicIds.Add("0");
                        continue;
                    }

                    magicIds.Add(magicTypeOp[i]);
                }

                string values = $"{rebirthTime},{professionAgo},{professionNow},{magicTypeOperation},{string.Join(",", magicIds.ToArray())}";
                string query = $"INSERT INTO `cq_magictypeop` (`rebirth_time`,`profession_ago`,`profession_now`,`magictype_op`,{columns}) VALUES ({values});";
                writer.WriteLine(query);
            }
            writer.Close();
        }

        private void btnPickEncrypted_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                AddExtension = false,
                InitialDirectory = Environment.CurrentDirectory,
                Multiselect = false
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            txtEncryptedFile.Text = open.FileName;
            txtDecryptedFile.Text = open.FileName.Replace(Path.GetExtension(open.FileName), ".txt");
        }

        private void btnPickDecrypted_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                AddExtension = false,
                InitialDirectory = Environment.CurrentDirectory,
                Multiselect = false
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            txtDecryptedFile.Text = open.FileName;
            txtEncryptedFile.Text = open.FileName.Replace(Path.GetExtension(open.FileName), ".dat");
        }
    }
}
