﻿namespace SMT_WConquer_Framework.Structures.Magics
{
    partial class FrmMagicTypeOp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnConvert = new System.Windows.Forms.Button();
            this.btnPickDecrypted = new System.Windows.Forms.Button();
            this.btnPickEncrypted = new System.Windows.Forms.Button();
            this.txtDecryptedFile = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEncryptedFile = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnConvert
            // 
            this.BtnConvert.Location = new System.Drawing.Point(12, 58);
            this.BtnConvert.Name = "BtnConvert";
            this.BtnConvert.Size = new System.Drawing.Size(122, 48);
            this.BtnConvert.TabIndex = 0;
            this.BtnConvert.Text = "ConvertToSql";
            this.BtnConvert.UseVisualStyleBackColor = true;
            this.BtnConvert.Click += new System.EventHandler(this.BtnConvert_Click);
            // 
            // btnPickDecrypted
            // 
            this.btnPickDecrypted.Location = new System.Drawing.Point(420, 30);
            this.btnPickDecrypted.Name = "btnPickDecrypted";
            this.btnPickDecrypted.Size = new System.Drawing.Size(75, 23);
            this.btnPickDecrypted.TabIndex = 7;
            this.btnPickDecrypted.Text = "Pick";
            this.btnPickDecrypted.UseVisualStyleBackColor = true;
            this.btnPickDecrypted.Click += new System.EventHandler(this.btnPickDecrypted_Click);
            // 
            // btnPickEncrypted
            // 
            this.btnPickEncrypted.Location = new System.Drawing.Point(420, 4);
            this.btnPickEncrypted.Name = "btnPickEncrypted";
            this.btnPickEncrypted.Size = new System.Drawing.Size(75, 23);
            this.btnPickEncrypted.TabIndex = 8;
            this.btnPickEncrypted.Text = "Pick";
            this.btnPickEncrypted.UseVisualStyleBackColor = true;
            this.btnPickEncrypted.Click += new System.EventHandler(this.btnPickEncrypted_Click);
            // 
            // txtDecryptedFile
            // 
            this.txtDecryptedFile.Enabled = false;
            this.txtDecryptedFile.Location = new System.Drawing.Point(118, 32);
            this.txtDecryptedFile.Name = "txtDecryptedFile";
            this.txtDecryptedFile.Size = new System.Drawing.Size(296, 20);
            this.txtDecryptedFile.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Decrypted File";
            // 
            // txtEncryptedFile
            // 
            this.txtEncryptedFile.Enabled = false;
            this.txtEncryptedFile.Location = new System.Drawing.Point(118, 6);
            this.txtEncryptedFile.Name = "txtEncryptedFile";
            this.txtEncryptedFile.Size = new System.Drawing.Size(296, 20);
            this.txtEncryptedFile.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Encrypted File";
            // 
            // FrmMagicTypeOp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnPickDecrypted);
            this.Controls.Add(this.btnPickEncrypted);
            this.Controls.Add(this.txtDecryptedFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEncryptedFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnConvert);
            this.Name = "FrmMagicTypeOp";
            this.Text = "FrmMagicTypeOp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnConvert;
        private System.Windows.Forms.Button btnPickDecrypted;
        private System.Windows.Forms.Button btnPickEncrypted;
        private System.Windows.Forms.TextBox txtDecryptedFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEncryptedFile;
        private System.Windows.Forms.Label label1;
    }
}