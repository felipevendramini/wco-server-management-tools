﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - System User.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using SMT_WConquer_Framework.Database.Entities;

#endregion

namespace SMT_WConquer_Framework.Structures
{
    public class SystemUser
    {
        private readonly DbAccount m_dbAccount;

        public SystemUser(DbAccount account)
        {
            if (account == null)
                throw new NullReferenceException("Account for signed user cannot be null.");

            m_dbAccount = account;
        }

        public uint Identity => m_dbAccount?.Id ?? 0;

        public string Username => m_dbAccount?.Username;
    }
}