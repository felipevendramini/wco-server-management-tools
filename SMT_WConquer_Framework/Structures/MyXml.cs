﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - MyXml.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.IO;
using System.Text;
using System.Xml;

#endregion

namespace SMT_WConquer_Framework.Structures
{
    public sealed class MyXml
    {
        private string m_path = "";
        private XmlDocument m_xml = new XmlDocument();

        public MyXml(string path, bool autoSave = false)
        {
            if (!File.Exists(path))
            {
                XmlTextWriter writer = new XmlTextWriter(path, Encoding.UTF8)
                {
#if DEBUG
                    Formatting = Formatting.Indented
#else
                    Formatting = Formatting.None
#endif
                };
                writer.WriteStartDocument();
                writer.WriteStartElement("Nodes");
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            AutoSave = autoSave;

            m_path = path;
            m_xml.Load(path);
        }

        public bool AutoSave { get; set; } = false;

        public void AddNewNode(object value, string idNode, string node, params string[] xpath)
        {
            if (!CheckNodeExists(xpath))
            {
                CreateXPath(xpath);
            }

            string query = "";
            if (string.IsNullOrEmpty(idNode))
                query = TransformXPath(xpath) + $"/{node}";
            else
            {
                string[] tempxPath = new string[xpath.Length + 1];
                for (int i = 0; i < xpath.Length; i++)
                    tempxPath[i] = xpath[i];
                tempxPath[tempxPath.Length - 1] = $"{node}[@id='{idNode}']";
                //if (CheckNodeExists(tempxPath))
                    query = TransformXPath(tempxPath);
                //else
                    //query = TransformXPath(xpath) + $"/{node}";
            }

            if (CheckNodeExists(query))
            {
                if (!string.IsNullOrEmpty(node))
                    node = $"{node}[@id='{idNode}']";
                ChangeValue(value.ToString(), TransformXPath(xpath), node);
                return;
            }

            XmlNode appendTo = GetNode(xpath);
            XmlNode newNode = m_xml.CreateNode(XmlNodeType.Element, node, "");
            newNode.InnerText = value.ToString();
            if (!string.IsNullOrEmpty(idNode))
            {
                XmlAttribute attrib = m_xml.CreateAttribute("id");
                attrib.Value = idNode;

                // ReSharper disable once PossibleNullReferenceException
                newNode.Attributes.Append(attrib);
            }

            appendTo.AppendChild(newNode);
            if (AutoSave)
                m_xml.Save(m_path);
        }

        public void CreateXPath(params string[] xpath)
        {
            XmlNode parent = null;
            string tempXPath = string.Empty;
            foreach (var path in xpath)
            {
                tempXPath = $"/{path}";
                if (!CheckNodeExists(tempXPath) && parent == null)
                    m_xml.AppendChild(parent = m_xml.CreateElement(path));
                if (!CheckNodeExists(tempXPath) && parent != null)
                    parent.AppendChild(parent = m_xml.CreateElement(path));
                else
                    parent = GetNode(tempXPath);
            }
        }

        public void DeleteNode(params string[] xpath)
        {
            DeleteNode(m_xml.SelectSingleNode(TransformXPath(xpath)));
        }

        private void DeleteNode(XmlNode node)
        {
            if (node == null)
                return;

            XmlNode parent = node.ParentNode;
            parent?.RemoveChild(node);
            if (AutoSave)
                m_xml.Save(m_path);
        }

        public XmlNode GetNode(params string[] xpath)
        {
            return m_xml.SelectSingleNode(TransformXPath(xpath));
        }

        public bool ChangeValue(string newValue, params string[] xpath)
        {
            try
            {
                m_xml.SelectSingleNode(TransformXPath(xpath)).InnerText = newValue;
                if (AutoSave)
                    m_xml.Save(m_path);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string GetValue(params string[] xpath)
        {
            if (!CheckNodeExists(xpath))
                return "";
            return GetNode(xpath)?.InnerText ?? "";
        }

        public XmlNodeList GetAllNodes(params string[] xpath)
        {
            if (!CheckNodeExists(xpath))
                return null;
            return GetNode(xpath).ChildNodes;
        }

        public bool CheckNodeExists(params string[] xpath)
        {
            return m_xml.SelectSingleNode(TransformXPath(xpath)) != null;
        }

        private string TransformXPath(params string[] xpath)
        {
            string path = "";
            foreach (var str in xpath)
                path += $"/{str}";
            return path;
        }

        public void Save()
        {
            m_xml.Save(m_path);
        }
    }
}