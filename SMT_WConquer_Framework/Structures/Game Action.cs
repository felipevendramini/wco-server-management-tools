﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - Game Action.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.ComponentModel;
using System.Linq;

namespace SMT_WConquer_Framework.Structures
{
    public static class GameAction
    {
        public static string[] SplitParam(string param, int count = 0)
        {
            return count > 0 ? param.Split(new[] { ' ' }, count, StringSplitOptions.RemoveEmptyEntries) : param.Split(' ');
        }

        public static string GetParenthesys(string szParam)
        {
            int varIdx = szParam.IndexOf("(", StringComparison.CurrentCulture) + 1;
            int endIdx = szParam.IndexOf(")", StringComparison.CurrentCulture);
            return szParam.Substring(varIdx, endIdx - varIdx);
        }

        public static byte VarId(string szParam)
        {
            int varIdx = szParam.IndexOf("(", StringComparison.CurrentCulture) + 1;
            return byte.Parse(szParam.Substring(varIdx, 1));
        }

        public static string GetDescription(TaskActionType type)
        {
            var enumType = typeof(TaskActionType);
            var memberInfos = enumType.GetMember(type.ToString());
            var enumValueMemberInfo = memberInfos.FirstOrDefault(m => m.DeclaringType == enumType);
            var valueAttributes = enumValueMemberInfo?.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (valueAttributes != null)
            {
                return  ((DescriptionAttribute)valueAttributes[0]).Description;
            }
            return "No usage description defined.";
        }

        public enum TaskActionType
        {
            // System
            [Description("Identity for the start of the system action types.")]
            ActionSysFirst = 100,
            [Description("Only param: Dialog for NPCs. This type gives you the fields to add what the NPC will say and the possible replies.")]
            ActionMenutext = 101,
            [Description("Only param: Clickable NPC Option for the user to choose. The param can have 2 params separated by blank spaces," +
                         " and if you need blank spaces in your option use the character `~`. Param is: 'optiontext taskid'")]
            ActionMenulink = 102,
            [Description("Only param: This is a field for the user to enter data. The param can have 3 params separated by blank spaces," +
                         " and if you need blank spaces in your description use the character `~`. Param is: 'length taskid description'")]
            ActionMenuedit = 103,
            [Description("Only param: This will show an image in the dialog. Param is: 'width height faceid'")]
            ActionMenupic = 104,
            ActionMenuMessageBox = 105,
            [Description("No use")]
            ActionMenubutton = 110,
            [Description("No use")]
            ActionMenulistpart = 111,
            [Description("This action sends a signal for the client representing the end of the dialog. No Data or Param is processed.")]
            ActionMenucreate = 120,
            [Description("Only param: Goes to Next action if the rate is successful or to Fail if not. Param is: 'chance limit'.\n" +
                         "Example: 1 10 (Equals that 1 in 10 tries will be successful)")]
            ActionRand = 121,
            [Description("Only param: Selects one task inserted in the param separated by spaces. Param is: 'task0 task1 task2...task7'")]
            ActionRandaction = 122,
            [Description("Data: time range type.\n" +
                         "\t1 -> Param: Year-Month-Day Hour:Minute Year-Month-Day Hour:Minute" +
                         "\t2 -> Param: DayOfMonth Hour:Minute DayOfMonth Hour:Minute" +
                         "\t3 -> Param: DayOfWeek Hour:Minute DayOfWeek Hour:Minute" +
                         "\t4 -> Param: Hour:Minute Hour:Minute" +
                         "\t5 -> Param: Minute Minute")]
            ActionChktime = 123,
            [Description("Unknown at the moment.")]
            ActionPostcmd = 124,
            [Description("Broadcasts message to the entire channel.\n" +
                         "Data is the channel ID\n" +
                         "Param is the message. Don't need to be separated by `~`.")]
            ActionBrocastmsg = 125,
            [Description("Unknown at the moment.")]
            ActionMessagebox = 126,
            [Description("Execute a query in the game database. `DELETE` and `UPDATE` MUST have a WHERE and LIMIT clause or won't be executed. Param is the query.")]
            ActionExecutequery = 127,
            ActionSysLimit = 199,

            //NPC
            ActionNpcFirst = 200,
            ActionNpcAttr = 201,
            ActionNpcErase = 205,
            ActionNpcModify = 206,
            ActionNpcResetsynowner = 207,
            ActionNpcFindNextTable = 208,
            ActionNpcAddTable = 209,
            ActionNpcDelTable = 210,
            ActionNpcDelInvalid = 211,
            ActionNpcTableAmount = 212,
            ActionNpcSysAuction = 213,
            ActionNpcDressSynclothing = 214,
            ActionNpcTakeoffSynclothing = 215,
            ActionNpcAuctioning = 216,
            ActionNpcLimit = 299,

            // Map
            ActionMapFirst = 300,
            ActionMapMovenpc = 301,
            ActionMapMapuser = 302,
            ActionMapBrocastmsg = 303,
            ActionMapDropitem = 304,
            ActionMapSetstatus = 305,
            ActionMapAttrib = 306,
            ActionMapRegionMonster = 307,
            ActionMapChangeweather = 310,
            ActionMapChangelight = 311,
            ActionMapMapeffect = 312,
            ActionMapCreatemap = 313,
            ActionMapFireworks = 314,
            ActionMapLimit = 399,

            // Lay item
            ActionItemonlyFirst = 400,
            ActionItemRequestlaynpc = 401,
            ActionItemCountnpc = 402,
            ActionItemLaynpc = 403,
            ActionItemDelthis = 498,
            ActionItemonlyLimit = 499,

            // Item
            ActionItemFirst = 500,
            ActionItemAdd = 501,
            ActionItemDel = 502,
            ActionItemCheck = 503,
            ActionItemHole = 504,
            ActionItemRepair = 505,
            ActionItemMultidel = 506,
            ActionItemMultichk = 507,
            ActionItemLeavespace = 508,
            ActionItemUpequipment = 509,
            ActionItemEquiptest = 510,
            ActionItemEquipexist = 511,
            ActionItemEquipcolor = 512,
            ActionItemRemoveAny = 513,
            ActionItemCheckrand = 516,
            ActionItemModify = 517,
            ActionItemJarCreate = 528,
            ActionItemJarVerify = 529,
            ActionItemLimit = 599,

            // Dyn NPCs
            ActionNpconlyFirst = 600,
            ActionNpconlyCreatenewPet = 601,
            ActionNpconlyDeletePet = 602,
            ActionNpconlyMagiceffect = 603,
            ActionNpconlyMagiceffect2 = 604,
            ActionNpconlyLimit = 699,

            // Syndicate
            ActionSynFirst = 700,
            ActionSynCreate = 701,
            ActionSynDestroy = 702,
            ActionSynSetAssistant = 705,
            ActionSynClearRank = 706,
            ActionSynChangeLeader = 709,
            ActionSynAntagonize = 711,
            ActionSynClearAntagonize = 712,
            ActionSynAlly = 713,
            ActionSynClearAlly = 714,
            ActionSynAttr = 717,
            ActionSynLimit = 799,

            //Monsters
            ActionMstFirst = 800,
            ActionMstDropitem = 801,
            ActionMstMagic = 802,
            ActionMstRefinery = 803,
            ActionMstLimit = 899,

            //Family
            ActionFamilyFirst = 900,
            ActionFamilyCreate = 901,
            ActionFamilyDestroy = 902,
            ActionFamilyAttr = 917,
            ActionFamilyUplev = 918,
            ActionFamilyBpuplev = 919,
            ActionFamilyLimit = 999,

            //User
            ActionUserFirst = 1000,
            ActionUserAttr = 1001,
            ActionUserFull = 1002, // Fill the user attributes. param is the attribute name. life/mana/xp/sp
            ActionUserChgmap = 1003, // Mapid Mapx Mapy savelocation
            ActionUserRecordpoint = 1004, // Records the user location, so he can be teleported back there later.
            ActionUserHair = 1005,
            ActionUserChgmaprecord = 1006,
            ActionUserChglinkmap = 1007,
            ActionUserTransform = 1008,
            ActionUserIspure = 1009,
            ActionUserTalk = 1010,
            ActionUserMagic = 1020,
            ActionUserWeaponskill = 1021,
            ActionUserLog = 1022,
            ActionUserBonus = 1023,
            ActionUserDivorce = 1024,
            ActionUserMarriage = 1025,
            ActionUserSex = 1026,
            ActionUserEffect = 1027,
            ActionUserTaskmask = 1028,
            ActionUserMediaplay = 1029,
            ActionUserSupermanlist = 1030,
            ActionUserAddTitle = 1031,
            ActionUserRemoveTitle = 1032,
            ActionUserCreatemap = 1033,
            ActionUserEnterHome = 1034,
            ActionUserEnterMateHome = 1035,
            ActionUserChkinCard2 = 1036,
            ActionUserChkoutCard2 = 1037,
            ActionUserFlyNeighbor = 1038,
            ActionUserUnlearnMagic = 1039,
            ActionUserRebirth = 1040,
            ActionUserWebpage = 1041,
            ActionUserBbs = 1042,
            ActionUserUnlearnSkill = 1043,
            ActionUserDropMagic = 1044,
            ActionUserFixAttr = 1045,
            ActionUserOpenDialog = 1046,
            ActionUserPointAllot = 1047,
            ActionUserExpMultiply = 1048,
            ActionUserDelWpgBadge = 1049,
            ActionUserChkWpgBadge = 1050,
            ActionUserTakestudentexp = 1051,
            ActionUserWhPassword = 1052,
            ActionUserSetWhPassword = 1053,
            ActionUserOpeninterface = 1054,
            ActionUserVarCompare = 1060,
            ActionUserVarDefine = 1061,
            ActionUserVarCalc = 1064,
            ActionUserExecAction = 1071,
            ActionUserStcCompare = 1073,
            ActionUserStcOpe = 1074,
            ActionUserTaskManager = 1080,
            ActionUserTaskOpe = 1081,
            ActionUserAttachStatus = 1082,
            ActionUserGodTime = 1083,
            ActionUserExpballExp = 1086,
            ActionUserStatusCreate = 1096,
            ActionUserStatusCheck = 1098,

            //User -> Team
            ActionTeamBroadcast = 1101,
            ActionTeamAttr = 1102,
            ActionTeamLeavespace = 1103,
            ActionTeamItemAdd = 1104,
            ActionTeamItemDel = 1105,
            ActionTeamItemCheck = 1106,
            ActionTeamChgmap = 1107,
            ActionTeamChkIsleader = 1501,

            // User -> General
            ActionGeneralLottery = 1508,

            ActionUserLimit = 1999,

            //Events
            ActionEventFirst = 2000,
            ActionEventSetstatus = 2001,
            ActionEventDelnpcGenid = 2002,
            ActionEventCompare = 2003,
            ActionEventCompareUnsigned = 2004,
            ActionEventChangeweather = 2005,
            ActionEventCreatepet = 2006,
            ActionEventCreatenewNpc = 2007,
            ActionEventCountmonster = 2008,
            ActionEventDeletemonster = 2009,
            ActionEventBbs = 2010,
            ActionEventErase = 2011,
            ActionEventTeleport = 2012,
            ActionEventMassaction = 2013,
            ActionEventLimit = 2099,

            //Traps
            ActionTrapFirst = 2100,
            ActionTrapCreate = 2101,
            ActionTrapErase = 2102,
            ActionTrapCount = 2103,
            ActionTrapAttr = 2104,
            ActionTrapLimit = 2199,

            // Detained Item
            ActionDetainFirst = 2200,
            ActionDetainDialog = 2205,
            ActionDetainLimit = 2299,

            //Wanted
            ActionWantedFirst = 3000,
            ActionWantedNext = 3001,
            ActionWantedName = 3002,
            ActionWantedBonuty = 3003,
            ActionWantedNew = 3004,
            ActionWantedOrder = 3005,
            ActionWantedCancel = 3006,
            ActionWantedModifyid = 3007,
            ActionWantedSuperadd = 3008,
            ActionPolicewantedNext = 3010,
            ActionPolicewantedOrder = 3011,
            ActionPolicewantedCheck = 3012,
            ActionWantedLimit = 3099,

            //Magic
            ActionMagicFirst = 4000,
            ActionMagicAttachstatus = 4001,
            ActionMagicAttack = 4002,
            ActionMagicLimit = 4099
        }

        public enum TalkChannel : ushort
        {
            Talk = 2000,
            Whisper,
            Action,
            Team,
            Guild,
            Spouse = 2006,
            System,
            Yell,
            Friend,
            Center = 2011,
            TopLeft,
            Ghost,
            Service,
            Tip,
            World = 2021,
            Register = 2100,
            Login,
            Shop,
            Vendor = 2104,
            Website,
            GuildWarRight1 = 2108,
            GuildWarRight2,
            Offline,
            Announce,
            TradeBoard = 2201,
            FriendBoard,
            TeamBoard,
            GuildBoard,
            OthersBoard,
            Broadcast = 2500,
            Monster = 2600
        }
    }
}