﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmActionLeecher.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Primitives;
using SMT_WConquer_Framework.Database;
using SMT_WConquer_Framework.Database.Entities;
using SMT_WConquer_Framework.MyControls;

#endregion

namespace SMT_WConquer_Framework.Structures.Leecher
{
    public partial class FrmActionLeecher : MyForm
    {
        private enum LoadDataFrom
        {
            Npc,
            Monster,
            Item
        }

        private Dictionary<string, string> CnRes = new Dictionary<string, string>();
        private List<GameServerConfiguration> m_listConfig = new List<GameServerConfiguration>();

        private List<DbAction> m_selectedActions = new List<DbAction>();
        private List<DbTask> m_selectedTasks = new List<DbTask>();

        private Dictionary<uint, DbAction> m_actions = new Dictionary<uint, DbAction>();
        private Dictionary<uint, DbTask> m_tasks = new Dictionary<uint, DbTask>();
        private List<NpcStruct> m_npcs = new List<NpcStruct>();
        private Dictionary<uint, MapStruct> m_maps = new Dictionary<uint, MapStruct>();
        private List<ItemTypeStruct> m_itemTypes = new List<ItemTypeStruct>();
        private List<MonsterStruct> m_monsters = new List<MonsterStruct>();

        private LoadDataFrom mLoadDataFrom = LoadDataFrom.Npc;

        public FrmActionLeecher()
        {
            InitializeComponent();
            IsLoginRequired = true;
            AllowMultiple = false;
        }

        private async void FrmActionLeecher_Load(object sender, EventArgs e)
        {
            numInitAction.Maximum = int.MaxValue;
            numInitTask.Maximum = int.MaxValue;

#if DEBUG
            TxtCnRes.Text = @"D:\World Conquer\Scripts\Server_Cn_Res.ini";
            UpdateCnRes(TxtCnRes.Text);
#endif

            await LoadActionsAsync();
        }

        private async Task LoadActionsAsync()
        {
            m_actions.Clear();
            m_tasks.Clear();
            m_npcs.Clear();
            m_maps.Clear();
            m_itemTypes.Clear();
            m_monsters.Clear();
            lbxServerNPCs.Items.Clear();

            var server = ServerConfiguration.SelectedServerDatabase;

            var actions = await DbAction.GetAsync(server);
            foreach (var action in actions)
            {
                if (!m_actions.ContainsKey(action.Identity))
                    m_actions.Add(action.Identity, action);
            }

            UpdateActionsNewMethod();

            foreach (var task in await DbTask.GetAsync(server))
            {
                if (!m_tasks.ContainsKey(task.Id))
                    m_tasks.Add(task.Id, task);
            }

            if (await DatabaseContext.TableExistsAsync("cq_map", server))
            {
                foreach (DataRow row in (await BaseRepository.SelectAsync("SELECT id, `name` FROM cq_map", server)).Rows)
                {
                    uint idMap = uint.Parse(row["id"].ToString());
                    m_maps.Add(idMap, new MapStruct
                    {
                        Identity = idMap,
                        Name = row["name"].ToString()
                    });
                }
            }

            if (await DatabaseContext.TableExistsAsync("cq_itemtype", server))
            {
                btnFromItem.Enabled = true;

                foreach (DataRow row in (await BaseRepository.SelectAsync("SELECT id, name, id_action FROM cq_itemtype WHERE id_action != 0 ORDER BY 1 ASC;", server)).Rows)
                {
                    uint idItem = uint.Parse(row["id"].ToString());
                    string name = row["name"].ToString();
                    uint action = uint.Parse(row["id_action"].ToString());

                    m_itemTypes.Add(new ItemTypeStruct
                    {
                        Identity = idItem,
                        Name = name,
                        Action = action
                    });
                }
            }

            if (await DatabaseContext.TableExistsAsync("cq_monstertype", server))
            {
                btnFromMonster.Enabled = true;

                foreach (DataRow row in (await BaseRepository.SelectAsync("SELECT id, name, action FROM cq_monstertype WHERE action != 0 ORDER BY 1 ASC;", server)).Rows)
                {
                    uint idMonster = uint.Parse(row["id"].ToString());
                    string name = row["name"].ToString();
                    uint action = uint.Parse(row["action"].ToString());

                    m_monsters.Add(new MonsterStruct
                    {
                        Identity = idMonster,
                        Name = name,
                        Action = action
                    });
                }
            }

            string npcQuery = string.Empty;
            bool npcExists = await DatabaseContext.TableExistsAsync("cq_npc", server);
            bool dynaExists = await DatabaseContext.TableExistsAsync("cq_dynanpc", server);

            if (npcExists && dynaExists)
            {
                npcQuery = "SELECT * FROM (SELECT id, `name`, mapid, task0, task1, task2, task3, task4, task5, task6, task7, 'npc' npctype FROM cq_npc" +
                    " UNION ALL SELECT id, `name`, mapid, task0, task1, task2, task3, task4, task5, task6, task7, 'dyna' npctype FROM cq_dynanpc) t ORDER BY t.mapid ASC, t.id ASC";
            }
            else if (npcExists)
            {
                npcQuery = "SELECT id, `name`, mapid, task0, task1, task2, task3, task4, task5, task6, task7, 'npc' npctype FROM cq_npc ORDER BY mapid ASC, id ASC";
            }
            else if (dynaExists)
            {
                npcQuery = "SELECT id, `name`, mapid, task0, task1, task2, task3, task4, task5, task6, task7, 'dyna' npctype FROM cq_dynanpc ORDER BY mapid ASC, id ASC";
            }

            if (!string.IsNullOrEmpty(npcQuery))
            {
                btnFromNpc.Enabled = true;
                DataTable data = await BaseRepository.SelectAsync(npcQuery, server);
                foreach (DataRow row in data.Rows)
                {
                    uint[] tasks = new uint[8];
                    for (int i = 0; i < tasks.Length; i++)
                        tasks[i] = uint.Parse(row[$"task{i}"].ToString());

                    uint idMap = uint.Parse(row["mapid"].ToString());
                    string mapName = "Unknown";
                    if (m_maps.TryGetValue(idMap, out var map))
                        mapName = map.Name;

                    NpcStruct npc = new NpcStruct
                    {
                        Name = $"[{row["npctype"].ToString().ToUpper()}]{row["name"]}",
                        Identity = uint.Parse(row["id"].ToString()),
                        MapIdentity = idMap,
                        Task = tasks,
                        MapName = mapName
                    };

                    if (npc.Task.Count(x => x > 0) > 0)
                    {
                        m_npcs.Add(npc);
                        //lbxServerNPCs.Items.Add($"{npc.Identity:000000} - {npc.Name} [{mapName}]");
                    }
                }
            }

            btnFromNpc_Click(null, null);
            lbxServerNPCs.SelectedIndex = -1;
        }

        private void UpdateActionsNewMethod()
        {
            foreach (var action in  m_actions.Values)
            {
                string tempParam = action.Param;
                string stringId = "";

                if (tempParam.Contains("<") && tempParam.Contains(">") && tempParam.Contains("@@"))
                {
                    int retryNum = 0;
                    while (tempParam.Contains("<") && tempParam.Contains(">") && tempParam.Contains("@@"))
                    {
                        int openIdx = tempParam.IndexOf("<") + 1;
                        int closeIdx = tempParam.IndexOf(">");
                        stringId = tempParam.Substring(openIdx, closeIdx - openIdx).Replace("@@", "");
                        if (CnRes.TryGetValue(stringId, out var stringValue))
                        {
                            tempParam = action.Param.Replace($"<{stringId}@@>", stringValue);
                            action.Param = tempParam;
                            stringId = "";
                        }
                        else
                        {
                            tempParam = action.Param.Replace($"<{stringId}@@>", "");
                            action.Param = tempParam;
                        }

                        if (retryNum++ >= 32)
                        {
                            break;
                        }
                    }
                }
                else if (tempParam.Contains("@@"))
                {
                    List<string> splitParams = new List<string>();
                    string currentString = string.Empty;
                    foreach (var b in tempParam)
                    {
                        if (char.IsWhiteSpace(b))
                        {
                            splitParams.Add(currentString);
                            currentString = string.Empty;
                            continue;
                        }
                        else
                        {
                            currentString += b;
                        }
                    }

                    if (!string.IsNullOrEmpty(currentString))
                    {
                        splitParams.Add(currentString);
                        currentString = string.Empty;
                    }

                    foreach (var param in splitParams.Where(x => x.Contains("@@")))
                    {
                        string[] splitResource = param.Split(new string[] { "@@" }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                        stringId = splitResource[0];
                        CnRes.TryGetValue(stringId, out var stringValue);
                        tempParam = action.Param.Replace($"{stringId}@@", stringValue);

                        for (int i = 1; i < splitResource.Length; i++)
                        {
                            tempParam = tempParam.ReplaceFirst("%s", $"%replace{i}");
                        }

                        for (int i = 1; i < splitResource.Length; i++)
                        {
                            tempParam = tempParam.ReplaceFirst($"%replace{i}", splitResource[i]);
                            tempParam = tempParam.ReplaceFirst($"{splitResource[i]}@@", string.Empty);
                        }

                        action.Param = tempParam;
                        stringId = string.Empty;
                    }
                }
            }

            foreach (var action in m_actions.Values)
            {
                string tempParam = action.Param;
                string stringId = "";
                bool started = false;
                bool finished = false;

                for (int i = 0; i < tempParam.Length; i++)
                {
                    if (tempParam[i] == '@')
                    {
                        if (string.IsNullOrEmpty(stringId))
                        {
                            // start
                            started = true;
                        }
                        else
                        {
                            // end
                            finished = true;
                        }
                    }
                    else
                    {
                        if (started && !finished)
                            stringId += tempParam[i];
                    }

                    if (finished && CnRes.TryGetValue(stringId, out string stringValue))
                    {
                        action.Param = action.Param.Replace($"@{stringId}@", stringValue);
                        started = false;
                        finished = false;
                        stringId = "";
                    }
                    else if (finished)
                    {
                        action.Param = "";
                    }
                }
            }
        }
        
        public override Task OnTargetDatabaseChangeAsync(DatabaseServerConfiguration config)
        {
            btnSave.Enabled = false;
            return LoadActionsAsync();
        }

        private void numInitAction_ValueChanged(object sender, EventArgs e)
        {
            if (numInitAction.Value != 0)
            {
                numInitTask.Value = 0;
                lbxTaskList.Items.Clear();
                lbxServerNPCs.SelectedIndex = -1;
            }
        }

        private void numInitTask_ValueChanged(object sender, EventArgs e)
        {
            if (numInitTask.Value != 0)
            {
                numInitAction.Value = 0;
                lbxServerNPCs.SelectedIndex = -1;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dgvResult.Rows.Clear();
            m_selectedActions.Clear();
            m_selectedTasks.Clear();

            DbAction initial = null;
            DbAction initialFail = null;
            DbTask task = null;

            if (lbxTaskList.Items.Count == 0)
            {
                if (numInitAction.Value != 0 && !m_actions.TryGetValue((uint) numInitAction.Value, out initial))
                {
                    //MessageBox.Show(this, $@"Action not found.");
                    return;
                }

                if (numInitTask.Value != 0 && !m_tasks.TryGetValue((uint) numInitTask.Value, out task))
                {
                    //MessageBox.Show(this, $@"Task not found.");
                    return;
                }

                if (task != null)
                {
                    if (task.IdNext != 0)
                        m_actions.TryGetValue(task.IdNext, out initial);
                    if (task.IdNextfail != 0)
                        m_actions.TryGetValue(task.IdNextfail, out initialFail);
                }

                if (initial == null && initialFail == null)
                {
                    MessageBox.Show(this, $@"Action or Task not found.");
                    return;
                }

                if (task != null && !m_selectedTasks.Contains(task))
                    m_selectedTasks.Add(task);

                if (initial != null)
                    SeekEvents(initial);
                if (initialFail != null)
                    SeekEvents(initialFail);
            }
            else
            {
                for (int i = 0; i < lbxTaskList.Items.Count; i++)
                {
                    if (radioTask.Checked)
                    {
                        if (!m_tasks.TryGetValue(uint.Parse(lbxTaskList.Items[i].ToString()), out task))
                        {
                            //MessageBox.Show(this, $@"Task {lbxTaskList.Items[i]} not found.");
                            continue;
                        }

                        if (task != null)
                        {
                            if (task.IdNext != 0)
                                m_actions.TryGetValue(task.IdNext, out initial);
                            if (task.IdNextfail != 0)
                                m_actions.TryGetValue(task.IdNextfail, out initialFail);
                        }
                    }
                    else
                    {
                        if (!m_actions.TryGetValue(uint.Parse(lbxTaskList.Items[i].ToString()), out initial))
                        {
                            // MessageBox.Show(this, $@"Action {lbxTaskList.Items[i]} not found.");
                            continue;
                        }
                    }

                    if (initial == null && initialFail == null)
                    {
                        MessageBox.Show(this, $@"Action {lbxTaskList.Items[i]} or Task not found.");
                        continue;
                    }

                    if (task != null && !m_selectedTasks.Contains(task))
                        m_selectedTasks.Add(task);

                    if (initial != null)
                        SeekEvents(initial);
                    if (initialFail != null)
                        SeekEvents(initialFail);
                }
            }

            lblActionNum.Text = m_selectedActions.Count.ToString();
        }

        private void SeekEvents(DbAction current)
        {
            uint id = 0;
            DbAction action = null;
            DbTask task = null;

            if (m_selectedActions.All(x => x.Identity != current.Identity))
            {
                dgvResult.Rows.Add(new object[]
                {
                    current.Identity,
                    current.IdNext,
                    current.IdNextfail,
                    current.Type,
                    current.Data,
                    current.Param
                });

                m_selectedActions.Add(current);
            }
            else
            {
                return;
            }

            if (chkSingleAction.Checked)
            {
                return;
            }
            
            if (current.IdNext > 0 && m_actions.TryGetValue(current.IdNext, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                SeekEvents(action);
            if (current.IdNextfail > 0 && m_actions.TryGetValue(current.IdNextfail, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                SeekEvents(action);

            string[] parsed;
            switch ((GameAction.TaskActionType)current.Type)
            {
                case GameAction.TaskActionType.ActionMenulink:
                    current.Param = Regex.Replace(current.Param, @"\s+", " ").Trim();
                    parsed = current.Param.Split(' ');
                    if (parsed.Length > 1 && uint.TryParse(parsed[1], out id) && m_tasks.TryGetValue(id, out task))
                    {
                        if (task.IdNext > 0 && m_actions.TryGetValue(task.IdNext, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                            SeekEvents(action);
                        if (task.IdNextfail > 0 && m_actions.TryGetValue(task.IdNextfail, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                            SeekEvents(action);

                        if (m_selectedTasks.All(x => x.Id != task.Id))
                            m_selectedTasks.Add(task);
                    }
                    break;

                case GameAction.TaskActionType.ActionMenuedit:
                    current.Param = Regex.Replace(current.Param, @"\s+", " ").Trim();
                    parsed = current.Param.Split(' ');
                    if (parsed.Length > 2 && uint.TryParse(parsed[1], out id) && m_tasks.TryGetValue(id, out task))
                    {
                        if (task.IdNext > 0 && m_actions.TryGetValue(task.IdNext, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                            SeekEvents(action);
                        if (task.IdNextfail > 0 && m_actions.TryGetValue(task.IdNextfail, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                            SeekEvents(action);

                        if (m_selectedTasks.All(x => x.Id != task.Id))
                            m_selectedTasks.Add(task);
                    }
                    break;

                case GameAction.TaskActionType.ActionMenuMessageBox:

                    if (current.Data > 0 && m_actions.TryGetValue(current.Data, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                    {
                        SeekEvents(action);
                    }

                    break;

                case GameAction.TaskActionType.ActionRandaction:
                    current.Param = Regex.Replace(current.Param, @"\s+", " ").Trim();
                    parsed = current.Param.Split(' ');

                    foreach (var randId in parsed)
                    {
                        if (uint.TryParse(randId, out id))
                        {
                            if (id > 0 && m_actions.TryGetValue(id, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                                SeekEvents(action);
                        }
                    }
                    break;

                case GameAction.TaskActionType.ActionEventMassaction:
                case GameAction.TaskActionType.ActionUserExecAction:
                    current.Param = Regex.Replace(current.Param, @"\s+", " ").Trim();
                    parsed = current.Param.Split(' ');

                    if (parsed.Length > 2 && uint.TryParse(parsed[1], out id) && m_actions.TryGetValue(id, out action) && m_selectedActions.All(x => x.Identity != action.Identity))
                        SeekEvents(action);
                    break;
            }
        }

        private async void btnExport_Click(object sender, EventArgs e)
        {
            bool askForFileName = lbxServerNPCs.SelectedItems.Count <= 1 || (lbxServerNPCs.SelectedItems.Count > 1 && !chkDifferentFiles.Checked);
            if (askForFileName)
            {
                string fileName = "ExportQuery.sql";
                NpcStruct npc = default;
                ItemTypeStruct itemType = default;
                MonsterStruct monster = default;

                NpcStruct[] npcs = null;
                ItemTypeStruct[] itemTypes = null;
                MonsterStruct[] monsters = null;

                if (lbxServerNPCs.SelectedIndices.Count == 1)
                {
                    if (mLoadDataFrom == LoadDataFrom.Npc)
                    {
                        npc = m_npcs[lbxServerNPCs.SelectedIndex];
                        fileName = $"[{npc.Identity}]{npc.Name}.sql";
                        npcs = new NpcStruct[] { npc };
                    }
                    else if (mLoadDataFrom == LoadDataFrom.Item)
                    {
                        itemType = m_itemTypes[lbxServerNPCs.SelectedIndex];
                        fileName = $"[{itemType.Identity}]{itemType.Name}.sql";
                        itemTypes = new ItemTypeStruct[] { itemType };
                    }
                    else if (mLoadDataFrom == LoadDataFrom.Monster)
                    {
                        monster = m_monsters[lbxServerNPCs.SelectedIndex];
                        fileName = $"[{monster.Identity}]{monster.Name}.sql";
                        monsters = new MonsterStruct[] { monster };
                    }
                    else return;
                }
                else if (lbxServerNPCs.SelectedIndices.Count > 1)
                {
                    List<NpcStruct> tempNpcs = new List<NpcStruct>();
                    List<MonsterStruct> tempMonster = new List<MonsterStruct>();
                    List<ItemTypeStruct> tempItemTypes = new List<ItemTypeStruct>();

                    foreach (var selItem in lbxServerNPCs.SelectedItems)
                    {
                        uint id = uint.Parse(selItem.ToString().Split('-')[0].Trim());
                        if (mLoadDataFrom == LoadDataFrom.Npc)
                        {
                            tempNpcs.Add(m_npcs.FirstOrDefault(x => x.Identity == id));
                        }
                        else if (mLoadDataFrom == LoadDataFrom.Item)
                        {
                            tempItemTypes.Add(m_itemTypes.FirstOrDefault(x => x.Identity == id));
                        }
                        else if (mLoadDataFrom == LoadDataFrom.Monster)
                        {
                            tempMonster.Add(m_monsters.FirstOrDefault(x => x.Identity == id));
                        }
                        else continue;
                    }

                    npcs = tempNpcs.ToArray();
                    itemTypes = tempItemTypes.ToArray();
                    monsters = tempMonster.ToArray();
                }

                SaveFileDialog saveDialog = new SaveFileDialog
                {
                    AddExtension = true,
                    CheckFileExists = false,
                    CheckPathExists = true,
                    DefaultExt = ".sql",
                    RestoreDirectory = true,
                    Title = @"Export event SQL File",
                    FileName = fileName
                };

                if (saveDialog.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                await SaveToFileAsync(saveDialog.OpenFile(), saveDialog.FileName, npcs, itemTypes, monsters);
                MessageBox.Show(this, @"Success!");
                return;
            }

            FolderBrowserDialog dlg = new FolderBrowserDialog();
#if DEBUG
            dlg.SelectedPath = @"D:\World Conquer\Scripts";
#endif
            if (dlg.ShowDialog(this) != DialogResult.OK)
                return;

            List<string> selectedItems = lbxServerNPCs.SelectedItems.Cast<string>().ToList();

            string directory = dlg.SelectedPath;
            foreach (var selItem in selectedItems)
            {
                lbxTaskList.Items.Clear();
                uint id = uint.Parse(selItem.ToString().Split('-')[0].Trim());

                NpcStruct[] npcs = null;
                ItemTypeStruct[] itemTypes = null;
                MonsterStruct[] monsters = null;
                string fileName = $"{DateTime.Now:yyyyMMddHHmmssfff}TempFileName.sql";                

                if (mLoadDataFrom == LoadDataFrom.Npc)
                {
                    NpcStruct npc = m_npcs.FirstOrDefault(x => x.Identity == id);
                    npcs = new NpcStruct[] { npc };
                    fileName = $"[{npc.Identity}]{npc.Name}.sql";

                    foreach (var task in npc.Task)
                    {
                        if (task != 0)
                            lbxTaskList.Items.Add(task.ToString());
                    }
                }
                else if (mLoadDataFrom == LoadDataFrom.Item)
                {
                    ItemTypeStruct item = m_itemTypes.FirstOrDefault(x => x.Identity == id);
                    itemTypes = new ItemTypeStruct[] { item };
                    fileName = $"[{item.Identity}]{item.Name}.sql";

                    lbxTaskList.Items.Add(item.Action.ToString());
                }
                else if (mLoadDataFrom == LoadDataFrom.Monster)
                {
                    MonsterStruct monster = m_monsters.FirstOrDefault(x => x.Identity == id);
                    monsters = new MonsterStruct[] { monster };
                    fileName = $"[{monster.Identity}]{monster.Name}.sql";

                    lbxTaskList.Items.Add(monster.Action.ToString());
                }

                btnSearch_Click(sender, e);

                string fullPath = Path.Combine(directory, fileName);
                using FileStream fileStream = new FileStream(fullPath, FileMode.Create);
                await SaveToFileAsync(fileStream, fileName, npcs, itemTypes, monsters);
            }
            MessageBox.Show(this, @"Success!");
        }

        private async Task SaveToFileAsync(Stream stream, string fileName, NpcStruct[] npcs = null, ItemTypeStruct[] items = null, MonsterStruct[] monsters = null)
        {
            StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
            DateTime now = DateTime.Now;
            await writer.WriteLineAsync(@"/*==========================================================================");
            await writer.WriteLineAsync($"\tWorld Conquer Online Management Panel - Action Leecher Exportation Tool");
            await writer.WriteLineAsync($"\tDate: {now.Year:0000}-{now.Month:00}-{now.Day:00} {now.Hour:00}:{now.Minute:00}:{now.Second:00}.{now.Millisecond:000}");
            await writer.WriteLineAsync($"\tFile: {fileName}");
            await writer.WriteLineAsync($"\tStarting actions");
            await writer.WriteLineAsync(@"==========================================================================*/");
            await writer.WriteLineAsync();

            if (npcs != null)
            {
                foreach (var npc in npcs)
                {
                    if (!npc.Equals(default))
                    {
                        DataTable dt = await BaseRepository.SelectAsync($"SELECT * FROM cq_npc WHERE id='{npc.Identity}' LIMIT 1", ServerConfiguration.SelectedServerDatabase);
                        if (dt.Rows.Count > 0)
                        {
                            StringBuilder query = new StringBuilder();
                            query.Append("INSERT INTO `cq_npc` ");
                            string cols = "(", values = "(";
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                cols += $"`{dt.Columns[i].ColumnName}`,";
                                values += $"'{dt.Rows[0][i]}',";
                            }
                            cols = cols.Trim(',');
                            cols += ")";
                            values = values.Trim(',');
                            values += ")";
                            query.Append(cols);
                            query.Append(" VALUES ");
                            query.Append(values);
                            query.Append(";");
                            await writer.WriteLineAsync(query.ToString());
                        }
                    }
                }
            }

            if (items != null)
            {
                foreach (var item in items)
                {
                    if (!item.Equals(default))
                    {
                        DataTable dt = await BaseRepository.SelectAsync($"SELECT * FROM cq_itemtype WHERE id='{item.Identity}' LIMIT 1", ServerConfiguration.SelectedServerDatabase);
                        if (dt.Rows.Count > 0)
                        {
                            StringBuilder query = new StringBuilder();
                            query.Append("INSERT INTO `cq_itemtype` ");
                            string cols = "(", values = "(";
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                cols += $"`{dt.Columns[i].ColumnName}`,";
                                values += $"'{dt.Rows[0][i]}',";
                            }
                            cols = cols.Trim(',');
                            cols += ")";
                            values = values.Trim(',');
                            values += ")";
                            query.Append(cols);
                            query.Append(" VALUES ");
                            query.Append(values);
                            query.Append(";");
                            await writer.WriteLineAsync(query.ToString());
                        }
                    }
                }
            }

            if (monsters != null)
            {
                foreach (var monster in monsters)
                {
                    if (!monster.Equals(default))
                    {
                        await writer.WriteLineAsync($"UPDATE `cq_monstertype` SET `action`={monster.Action} WHERE `id`={monster.Identity} LIMIT 1;");
                    }
                }
            }

            await writer.WriteLineAsync();

            foreach (var action in m_selectedActions)
            {
                string param = action.Param
                    .Replace("'", "`")
                    .Replace("？", "?")
                    .Replace("。", ".")
                    .Replace("＠", "@")
                    .Replace("（", "(")
                    .Replace("）", ")")
                    .Replace("！", "!")
                    .Replace("，", ",")
                    .Replace("▔", " ");

                string query;
                if ((action.Identity >= 610000 && action.Identity <= 610255))
                {
                    query = $"INSERT IGNORE INTO `cq_action` VALUES ({action.Identity},{action.IdNext},{action.IdNextfail}," +
                               $"{action.Type},{action.Data},'{param}');";
                }
                else if (chkReplace.Checked)
                {
                    query = $"REPLACE INTO `cq_action` VALUES ({action.Identity},{action.IdNext},{action.IdNextfail}," +
                               $"{action.Type},{action.Data},'{param}');";
                }
                else
                {
                    query = $"INSERT INTO `cq_action` VALUES ({action.Identity},{action.IdNext},{action.IdNextfail}," +
                               $"{action.Type},{action.Data},'{param}');";
                }

                await writer.WriteLineAsync(query);
            }

            await writer.WriteLineAsync();
            await writer.WriteLineAsync(@"/*==========================================================================");
            await writer.WriteLineAsync($"\tExporting Tasks");
            await writer.WriteLineAsync(@"==========================================================================*/");
            await writer.WriteLineAsync();

            foreach (var task in m_selectedTasks)
            {
                if (task == null)
                    continue;
                string query = $"INSERT IGNORE INTO `cq_task` VALUES ({task.Id},{task.IdNext},{task.IdNextfail},'{task.Itemname1}','{task.Itemname2}'," +
                                $"{task.Money},{task.Profession},{task.Sex},{task.MinPk},{task.MaxPk},{task.Team},{task.Metempsychosis},{task.Query},{task.Marriage}," +
                                $"{task.ClientActive},0);";
                await writer.WriteLineAsync(query);
            }

            await writer.WriteLineAsync();
            await writer.WriteLineAsync(@"/*==========================================================================");
            await writer.WriteLineAsync($"\tFinished exporting process!");
            await writer.WriteLineAsync(@"==========================================================================*/");
            await writer.WriteLineAsync();

            writer.Flush();
            writer.Close();
            writer.Dispose();
        }

        private void numInitTask_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void numInitTask_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && numInitTask.Value > 0)
            {
                if (!lbxTaskList.Items.Contains(numInitTask.Value))
                {
                    lbxTaskList.Items.Add(numInitTask.Value);
                    numInitTask.Value = 0;
                }
            }
        }

        private void lbxTaskList_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (lbxTaskList.SelectedIndex < 0)
                    return;

                lbxTaskList.Items.RemoveAt(lbxTaskList.SelectedIndex);
            }
        }

        private void lbxServerNPCs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxServerNPCs.SelectedIndex < 0 || lbxServerNPCs.SelectedIndex >= lbxServerNPCs.Items.Count)
                return;

            numInitTask.Value = 0;
            numInitAction.Value = 0;

            if (mLoadDataFrom == LoadDataFrom.Npc)
            {
                radioTask.Checked = true;

                lbxTaskList.Items.Clear();
                foreach (var selectedItem in lbxServerNPCs.SelectedItems)
                {
                    int id = int.Parse(selectedItem.ToString().Split('-')[0].Trim());
                    NpcStruct npc = m_npcs.FirstOrDefault(x => x.Identity == id);
                    if (npc.Equals(default))
                        return;

                    foreach (var t in npc.Task)
                    {
                        if (t > 0)
                            lbxTaskList.Items.Add(t.ToString());
                    }
                }
            }
            else if (mLoadDataFrom == LoadDataFrom.Item)
            {
                radioAction.Checked = true;

                lbxTaskList.Items.Clear();
                foreach (var selectedItem in lbxServerNPCs.SelectedItems)
                {
                    int id = int.Parse(selectedItem.ToString().Split('-')[0].Trim());
                    var item = m_itemTypes.FirstOrDefault(x => x.Identity == id);
                    if (item.Equals(default))
                        return;

                    lbxTaskList.Items.Add(item.Action.ToString());
                }
            }
            else if (mLoadDataFrom == LoadDataFrom.Monster)
            {
                radioAction.Checked = true;

                lbxTaskList.Items.Clear();
                foreach (var selectedItem in lbxServerNPCs.SelectedItems)
                {
                    int id = int.Parse(selectedItem.ToString().Split('-')[0].Trim());
                    var monster = m_monsters.FirstOrDefault(x => x.Identity == id);
                    if (monster.Equals(default))
                        return;

                    lbxTaskList.Items.Add(monster.Action.ToString());
                }
            }
        }

        private void btnFromNpc_Click(object sender, EventArgs e)
        {
            mLoadDataFrom = LoadDataFrom.Npc;

            lbxServerNPCs.Items.Clear();
            foreach (var npc in m_npcs.OrderBy(x => x.MapIdentity).ThenBy(x => x.Identity))
            {
                lbxServerNPCs.Items.Add($"{npc.Identity:000000} - {npc.Name} [{npc.MapName}]");
            }
        }

        private void btnFromItem_Click(object sender, EventArgs e)
        {
            mLoadDataFrom = LoadDataFrom.Item;

            lbxServerNPCs.Items.Clear();
            foreach (var item in m_itemTypes.OrderBy(x => x.Identity))
            {
                lbxServerNPCs.Items.Add($"{item.Identity:000000} - {item.Name}");
            }
        }

        private void btnQueryList_Click(object sender, EventArgs e)
        {
            if (!Clipboard.ContainsText(TextDataFormat.Text))
                return;

            lbxTaskList.Items.Clear();

            string clipboard = Clipboard.GetText();
            string[] parsed = clipboard.Split(new string[] {"\r\n", "\t"}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var par in parsed)
            {
                lbxTaskList.Items.Add(par);
            }
        }

        private void dgvResult_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= dgvResult.RowCount)
                return;

            if (e.ColumnIndex < 0 || e.ColumnIndex >= dgvResult.ColumnCount)
                return;

            btnSave.Enabled = HasActionToSave();
        }

        private bool HasActionToSave()
        {
            for (int i = 0; i < dgvResult.RowCount; i++)
            {
                var action = GetAction(i);
                if (action != null && !action.Equals(GetAction(action.Identity)))
                    return true;
            }
            return false;
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvResult.RowCount; i++)
            {
                var action = GetAction(i);
                var target = GetAction(action?.Identity ?? 0);
                if (action != null && !action.Equals(target))
                {
                    if (target == null)
                    {
                        await action.CreateAsync();
                        if (m_actions.ContainsKey(action.Identity))
                            m_actions.Add(action.Identity, action);
                        else m_actions[action.Identity] = action;
                    }
                    else 
                        await action.SaveAsync();
                }
            }
        }

        private DbAction GetAction(uint idAction)
        {
            return m_actions.Values.FirstOrDefault(x => x.Identity == idAction);
        }

        private DbAction GetAction(int rowNum)
        {
            if (rowNum < 0 || rowNum >= dgvResult.RowCount)
                return null;

            DataGridViewRow row = dgvResult.Rows[rowNum];
            if (row.IsNewRow)
                return null;

            return new DbAction
            {
                Identity = uint.Parse(row.Cells[0].Value?.ToString() ?? "0"),
                IdNext = uint.Parse(row.Cells[1].Value?.ToString() ?? "0"),
                IdNextfail = uint.Parse(row.Cells[2].Value?.ToString() ?? "0"),
                Type = uint.Parse(row.Cells[3].Value?.ToString() ?? "0"),
                Data = uint.Parse(row.Cells[4].Value?.ToString() ?? "0"),
                Param = row.Cells[5].Value?.ToString() ?? ""
            };
        }

        private struct NpcStruct
        {
            public uint Identity { get; set; }
            public string Name { get; set; }
            public uint MapIdentity { get; set; }
            public uint[] Task { get; set; }
            public string MapName { get; set; }
        }

        private struct MonsterStruct
        {
            public uint Identity { get; set; }
            public string Name { get; set; }
            public uint Action { get; set; }
        }

        private struct ItemTypeStruct
        {
            public uint Identity { get; set; }
            public string Name { get; set; }
            public uint Action { get; set; }
        }

        private struct MapStruct
        {
            public uint Identity { get; set; }
            public string Name { get; set; }
        }

        private void BtnSelectCnRes_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog(this) != DialogResult.OK)
                return;
            if (!File.Exists(dlg.FileName))
                return;
            TxtCnRes.Text = dlg.FileName;

           UpdateCnRes(dlg.FileName);
        }

        private void UpdateCnRes(string fileName)
        {
            CnRes.Clear();

            using StreamReader reader = new StreamReader(fileName);
            string row;
            while ((row = reader.ReadLine()) != null)
            {
                string[] kvp = row.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries);
                if (kvp.Length > 1)
                {
                    if (CnRes.ContainsKey(kvp[0]))
                        CnRes[kvp[0]] = kvp[1];
                    else
                        CnRes.Add(kvp[0], kvp[1]);
                }
            }
        }

        private void btnFromMonster_Click(object sender, EventArgs e)
        {
            mLoadDataFrom = LoadDataFrom.Monster;

            lbxServerNPCs.Items.Clear();
            foreach (var mob in m_monsters.OrderBy(x => x.Identity))
            {
                lbxServerNPCs.Items.Add($"{mob.Identity:000000} - {mob.Name}");
            }
        }

        private void BtnSearchString_Click(object sender, EventArgs e)
        {
            lbxServerNPCs.SelectedItems.Clear();
            lbxTaskList.Items.Clear();
            numInitAction.Value = 0;
            numInitTask.Value = 0;
            dgvResult.Rows.Clear();

            string message = TxtSearchBox.Text;
            if (string.IsNullOrEmpty(message))
            {
                var result = MessageBox.Show(this,
                    $"Doing an empty search will result in loading {m_actions.Count} actions into the Data Table, which may freeze the interface and require " +
                    $"a lot of memory. Do you want to continue?",
                    "This will freeze the application, this is a warning! :D", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Cancel)
                    return;

                if (result == DialogResult.No)
                    return;

                foreach (var action in m_actions.Values.OrderBy(x => x.Identity))
                {
                    dgvResult.Rows.Add(new object[]
                    {
                        action.Identity,
                        action.IdNext,
                        action.IdNextfail,
                        action.Type,
                        action.Data,
                        action.Param
                    });
                }
                return;
            }

            if (long.TryParse(message, out var idAction))
            {
                foreach (var action in m_actions.Values
                    .Where(x => x.Identity == idAction || x.IdNext == idAction || x.IdNextfail == idAction || x.Type == idAction
                        || x.Data == idAction || x.Param.Contains(idAction.ToString()))
                    .OrderBy(x => x.Identity))
                {
                    dgvResult.Rows.Add(new object[]
                    {
                        action.Identity,
                        action.IdNext,
                        action.IdNextfail,
                        action.Type,
                        action.Data,
                        action.Param
                    });
                }
            }
            else
            {
                foreach (var action in m_actions.Values.Where(x => x.Param.ToLower().Contains(message.ToLower())).OrderBy(x => x.Identity))
                {
                    dgvResult.Rows.Add(new object[]
                    {
                        action.Identity,
                        action.IdNext,
                        action.IdNextfail,
                        action.Type,
                        action.Data,
                        action.Param
                    });
                }
            }
        }

        private void TxtSearchBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSearchBox.Text))
                return;

            if (e.KeyChar == 13)
                BtnSearchString_Click(sender, e);
        }

        private void dgvResult_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var grid = ((DataGridView) sender);

            if (e.ColumnIndex is < 0 or >= 3)
                return;
            if (e.RowIndex < 0 && e.RowIndex >= grid.RowCount)
                return;

            string value = grid.Rows[e.RowIndex].Cells[e.ColumnIndex]?.Value.ToString() ?? "";
            if (string.IsNullOrEmpty(value)) return;

            for (int row = 0; row < grid.RowCount; row++)
            {
                for (int cell = 0; cell < 3; cell++)
                {
                    if (value.Equals(grid.Rows[row].Cells[cell].Value?.ToString(), StringComparison.CurrentCultureIgnoreCase))
                    {
                        grid.Rows[row].Cells[cell].Style.BackColor = Color.Beige;
                    }
                    else
                    {
                        grid.Rows[row].Cells[cell].Style.BackColor = Color.White;
                    }
                }
            }
        }

        private void radioDoubleArroba_CheckedChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            _ = LoadActionsAsync();
        }
    }
}