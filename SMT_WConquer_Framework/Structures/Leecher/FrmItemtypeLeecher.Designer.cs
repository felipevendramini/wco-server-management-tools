﻿namespace SMT_WConquer_Framework.Structures.Leecher
{
    partial class FrmItemtypeLeecher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtEncryptedFile = new System.Windows.Forms.TextBox();
            this.btnPickEncrypted = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDecryptedFile = new System.Windows.Forms.TextBox();
            this.btnPickDecrypted = new System.Windows.Forms.Button();
            this.radioVersion5017 = new System.Windows.Forms.RadioButton();
            this.radioVersion5035 = new System.Windows.Forms.RadioButton();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.pbProgress = new System.Windows.Forms.ProgressBar();
            this.btnConversion = new System.Windows.Forms.Button();
            this.radioVersion5078 = new System.Windows.Forms.RadioButton();
            this.btnCreateAddition = new System.Windows.Forms.Button();
            this.radioVersion5500Plus = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Encrypted File";
            // 
            // txtEncryptedFile
            // 
            this.txtEncryptedFile.Enabled = false;
            this.txtEncryptedFile.Location = new System.Drawing.Point(118, 6);
            this.txtEncryptedFile.Name = "txtEncryptedFile";
            this.txtEncryptedFile.Size = new System.Drawing.Size(296, 20);
            this.txtEncryptedFile.TabIndex = 1;
            // 
            // btnPickEncrypted
            // 
            this.btnPickEncrypted.Location = new System.Drawing.Point(420, 4);
            this.btnPickEncrypted.Name = "btnPickEncrypted";
            this.btnPickEncrypted.Size = new System.Drawing.Size(75, 23);
            this.btnPickEncrypted.TabIndex = 2;
            this.btnPickEncrypted.Text = "Pick";
            this.btnPickEncrypted.UseVisualStyleBackColor = true;
            this.btnPickEncrypted.Click += new System.EventHandler(this.btnPickEncrypted_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Decrypted File";
            // 
            // txtDecryptedFile
            // 
            this.txtDecryptedFile.Enabled = false;
            this.txtDecryptedFile.Location = new System.Drawing.Point(118, 32);
            this.txtDecryptedFile.Name = "txtDecryptedFile";
            this.txtDecryptedFile.Size = new System.Drawing.Size(296, 20);
            this.txtDecryptedFile.TabIndex = 1;
            // 
            // btnPickDecrypted
            // 
            this.btnPickDecrypted.Location = new System.Drawing.Point(420, 30);
            this.btnPickDecrypted.Name = "btnPickDecrypted";
            this.btnPickDecrypted.Size = new System.Drawing.Size(75, 23);
            this.btnPickDecrypted.TabIndex = 2;
            this.btnPickDecrypted.Text = "Pick";
            this.btnPickDecrypted.UseVisualStyleBackColor = true;
            this.btnPickDecrypted.Click += new System.EventHandler(this.btnPickDecrypted_Click);
            // 
            // radioVersion5017
            // 
            this.radioVersion5017.AutoSize = true;
            this.radioVersion5017.Checked = true;
            this.radioVersion5017.Location = new System.Drawing.Point(525, 7);
            this.radioVersion5017.Name = "radioVersion5017";
            this.radioVersion5017.Size = new System.Drawing.Size(192, 17);
            this.radioVersion5017.TabIndex = 3;
            this.radioVersion5017.TabStop = true;
            this.radioVersion5017.Text = "5017 (Memory Stream, 39 Columns)";
            this.radioVersion5017.UseVisualStyleBackColor = true;
            // 
            // radioVersion5035
            // 
            this.radioVersion5035.AutoSize = true;
            this.radioVersion5035.Location = new System.Drawing.Point(525, 30);
            this.radioVersion5035.Name = "radioVersion5035";
            this.radioVersion5035.Size = new System.Drawing.Size(169, 17);
            this.radioVersion5035.TabIndex = 3;
            this.radioVersion5035.Text = "5035 (Encryption, 39 Columns)";
            this.radioVersion5035.UseVisualStyleBackColor = true;
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(339, 59);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnEncrypt.TabIndex = 4;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(420, 59);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(75, 23);
            this.btnDecrypt.TabIndex = 4;
            this.btnDecrypt.Text = "Decrypt";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // pbProgress
            // 
            this.pbProgress.Location = new System.Drawing.Point(15, 59);
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(318, 23);
            this.pbProgress.TabIndex = 5;
            // 
            // btnConversion
            // 
            this.btnConversion.Location = new System.Drawing.Point(420, 88);
            this.btnConversion.Name = "btnConversion";
            this.btnConversion.Size = new System.Drawing.Size(75, 48);
            this.btnConversion.TabIndex = 6;
            this.btnConversion.Text = "Convert Decrypted to SQL";
            this.btnConversion.UseVisualStyleBackColor = true;
            this.btnConversion.Click += new System.EventHandler(this.btnConversion_Click);
            // 
            // radioVersion5078
            // 
            this.radioVersion5078.AutoSize = true;
            this.radioVersion5078.Location = new System.Drawing.Point(525, 53);
            this.radioVersion5078.Name = "radioVersion5078";
            this.radioVersion5078.Size = new System.Drawing.Size(169, 17);
            this.radioVersion5078.TabIndex = 3;
            this.radioVersion5078.Text = "5078 (Encryption, 40 Columns)";
            this.radioVersion5078.UseVisualStyleBackColor = true;
            // 
            // btnCreateAddition
            // 
            this.btnCreateAddition.Location = new System.Drawing.Point(339, 88);
            this.btnCreateAddition.Name = "btnCreateAddition";
            this.btnCreateAddition.Size = new System.Drawing.Size(75, 48);
            this.btnCreateAddition.TabIndex = 7;
            this.btnCreateAddition.Text = "Addition SQL";
            this.btnCreateAddition.UseVisualStyleBackColor = true;
            this.btnCreateAddition.Click += new System.EventHandler(this.btnCreateAddition_Click);
            // 
            // radioVersion5500Plus
            // 
            this.radioVersion5500Plus.AutoSize = true;
            this.radioVersion5500Plus.Location = new System.Drawing.Point(525, 76);
            this.radioVersion5500Plus.Name = "radioVersion5500Plus";
            this.radioVersion5500Plus.Size = new System.Drawing.Size(175, 17);
            this.radioVersion5500Plus.TabIndex = 3;
            this.radioVersion5500Plus.Text = "5500+ (Encryption, 40 Columns)";
            this.radioVersion5500Plus.UseVisualStyleBackColor = true;
            // 
            // FrmItemtypeLeecher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnCreateAddition);
            this.Controls.Add(this.btnConversion);
            this.Controls.Add(this.pbProgress);
            this.Controls.Add(this.btnDecrypt);
            this.Controls.Add(this.btnEncrypt);
            this.Controls.Add(this.radioVersion5500Plus);
            this.Controls.Add(this.radioVersion5078);
            this.Controls.Add(this.radioVersion5035);
            this.Controls.Add(this.radioVersion5017);
            this.Controls.Add(this.btnPickDecrypted);
            this.Controls.Add(this.btnPickEncrypted);
            this.Controls.Add(this.txtDecryptedFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEncryptedFile);
            this.Controls.Add(this.label1);
            this.Name = "FrmItemtypeLeecher";
            this.Text = "FrmItemtypeLeecher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEncryptedFile;
        private System.Windows.Forms.Button btnPickEncrypted;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDecryptedFile;
        private System.Windows.Forms.Button btnPickDecrypted;
        private System.Windows.Forms.RadioButton radioVersion5017;
        private System.Windows.Forms.RadioButton radioVersion5035;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.ProgressBar pbProgress;
        private System.Windows.Forms.Button btnConversion;
        private System.Windows.Forms.RadioButton radioVersion5078;
        private System.Windows.Forms.Button btnCreateAddition;
        private System.Windows.Forms.RadioButton radioVersion5500Plus;
    }
}