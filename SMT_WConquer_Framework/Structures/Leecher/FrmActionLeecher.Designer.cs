﻿namespace SMT_WConquer_Framework.Structures.Leecher
{
    partial class FrmActionLeecher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Next = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Param = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkSingleAction = new System.Windows.Forms.CheckBox();
            this.BtnSearchString = new System.Windows.Forms.Button();
            this.TxtSearchBox = new System.Windows.Forms.TextBox();
            this.chkDifferentFiles = new System.Windows.Forms.CheckBox();
            this.btnFromMonster = new System.Windows.Forms.Button();
            this.TxtCnRes = new System.Windows.Forms.TextBox();
            this.BtnSelectCnRes = new System.Windows.Forms.Button();
            this.chkReplace = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnQueryList = new System.Windows.Forms.Button();
            this.radioAction = new System.Windows.Forms.RadioButton();
            this.radioTask = new System.Windows.Forms.RadioButton();
            this.btnFromItem = new System.Windows.Forms.Button();
            this.btnFromNpc = new System.Windows.Forms.Button();
            this.lbxServerNPCs = new System.Windows.Forms.ListBox();
            this.lbxTaskList = new System.Windows.Forms.ListBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.lblActionNum = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.numInitTask = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numInitAction = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numInitTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInitAction)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dgvResult);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(810, 770);
            this.panel1.TabIndex = 1;
            // 
            // dgvResult
            // 
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.Next,
            this.Fail,
            this.Type,
            this.Data,
            this.Param});
            this.dgvResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvResult.Location = new System.Drawing.Point(0, 0);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.Size = new System.Drawing.Size(810, 770);
            this.dgvResult.TabIndex = 0;
            this.dgvResult.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvResult_CellClick);
            this.dgvResult.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvResult_CellEndEdit);
            // 
            // id
            // 
            this.id.Frozen = true;
            this.id.HeaderText = "Identity";
            this.id.MaxInputLength = 12;
            this.id.Name = "id";
            // 
            // Next
            // 
            this.Next.Frozen = true;
            this.Next.HeaderText = "Next";
            this.Next.MaxInputLength = 12;
            this.Next.Name = "Next";
            // 
            // Fail
            // 
            this.Fail.Frozen = true;
            this.Fail.HeaderText = "Fail";
            this.Fail.MaxInputLength = 12;
            this.Fail.Name = "Fail";
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.MaxInputLength = 12;
            this.Type.Name = "Type";
            // 
            // Data
            // 
            this.Data.HeaderText = "Data";
            this.Data.MaxInputLength = 12;
            this.Data.Name = "Data";
            // 
            // Param
            // 
            this.Param.HeaderText = "Param";
            this.Param.MaxInputLength = 255;
            this.Param.Name = "Param";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.chkSingleAction);
            this.panel2.Controls.Add(this.BtnSearchString);
            this.panel2.Controls.Add(this.TxtSearchBox);
            this.panel2.Controls.Add(this.chkDifferentFiles);
            this.panel2.Controls.Add(this.btnFromMonster);
            this.panel2.Controls.Add(this.TxtCnRes);
            this.panel2.Controls.Add(this.BtnSelectCnRes);
            this.panel2.Controls.Add(this.chkReplace);
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.btnQueryList);
            this.panel2.Controls.Add(this.radioAction);
            this.panel2.Controls.Add(this.radioTask);
            this.panel2.Controls.Add(this.btnFromItem);
            this.panel2.Controls.Add(this.btnFromNpc);
            this.panel2.Controls.Add(this.lbxServerNPCs);
            this.panel2.Controls.Add(this.lbxTaskList);
            this.panel2.Controls.Add(this.btnExport);
            this.panel2.Controls.Add(this.lblActionNum);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Controls.Add(this.numInitTask);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.numInitAction);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(828, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 770);
            this.panel2.TabIndex = 2;
            // 
            // chkSingleAction
            // 
            this.chkSingleAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSingleAction.AutoSize = true;
            this.chkSingleAction.Location = new System.Drawing.Point(17, 520);
            this.chkSingleAction.Name = "chkSingleAction";
            this.chkSingleAction.Size = new System.Drawing.Size(88, 17);
            this.chkSingleAction.TabIndex = 19;
            this.chkSingleAction.Text = "Single Action";
            this.chkSingleAction.UseVisualStyleBackColor = true;
            // 
            // BtnSearchString
            // 
            this.BtnSearchString.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSearchString.Location = new System.Drawing.Point(203, 377);
            this.BtnSearchString.Name = "BtnSearchString";
            this.BtnSearchString.Size = new System.Drawing.Size(57, 20);
            this.BtnSearchString.TabIndex = 18;
            this.BtnSearchString.Text = "Search";
            this.BtnSearchString.UseVisualStyleBackColor = true;
            this.BtnSearchString.Click += new System.EventHandler(this.BtnSearchString_Click);
            // 
            // TxtSearchBox
            // 
            this.TxtSearchBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtSearchBox.Location = new System.Drawing.Point(11, 377);
            this.TxtSearchBox.Name = "TxtSearchBox";
            this.TxtSearchBox.Size = new System.Drawing.Size(184, 20);
            this.TxtSearchBox.TabIndex = 17;
            this.TxtSearchBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSearchBox_KeyPress);
            // 
            // chkDifferentFiles
            // 
            this.chkDifferentFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDifferentFiles.AutoSize = true;
            this.chkDifferentFiles.Location = new System.Drawing.Point(149, 608);
            this.chkDifferentFiles.Name = "chkDifferentFiles";
            this.chkDifferentFiles.Size = new System.Drawing.Size(90, 17);
            this.chkDifferentFiles.TabIndex = 16;
            this.chkDifferentFiles.Text = "Different Files";
            this.chkDifferentFiles.UseVisualStyleBackColor = true;
            // 
            // btnFromMonster
            // 
            this.btnFromMonster.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFromMonster.Enabled = false;
            this.btnFromMonster.Location = new System.Drawing.Point(160, 433);
            this.btnFromMonster.Name = "btnFromMonster";
            this.btnFromMonster.Size = new System.Drawing.Size(57, 23);
            this.btnFromMonster.TabIndex = 15;
            this.btnFromMonster.Text = "Monster";
            this.btnFromMonster.UseVisualStyleBackColor = true;
            this.btnFromMonster.Click += new System.EventHandler(this.btnFromMonster_Click);
            // 
            // TxtCnRes
            // 
            this.TxtCnRes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtCnRes.Location = new System.Drawing.Point(11, 404);
            this.TxtCnRes.Name = "TxtCnRes";
            this.TxtCnRes.ReadOnly = true;
            this.TxtCnRes.Size = new System.Drawing.Size(185, 20);
            this.TxtCnRes.TabIndex = 14;
            // 
            // BtnSelectCnRes
            // 
            this.BtnSelectCnRes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSelectCnRes.Location = new System.Drawing.Point(203, 403);
            this.BtnSelectCnRes.Name = "BtnSelectCnRes";
            this.BtnSelectCnRes.Size = new System.Drawing.Size(54, 23);
            this.BtnSelectCnRes.TabIndex = 13;
            this.BtnSelectCnRes.Text = "Cn_Res";
            this.BtnSelectCnRes.UseVisualStyleBackColor = true;
            this.BtnSelectCnRes.Click += new System.EventHandler(this.BtnSelectCnRes_Click);
            // 
            // chkReplace
            // 
            this.chkReplace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkReplace.AutoSize = true;
            this.chkReplace.Location = new System.Drawing.Point(153, 700);
            this.chkReplace.Name = "chkReplace";
            this.chkReplace.Size = new System.Drawing.Size(104, 17);
            this.chkReplace.TabIndex = 12;
            this.chkReplace.Text = "REPLACE INTO";
            this.chkReplace.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(79, 723);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 44);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save DB";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnQueryList
            // 
            this.btnQueryList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnQueryList.Location = new System.Drawing.Point(12, 433);
            this.btnQueryList.Name = "btnQueryList";
            this.btnQueryList.Size = new System.Drawing.Size(60, 23);
            this.btnQueryList.TabIndex = 10;
            this.btnQueryList.Text = "Clipboard";
            this.btnQueryList.UseVisualStyleBackColor = true;
            this.btnQueryList.Click += new System.EventHandler(this.btnQueryList_Click);
            // 
            // radioAction
            // 
            this.radioAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radioAction.AutoSize = true;
            this.radioAction.Location = new System.Drawing.Point(149, 578);
            this.radioAction.Name = "radioAction";
            this.radioAction.Size = new System.Drawing.Size(74, 17);
            this.radioAction.TabIndex = 9;
            this.radioAction.Text = "Action List";
            this.radioAction.UseVisualStyleBackColor = true;
            // 
            // radioTask
            // 
            this.radioTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radioTask.AutoSize = true;
            this.radioTask.Checked = true;
            this.radioTask.Location = new System.Drawing.Point(149, 555);
            this.radioTask.Name = "radioTask";
            this.radioTask.Size = new System.Drawing.Size(68, 17);
            this.radioTask.TabIndex = 9;
            this.radioTask.TabStop = true;
            this.radioTask.Text = "Task List";
            this.radioTask.UseVisualStyleBackColor = true;
            // 
            // btnFromItem
            // 
            this.btnFromItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFromItem.Enabled = false;
            this.btnFromItem.Location = new System.Drawing.Point(75, 433);
            this.btnFromItem.Name = "btnFromItem";
            this.btnFromItem.Size = new System.Drawing.Size(39, 24);
            this.btnFromItem.TabIndex = 8;
            this.btnFromItem.Text = "Item";
            this.btnFromItem.UseVisualStyleBackColor = true;
            this.btnFromItem.Click += new System.EventHandler(this.btnFromItem_Click);
            // 
            // btnFromNpc
            // 
            this.btnFromNpc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFromNpc.Enabled = false;
            this.btnFromNpc.Location = new System.Drawing.Point(117, 432);
            this.btnFromNpc.Name = "btnFromNpc";
            this.btnFromNpc.Size = new System.Drawing.Size(37, 24);
            this.btnFromNpc.TabIndex = 8;
            this.btnFromNpc.Text = "NPC";
            this.btnFromNpc.UseVisualStyleBackColor = true;
            this.btnFromNpc.Click += new System.EventHandler(this.btnFromNpc_Click);
            // 
            // lbxServerNPCs
            // 
            this.lbxServerNPCs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxServerNPCs.FormattingEnabled = true;
            this.lbxServerNPCs.Location = new System.Drawing.Point(3, 3);
            this.lbxServerNPCs.Name = "lbxServerNPCs";
            this.lbxServerNPCs.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbxServerNPCs.Size = new System.Drawing.Size(257, 368);
            this.lbxServerNPCs.TabIndex = 7;
            this.lbxServerNPCs.SelectedIndexChanged += new System.EventHandler(this.lbxServerNPCs_SelectedIndexChanged);
            // 
            // lbxTaskList
            // 
            this.lbxTaskList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxTaskList.FormattingEnabled = true;
            this.lbxTaskList.Location = new System.Drawing.Point(17, 555);
            this.lbxTaskList.Name = "lbxTaskList";
            this.lbxTaskList.Size = new System.Drawing.Size(120, 108);
            this.lbxTaskList.TabIndex = 6;
            this.lbxTaskList.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lbxTaskList_KeyUp);
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(160, 723);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(97, 44);
            this.btnExport.TabIndex = 5;
            this.btnExport.Text = "&Export SQL";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // lblActionNum
            // 
            this.lblActionNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblActionNum.AutoSize = true;
            this.lblActionNum.Location = new System.Drawing.Point(157, 666);
            this.lblActionNum.Name = "lblActionNum";
            this.lblActionNum.Size = new System.Drawing.Size(13, 13);
            this.lblActionNum.TabIndex = 4;
            this.lblActionNum.Text = "0";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 666);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Actions Currently Loaded:";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(149, 631);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(99, 31);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "&Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // numInitTask
            // 
            this.numInitTask.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numInitTask.Location = new System.Drawing.Point(128, 488);
            this.numInitTask.Name = "numInitTask";
            this.numInitTask.Size = new System.Drawing.Size(120, 20);
            this.numInitTask.TabIndex = 1;
            this.numInitTask.ValueChanged += new System.EventHandler(this.numInitTask_ValueChanged);
            this.numInitTask.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numInitTask_KeyPress);
            this.numInitTask.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numInitTask_KeyUp);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 490);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Initial Task";
            // 
            // numInitAction
            // 
            this.numInitAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numInitAction.Location = new System.Drawing.Point(128, 462);
            this.numInitAction.Name = "numInitAction";
            this.numInitAction.Size = new System.Drawing.Size(120, 20);
            this.numInitAction.TabIndex = 1;
            this.numInitAction.ValueChanged += new System.EventHandler(this.numInitAction_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 464);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Initial Action";
            // 
            // FrmActionLeecher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 794);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FrmActionLeecher";
            this.Text = "FrmActionLeecher";
            this.Load += new System.EventHandler(this.FrmActionLeecher_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numInitTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInitAction)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown numInitTask;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numInitAction;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblActionNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ListBox lbxTaskList;
        private System.Windows.Forms.Button btnFromNpc;
        private System.Windows.Forms.ListBox lbxServerNPCs;
        private System.Windows.Forms.Button btnFromItem;
        private System.Windows.Forms.RadioButton radioAction;
        private System.Windows.Forms.RadioButton radioTask;
        private System.Windows.Forms.Button btnQueryList;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox chkReplace;
        private System.Windows.Forms.TextBox TxtCnRes;
        private System.Windows.Forms.Button BtnSelectCnRes;
        private System.Windows.Forms.Button btnFromMonster;
        private System.Windows.Forms.CheckBox chkDifferentFiles;
        private System.Windows.Forms.Button BtnSearchString;
        private System.Windows.Forms.TextBox TxtSearchBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Next;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Param;
        private System.Windows.Forms.CheckBox chkSingleAction;
    }
}