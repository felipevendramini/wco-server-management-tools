﻿namespace SMT_WConquer_Framework.Structures.Leecher
{
    partial class FrmMagictypeLeecher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnToSql = new System.Windows.Forms.Button();
            this.dgvMagictype = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagictype)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnToSql
            // 
            this.BtnToSql.Location = new System.Drawing.Point(12, 12);
            this.BtnToSql.Name = "BtnToSql";
            this.BtnToSql.Size = new System.Drawing.Size(100, 45);
            this.BtnToSql.TabIndex = 1;
            this.BtnToSql.Text = "Convert to SQL";
            this.BtnToSql.UseVisualStyleBackColor = true;
            this.BtnToSql.Click += new System.EventHandler(this.BtnToSql_Click);
            // 
            // dgvMagictype
            // 
            this.dgvMagictype.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMagictype.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMagictype.Location = new System.Drawing.Point(12, 63);
            this.dgvMagictype.Name = "dgvMagictype";
            this.dgvMagictype.Size = new System.Drawing.Size(1282, 594);
            this.dgvMagictype.TabIndex = 2;
            // 
            // FrmMagictypeLeecher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1306, 669);
            this.Controls.Add(this.dgvMagictype);
            this.Controls.Add(this.BtnToSql);
            this.Name = "FrmMagictypeLeecher";
            this.Text = "FrmMagictypeLeecher";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMagictype)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnToSql;
        private System.Windows.Forms.DataGridView dgvMagictype;
    }
}