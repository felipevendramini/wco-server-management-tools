﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SMT_WConquer_Framework.Structures.Leecher
{
    public partial class FrmMagictypeLeecher : Form
    {
        private readonly string[] columns =
        {
            "id", "type", "sort", "name", "crime", "ground", "multi", "target", "level", "use_mp", "power", "intone_speed",
            "percent", "step_secs", "range", "distance", "status", "need_prof", "need_exp", "need_time", "need_level", "use_xp",
            "weapon_subtype", "active_times", "auto_active", "floor_attr", "auto_learn", "learn_level", "drop_weapon", "use_ep",
            "weapon_hit", "use_item", "next_magic", "delay_ms", "use_item_num", "status_data0", "status_data1", "status_data2",
            "attr_type", "attr_power", "target_num", "need_ast_prof", "need_ast_prof_rank", "width", "data", "dur_time", "atk_interval",
            "coldtime", "req_uplevtime", "first_magic", "combo_delay", "combo_timeout", "magictypeex_id"
        };

        public FrmMagictypeLeecher()
        {
            InitializeComponent();

            foreach (var column in columns)
            {
                dgvMagictype.Columns.Add(column, column);
            }
        }

        private void BtnToSql_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                AddExtension = false,
#if !DEBUG
                InitialDirectory = Environment.CurrentDirectory,
#else
                InitialDirectory = @"D:\World Conquer\NewConquer\ini",
#endif
                Multiselect = false
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            dgvMagictype.Rows.Clear();

            string directory = Path.GetDirectoryName(open.FileName);
            string fileName = Path.GetFileNameWithoutExtension(open.FileName);
            using StreamWriter txtWriter = new StreamWriter(Path.Combine(directory, $"{fileName}.txt"), false);
            using StreamWriter writer = new StreamWriter(Path.Combine(directory, $"{fileName}.sql"), false);

            byte[] buffer = File.ReadAllBytes(open.FileName);
            var cipher = new FileCipher(0x2537);
            cipher.Decrypt(ref buffer, buffer.Length);
            txtWriter.Write(Encoding.ASCII.GetString(buffer));
            txtWriter.Close();
            buffer = null;

            using StreamReader streamReader = new StreamReader(Path.Combine(directory, $"{fileName}.txt"));
            string row;
            while ((row = streamReader.ReadLine()) != null)
            {
                string[] splitRow = row.Split(new string[] { "@@" }, StringSplitOptions.RemoveEmptyEntries);
                dgvMagictype.Rows.Add(splitRow);
                StringBuilder columns = new();
                StringBuilder values = new();
                for (int i = 0; i < splitRow.Length; i++)
                {
                    columns.Append($"`{this.columns[i]}`,");
                    values.Append($"'{splitRow[i]}',");
                }
                string sql = $"INSERT INTO `cq_magictype` ({columns.ToString().TrimEnd(',')}) VALUES ({values.ToString().TrimEnd(',')});";
                writer.WriteLine(sql);
            }
            writer.Close();
        }
    }
}
