﻿namespace SMT_WConquer_Framework.Structures.Actions
{
    partial class FrmActionPreviewSql
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvTablePreview = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_next = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_nextfail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Param = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSqlPreview = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.lblSaveTo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numStartWith = new System.Windows.Forms.NumericUpDown();
            this.btnDoTheMagic = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablePreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartWith)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvTablePreview);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtSqlPreview);
            this.splitContainer1.Size = new System.Drawing.Size(1135, 673);
            this.splitContainer1.SplitterDistance = 568;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvTablePreview
            // 
            this.dgvTablePreview.AllowUserToAddRows = false;
            this.dgvTablePreview.AllowUserToDeleteRows = false;
            this.dgvTablePreview.AllowUserToOrderColumns = true;
            this.dgvTablePreview.AllowUserToResizeRows = false;
            this.dgvTablePreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTablePreview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.id_next,
            this.id_nextfail,
            this.type,
            this.data,
            this.Param});
            this.dgvTablePreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTablePreview.Location = new System.Drawing.Point(0, 0);
            this.dgvTablePreview.Name = "dgvTablePreview";
            this.dgvTablePreview.Size = new System.Drawing.Size(568, 673);
            this.dgvTablePreview.TabIndex = 0;
            // 
            // id
            // 
            this.id.Frozen = true;
            this.id.HeaderText = "Identity";
            this.id.MaxInputLength = 10;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // id_next
            // 
            this.id_next.Frozen = true;
            this.id_next.HeaderText = "Next";
            this.id_next.MaxInputLength = 10;
            this.id_next.Name = "id_next";
            this.id_next.ReadOnly = true;
            // 
            // id_nextfail
            // 
            this.id_nextfail.Frozen = true;
            this.id_nextfail.HeaderText = "Fail";
            this.id_nextfail.MaxInputLength = 10;
            this.id_nextfail.Name = "id_nextfail";
            this.id_nextfail.ReadOnly = true;
            // 
            // type
            // 
            this.type.HeaderText = "Type";
            this.type.MaxInputLength = 10;
            this.type.Name = "type";
            this.type.ReadOnly = true;
            // 
            // data
            // 
            this.data.HeaderText = "Data";
            this.data.MaxInputLength = 10;
            this.data.Name = "data";
            this.data.ReadOnly = true;
            // 
            // Param
            // 
            this.Param.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
            this.Param.HeaderText = "Parameter";
            this.Param.MaxInputLength = 256;
            this.Param.Name = "Param";
            this.Param.ReadOnly = true;
            this.Param.Width = 5;
            // 
            // txtSqlPreview
            // 
            this.txtSqlPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSqlPreview.Location = new System.Drawing.Point(0, 0);
            this.txtSqlPreview.Multiline = true;
            this.txtSqlPreview.Name = "txtSqlPreview";
            this.txtSqlPreview.ReadOnly = true;
            this.txtSqlPreview.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSqlPreview.Size = new System.Drawing.Size(563, 673);
            this.txtSqlPreview.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(1048, 679);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(967, 679);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "&Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // lblSaveTo
            // 
            this.lblSaveTo.AutoSize = true;
            this.lblSaveTo.Location = new System.Drawing.Point(12, 684);
            this.lblSaveTo.Name = "lblSaveTo";
            this.lblSaveTo.Size = new System.Drawing.Size(46, 13);
            this.lblSaveTo.TabIndex = 2;
            this.lblSaveTo.Text = "Saving: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 710);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Starting Action: ";
            // 
            // numStartWith
            // 
            this.numStartWith.Location = new System.Drawing.Point(100, 708);
            this.numStartWith.Name = "numStartWith";
            this.numStartWith.Size = new System.Drawing.Size(120, 20);
            this.numStartWith.TabIndex = 4;
            this.numStartWith.ValueChanged += new System.EventHandler(this.numStartWith_ValueChanged);
            // 
            // btnDoTheMagic
            // 
            this.btnDoTheMagic.Location = new System.Drawing.Point(226, 706);
            this.btnDoTheMagic.Name = "btnDoTheMagic";
            this.btnDoTheMagic.Size = new System.Drawing.Size(101, 23);
            this.btnDoTheMagic.TabIndex = 5;
            this.btnDoTheMagic.Text = "Do The Magic";
            this.btnDoTheMagic.UseVisualStyleBackColor = true;
            this.btnDoTheMagic.Click += new System.EventHandler(this.btnDoTheMagic_Click);
            // 
            // FrmActionPreviewSql
            // 
            this.AcceptButton = this.btnExport;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1135, 732);
            this.Controls.Add(this.btnDoTheMagic);
            this.Controls.Add(this.numStartWith);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSaveTo);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FrmActionPreviewSql";
            this.Text = "FrmActionPreviewSql";
            this.Load += new System.EventHandler(this.FrmActionPreviewSql_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablePreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartWith)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvTablePreview;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_next;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_nextfail;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Param;
        private System.Windows.Forms.TextBox txtSqlPreview;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label lblSaveTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numStartWith;
        private System.Windows.Forms.Button btnDoTheMagic;
    }
}