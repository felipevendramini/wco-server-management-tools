﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmActionPreviewSql.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions
{
    public partial class FrmActionPreviewSql : Form
    {
        private bool m_generated = false;

        private int m_currentAction = 1;

        private struct GameAction
        {
            public int Id;
            public int Next;
            public int Fail;
            public Structures.GameAction.TaskActionType Type;
            public int Data;
            public string Param;
        }

        private string m_savePath;

        private List<MyActionNode> m_nodes;
        private List<GameAction> m_lActions = new List<GameAction>();
        private Queue<MyActionNode> m_nextActions = new Queue<MyActionNode>();

        public FrmActionPreviewSql(string savePath, List<MyActionNode> nodes)
        {
            InitializeComponent();

            m_savePath = savePath;
            m_nodes = new List<MyActionNode>(nodes);
        }

        private void FrmActionPreviewSql_Load(object sender, System.EventArgs e)
        {
            lblSaveTo.Text = @$"Saving to: {m_savePath}";

            numStartWith.Minimum = 1;
            numStartWith.Maximum = int.MaxValue;
        }

        private async void btnExport_Click(object sender, System.EventArgs e)
        {
            if (!m_generated)
            {
                MessageBox.Show(this, @"Please, do the magic first!");
                return;
            }

            await WriteToFileAsync();
            Close();
        }

        private Task FillDataTableAsync()
        {
            dgvTablePreview.Rows.Clear();
            foreach (var action in m_lActions.OrderBy(x => x.Id))
            {
                dgvTablePreview.Rows.Add(action.Id, action.Next, action.Fail, action.Type.ToString(), action.Data, action.Param);
            }
            return Task.CompletedTask;
        }

        private Task FillQueryAsync()
        {
            txtSqlPreview.Text = string.Empty;
            foreach (var action in m_lActions.OrderBy(x => x.Id))
            {
                txtSqlPreview.AppendText($"INSERT INTO `cq_action` VALUES ({action.Id:0000},{action.Next:0000},{action.Fail:0000},{(int) action.Type:0000},{action.Data:0000},'{action.Param.Replace("'", "\\'")}');\r\n");
            }
            return Task.CompletedTask;
        }

        private int GenerateActionsRecursively(MyActionNode current)
        {
            if (current == null)
                return 0;

            int currentId = 0;
            switch (current.ActionType)
            {
                case Structures.GameAction.TaskActionType.ActionMenutext:
                {
                    foreach (var child in current.Childrens)
                    {
                        currentId = m_currentAction++;
                        string param = child.Param;
                        if (child.Type == Structures.GameAction.TaskActionType.ActionMenulink
                            || child.Type == Structures.GameAction.TaskActionType.ActionMenuedit)
                        {
                            param = param.Replace(" ", "~");
                            if (child.Next == null)
                            {
                                param += " 0";
                            }
                            else
                            {
                                param += " %task_id";
                            }
                        }

                        int idNext = 0;
                        if (child.Type != Structures.GameAction.TaskActionType.ActionMenucreate)
                        {
                            idNext = currentId + 1;
                        }

                        child.Identity = currentId;

                        if (child.Next != null)
                            child.Next.ParentIdentity = currentId;

                        m_lActions.Add(new GameAction
                        {
                            Id = currentId,
                            Next = idNext,
                            Fail = 0,
                            Type = child.Type,
                            Data = (int) child.Data,
                            Param = param
                        });

                        if ((child.Type == Structures.GameAction.TaskActionType.ActionMenulink
                             || child.Type == Structures.GameAction.TaskActionType.ActionMenuedit)
                            && child.Next != null)
                        {
                            m_nextActions.Enqueue(child.Next);
                        }
                    }

                    m_currentAction = m_currentAction + 10 - m_currentAction % 10;

                    DoRemaining();
                    break;
                }
                default:
                {
                    currentId = m_currentAction++;
                    m_lActions.Add(new GameAction
                    {
                        Id = currentId,
                        Next = current.Next != null ? GenerateActionsRecursively(current.Next) : 0,
                        Fail = current.Fail != null ? GenerateActionsRecursively(current.Fail) : 0,
                        Type = current.ActionType,
                        Data = (int) current.Childrens[0].Data,
                        Param = current.Childrens[0].Param
                    });
                    current.Childrens[0].Identity = currentId;
                    break;
                }
            }

            return currentId;
        }

        private void DoRemaining()
        {
            while (m_nextActions.Count > 0)
            {
                MyActionNode remaining = m_nextActions.Dequeue();
                var action = remaining.Childrens[0];
                string result = GenerateActionsRecursively(remaining).ToString();

                GameAction parent = m_lActions.FirstOrDefault(x => x.Id == remaining.ParentIdentity);
                for (int i = 0; i < m_lActions.Count; i++)
                {
                    if (m_lActions[i].Id == remaining.ParentIdentity)
                    {
                        m_lActions[i] = new GameAction
                        {
                            Id = parent.Id,
                            Next = parent.Next,
                            Fail = parent.Fail,
                            Type = parent.Type,
                            Data = parent.Data,
                            Param = parent.Param.Replace("%task_id", result)
                        };
                        break;
                    }
                }

                m_currentAction = m_currentAction + 10 - m_currentAction % 10;
            }
        }

        private async Task WriteToFileAsync()
        {
            using StreamWriter writer = new StreamWriter(m_savePath, false, Encoding.UTF8, 4098);

            await writer.WriteLineAsync("".PadRight(32, '#'));
            await writer.WriteLineAsync($"# World Conquer Online Server Management Tools - {Kernel.Version}");
            await writer.WriteLineAsync($"# Current File: {m_savePath}");
            await writer.WriteLineAsync($"# Auto generated file - {DateTime.Now:R}");
            await writer.WriteLineAsync("".PadRight(32, '#'));
            await writer.WriteLineAsync("");

            Stopwatch watch = new Stopwatch();
            watch.Start();
            foreach (var action in m_lActions.OrderBy(x => x.Id))
            {
                await writer.WriteLineAsync($"INSERT INTO `cq_action` VALUES ({action.Id:0000},{action.Next:0000},{action.Fail:0000},{(int) action.Type:0000},{action.Data:0000},'{action.Param.Replace("'", "\\'")}');");
            }
            watch.Stop();
            await writer.WriteLineAsync("");
            await writer.WriteLineAsync($"# Script creation took: {watch.ElapsedMilliseconds:N0}ms");
            writer.Close();
        }

        private async void btnDoTheMagic_Click(object sender, EventArgs e)
        {
            SetControls(false);
            GenerateActionsRecursively(m_nodes.FirstOrDefault(x => x.IsEntryPoint) ?? m_nodes[0]);

            await FillDataTableAsync().ConfigureAwait(false);
            await FillQueryAsync().ConfigureAwait(false);
            SetControls(true);

            m_generated = true;
        }

        private void numStartWith_ValueChanged(object sender, EventArgs e)
        {
            m_currentAction = (int) numStartWith.Value;
        }

        private void SetControls(bool enabled)
        {
            btnCancel.Enabled = enabled;
            btnDoTheMagic.Enabled = enabled;
            btnExport.Enabled = enabled;
            numStartWith.Enabled = enabled;
        }
    }
}