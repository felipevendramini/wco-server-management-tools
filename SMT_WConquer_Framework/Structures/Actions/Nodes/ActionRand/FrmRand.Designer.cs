﻿namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionRand
{
    partial class FrmRand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numChances = new System.Windows.Forms.NumericUpDown();
            this.numTries = new System.Windows.Forms.NumericUpDown();
            this.lblChance = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numChances)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTries)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(274, 33);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(188, 33);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 23);
            this.btnAccept.TabIndex = 2;
            this.btnAccept.Text = "&Save";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "chances of success for";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(317, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "tries.";
            // 
            // numChances
            // 
            this.numChances.Location = new System.Drawing.Point(12, 7);
            this.numChances.Maximum = new decimal(new int[] {
            2100000000,
            0,
            0,
            0});
            this.numChances.Name = "numChances";
            this.numChances.Size = new System.Drawing.Size(84, 20);
            this.numChances.TabIndex = 0;
            this.numChances.ValueChanged += new System.EventHandler(this.numChances_ValueChanged);
            // 
            // numTries
            // 
            this.numTries.Location = new System.Drawing.Point(225, 7);
            this.numTries.Maximum = new decimal(new int[] {
            2100000000,
            0,
            0,
            0});
            this.numTries.Name = "numTries";
            this.numTries.Size = new System.Drawing.Size(86, 20);
            this.numTries.TabIndex = 1;
            this.numTries.ValueChanged += new System.EventHandler(this.numTries_ValueChanged);
            // 
            // lblChance
            // 
            this.lblChance.AutoSize = true;
            this.lblChance.Location = new System.Drawing.Point(12, 38);
            this.lblChance.Name = "lblChance";
            this.lblChance.Size = new System.Drawing.Size(133, 13);
            this.lblChance.TabIndex = 9;
            this.lblChance.Text = "Success Rate: 000.0000%";
            // 
            // FrmRand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 68);
            this.Controls.Add(this.lblChance);
            this.Controls.Add(this.numTries);
            this.Controls.Add(this.numChances);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmRand";
            this.Text = "FrmRand";
            ((System.ComponentModel.ISupportInitialize)(this.numChances)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTries)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numChances;
        private System.Windows.Forms.NumericUpDown numTries;
        private System.Windows.Forms.Label lblChance;
    }
}