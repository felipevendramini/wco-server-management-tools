﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - ActionRandDlg.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MindFusion.Diagramming;

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionRand
{
    public class ActionRandDlg : MyActionNode
    {
        public ActionRandDlg(Diagram diagram) 
            : base(diagram, GameAction.TaskActionType.ActionRand.ToString())
        {
        }

        public override void Create(GameAction.TaskActionType type, RectangleF? rect = null)
        {
            base.Create(type, rect);

            m_node[0, 2].Text = "Probability";
            m_node[1, 2].Text = "0%";
        }

        public override void Edit()
        {
            if (!Created)
                return;

            FrmRand form = Childrens.Count > 0 ? new FrmRand(Childrens[0]) : new FrmRand();
            if (form.ShowDialog() != DialogResult.OK)
                return;

            Fill(new List<CqAction>
            {
                form.Result
            });
        }

        public override void Fill(List<CqAction> result)
        {
            string[] stc = result[0].Param.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
            if (stc.Length < 2)
                return;

            double.TryParse(stc[0], out var val0);
            double.TryParse(stc[1], out var val1);

            m_node[1, 2].Text = $"{val0 / val1 * 100d:0.0000}%";

            Childrens.Clear();
            Childrens.Add(result[0]);
        }
    }
}