﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmRand.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Windows.Forms;
using SMT_WConquer_Framework.MyControls;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionRand
{
    public partial class FrmRand : MyForm
    {
        public FrmRand()
        {
            InitializeComponent();
            IsLoginRequired = true;
        }

        public FrmRand(CqAction action)
        {
            InitializeComponent();
            IsLoginRequired = true;

            string[] stc = action.Param.Split(new [] {' '}, 2, StringSplitOptions.RemoveEmptyEntries);
            if (stc.Length < 2)
                return;

            numChances.Value = int.Parse(stc[0]);
            numTries.Value = int.Parse(stc[1]);
        }

        public CqAction Result { get; set; }

        private void numChances_ValueChanged(object sender, EventArgs e)
        {
            numTries.Value = Math.Max(numChances.Value, numTries.Value);
            UpdateLabel();
        }

        private void numTries_ValueChanged(object sender, EventArgs e)
        {
            numChances.Value = Math.Min(numChances.Value, numTries.Value);
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            lblChance.Text = $@"Success Rate: {numChances.Value/numTries.Value*100:0.0000}%";
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (numChances.Value > numTries.Value)
            {
                MessageBox.Show(this, @"The number of chances has to be lower than the number of tries.");
                return;
            }

            Result = new CqAction
            {
                Type = GameAction.TaskActionType.ActionRand,
                Param = $"{(int) numChances.Value} {(int) numTries.Value}"
            };

            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            Close();
        }
    }
}