﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - ActionMenuDlg.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MindFusion.Diagramming;
using SMT_WConquer_Framework.ChildrenForms;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionMenu
{
    public sealed class ActionMenuDlg : MyActionNode
    {
        public ActionMenuDlg(Diagram diagram)
            : base(diagram, GameAction.TaskActionType.ActionMenutext.ToString())
        {
        }

        public override void Create(GameAction.TaskActionType type, RectangleF? rect = null)
        {
            base.Create(type, rect);
            
            m_node[0, 2].Text = "Message";

            m_node.AddRow();
            m_node[0, 3].Text = "Reply 0";
            m_node.Rows[3].AnchorPattern = DefaultOptionAnchorPattern;
        }

        public override void Edit()
        {
            if (!Created)
                return;

            FrmMenu form = Childrens.Count == 0 ? new FrmMenu() : new FrmMenu(Childrens);
            if (form.ShowDialog() != DialogResult.OK)
                return;

            Fill(form.Result);
        }

        public override void Fill(List<CqAction> result)
        {
            m_node[1, 2].Text = "";

            if (result.Count(x =>
                    x.Type == GameAction.TaskActionType.ActionMenulink ||
                    x.Type == GameAction.TaskActionType.ActionMenuedit) < m_node.Rows.Count - 3)
            {
                for (int i = m_node.Rows.Count - 1; i > 2; i--)
                {
                    m_node.Rows.RemoveAt(i);

                    if (result.Count(x => x.Type == GameAction.TaskActionType.ActionMenulink || x.Type == GameAction.TaskActionType.ActionMenuedit) <= m_node.Rows.Count - 3)
                        break;
                }
            }

            if (result.Count(x =>
                    x.Type == GameAction.TaskActionType.ActionMenulink ||
                    x.Type == GameAction.TaskActionType.ActionMenuedit) > m_node.Rows.Count - 3)
            {
                int difference = result.Count(x =>
                                     x.Type == GameAction.TaskActionType.ActionMenulink ||
                                     x.Type == GameAction.TaskActionType.ActionMenuedit) - (m_node.Rows.Count - 3);
                for (int i = 0; i < difference; i++)
                {
                    m_node.AddRow();
                }
            }

            int row = 3;
            foreach (var action in result)
            {
                if (action.Type == GameAction.TaskActionType.ActionMenutext)
                {
                    m_node[1, 2].Text += action.Param;
                }
                else if (action.Type == GameAction.TaskActionType.ActionMenulink
                         || action.Type == GameAction.TaskActionType.ActionMenuedit)
                {
                    m_node[0, row].Text = $"Option {row - 2}";
                    m_node[1, row].Text = action.Param;
                    m_node.Rows[row].AnchorPattern = new AnchorPattern(new[]
                    {
                        new AnchorPoint(95, 50, false, true, MarkStyle.Circle, Color.Green)
                            {ToolTip = "If action result is success", Tag = 1}
                    });

                    row++;
                }
            }

            m_node.ReassignAnchorPoints();
            Childrens = new List<CqAction>(result);
        }
    }
}