﻿namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionMenu
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbFace = new System.Windows.Forms.ComboBox();
            this.cmbReplyType7 = new System.Windows.Forms.ComboBox();
            this.cmbReplyType5 = new System.Windows.Forms.ComboBox();
            this.cmbReplyType3 = new System.Windows.Forms.ComboBox();
            this.cmbReplyType1 = new System.Windows.Forms.ComboBox();
            this.cmbReplyType6 = new System.Windows.Forms.ComboBox();
            this.cmbReplyType4 = new System.Windows.Forms.ComboBox();
            this.cmbReplyType2 = new System.Windows.Forms.ComboBox();
            this.cmbReplyType0 = new System.Windows.Forms.ComboBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtReply7 = new System.Windows.Forms.TextBox();
            this.txtReply6 = new System.Windows.Forms.TextBox();
            this.txtReply5 = new System.Windows.Forms.TextBox();
            this.txtReply4 = new System.Windows.Forms.TextBox();
            this.txtReply3 = new System.Windows.Forms.TextBox();
            this.txtReply2 = new System.Windows.Forms.TextBox();
            this.txtReply1 = new System.Windows.Forms.TextBox();
            this.txtReply0 = new System.Windows.Forms.TextBox();
            this.txtParam = new System.Windows.Forms.TextBox();
            this.txtDlgLength = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 236);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 88;
            this.label1.Text = "Lookface";
            // 
            // cmbFace
            // 
            this.cmbFace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbFace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFace.FormattingEnabled = true;
            this.cmbFace.Items.AddRange(new object[] {
            "610001",
            "610002",
            "610003",
            "610004",
            "610005",
            "610006",
            "610007",
            "610008",
            "610009",
            "610010",
            "610012",
            "610013",
            "610014",
            "610015",
            "610016",
            "610017",
            "610018",
            "610019",
            "610020",
            "610021",
            "610022",
            "610023",
            "610024",
            "610025",
            "610026",
            "610027",
            "610028",
            "610029",
            "610030",
            "610031",
            "610032",
            "610033",
            "610034",
            "610035",
            "610036",
            "610037",
            "610038",
            "610039",
            "610040",
            "610041",
            "610042",
            "610043",
            "610044",
            "610045",
            "610046",
            "610047",
            "610048",
            "610049",
            "610050",
            "610051",
            "610052",
            "610053",
            "610054",
            "610055",
            "610056",
            "610057",
            "610058",
            "610059",
            "610060",
            "610061",
            "610062",
            "610063",
            "610064",
            "610065",
            "610066",
            "610067",
            "610068",
            "610069",
            "610070",
            "610071",
            "610072",
            "610073",
            "610074",
            "610075",
            "610076",
            "610077",
            "610078",
            "610079",
            "610080",
            "610081",
            "610082",
            "610083",
            "610084",
            "610085",
            "610086",
            "610087",
            "610088",
            "610089",
            "610090",
            "610091",
            "610092",
            "610093",
            "610094",
            "610095",
            "610096",
            "610097",
            "610098",
            "610099",
            "610100",
            "610101",
            "610102",
            "610103",
            "610104",
            "610105",
            "610106",
            "610107",
            "610108",
            "610109",
            "610110",
            "610111",
            "610112",
            "610113",
            "610114",
            "610115",
            "610116",
            "610117",
            "610118",
            "610119",
            "610120",
            "610121",
            "610122",
            "610123",
            "610124",
            "610125",
            "610126",
            "610127",
            "610128",
            "610129",
            "610130",
            "610131",
            "610132",
            "610133",
            "610134",
            "610135",
            "610136",
            "610137",
            "610138",
            "610139",
            "610140",
            "610141",
            "610142",
            "610143",
            "610144",
            "610145",
            "610146",
            "610147",
            "610148",
            "610149",
            "610150",
            "610151",
            "610152",
            "610153",
            "610154",
            "610155",
            "610156",
            "610157",
            "610158",
            "610159",
            "610160",
            "610161",
            "610162",
            "610163",
            "610164",
            "610165",
            "610166",
            "610167",
            "610168",
            "610169",
            "610170",
            "610171",
            "610172",
            "610173",
            "610174",
            "610175",
            "610176",
            "610177",
            "610178",
            "610179",
            "610180",
            "610181",
            "610182",
            "610183",
            "610184",
            "610185",
            "610186",
            "610187"});
            this.cmbFace.Location = new System.Drawing.Point(268, 233);
            this.cmbFace.Name = "cmbFace";
            this.cmbFace.Size = new System.Drawing.Size(121, 21);
            this.cmbFace.TabIndex = 85;
            // 
            // cmbReplyType7
            // 
            this.cmbReplyType7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbReplyType7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType7.FormattingEnabled = true;
            this.cmbReplyType7.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType7.Location = new System.Drawing.Point(481, 204);
            this.cmbReplyType7.Name = "cmbReplyType7";
            this.cmbReplyType7.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType7.TabIndex = 84;
            // 
            // cmbReplyType5
            // 
            this.cmbReplyType5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbReplyType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType5.FormattingEnabled = true;
            this.cmbReplyType5.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType5.Location = new System.Drawing.Point(481, 178);
            this.cmbReplyType5.Name = "cmbReplyType5";
            this.cmbReplyType5.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType5.TabIndex = 80;
            // 
            // cmbReplyType3
            // 
            this.cmbReplyType3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbReplyType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType3.FormattingEnabled = true;
            this.cmbReplyType3.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType3.Location = new System.Drawing.Point(481, 154);
            this.cmbReplyType3.Name = "cmbReplyType3";
            this.cmbReplyType3.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType3.TabIndex = 76;
            // 
            // cmbReplyType1
            // 
            this.cmbReplyType1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbReplyType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType1.FormattingEnabled = true;
            this.cmbReplyType1.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType1.Location = new System.Drawing.Point(481, 127);
            this.cmbReplyType1.Name = "cmbReplyType1";
            this.cmbReplyType1.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType1.TabIndex = 72;
            // 
            // cmbReplyType6
            // 
            this.cmbReplyType6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbReplyType6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType6.FormattingEnabled = true;
            this.cmbReplyType6.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType6.Location = new System.Drawing.Point(169, 204);
            this.cmbReplyType6.Name = "cmbReplyType6";
            this.cmbReplyType6.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType6.TabIndex = 82;
            // 
            // cmbReplyType4
            // 
            this.cmbReplyType4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbReplyType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType4.FormattingEnabled = true;
            this.cmbReplyType4.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType4.Location = new System.Drawing.Point(169, 178);
            this.cmbReplyType4.Name = "cmbReplyType4";
            this.cmbReplyType4.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType4.TabIndex = 78;
            // 
            // cmbReplyType2
            // 
            this.cmbReplyType2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbReplyType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType2.FormattingEnabled = true;
            this.cmbReplyType2.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType2.Location = new System.Drawing.Point(169, 154);
            this.cmbReplyType2.Name = "cmbReplyType2";
            this.cmbReplyType2.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType2.TabIndex = 74;
            // 
            // cmbReplyType0
            // 
            this.cmbReplyType0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbReplyType0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReplyType0.FormattingEnabled = true;
            this.cmbReplyType0.Items.AddRange(new object[] {
            "Text",
            "Input"});
            this.cmbReplyType0.Location = new System.Drawing.Point(169, 127);
            this.cmbReplyType0.Name = "cmbReplyType0";
            this.cmbReplyType0.Size = new System.Drawing.Size(79, 21);
            this.cmbReplyType0.TabIndex = 70;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(485, 231);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 87;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerate.Location = new System.Drawing.Point(404, 231);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 86;
            this.btnGenerate.Text = "&Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // txtReply7
            // 
            this.txtReply7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReply7.Location = new System.Drawing.Point(320, 205);
            this.txtReply7.Name = "txtReply7";
            this.txtReply7.Size = new System.Drawing.Size(151, 20);
            this.txtReply7.TabIndex = 83;
            this.txtReply7.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtReply6
            // 
            this.txtReply6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReply6.Location = new System.Drawing.Point(12, 205);
            this.txtReply6.Name = "txtReply6";
            this.txtReply6.Size = new System.Drawing.Size(151, 20);
            this.txtReply6.TabIndex = 81;
            this.txtReply6.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtReply5
            // 
            this.txtReply5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReply5.Location = new System.Drawing.Point(320, 179);
            this.txtReply5.Name = "txtReply5";
            this.txtReply5.Size = new System.Drawing.Size(151, 20);
            this.txtReply5.TabIndex = 79;
            this.txtReply5.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtReply4
            // 
            this.txtReply4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReply4.Location = new System.Drawing.Point(12, 179);
            this.txtReply4.Name = "txtReply4";
            this.txtReply4.Size = new System.Drawing.Size(151, 20);
            this.txtReply4.TabIndex = 77;
            this.txtReply4.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtReply3
            // 
            this.txtReply3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReply3.Location = new System.Drawing.Point(320, 153);
            this.txtReply3.Name = "txtReply3";
            this.txtReply3.Size = new System.Drawing.Size(151, 20);
            this.txtReply3.TabIndex = 75;
            this.txtReply3.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtReply2
            // 
            this.txtReply2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReply2.Location = new System.Drawing.Point(12, 153);
            this.txtReply2.Name = "txtReply2";
            this.txtReply2.Size = new System.Drawing.Size(151, 20);
            this.txtReply2.TabIndex = 73;
            this.txtReply2.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtReply1
            // 
            this.txtReply1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReply1.Location = new System.Drawing.Point(320, 127);
            this.txtReply1.Name = "txtReply1";
            this.txtReply1.Size = new System.Drawing.Size(151, 20);
            this.txtReply1.TabIndex = 71;
            this.txtReply1.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtReply0
            // 
            this.txtReply0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtReply0.Location = new System.Drawing.Point(12, 127);
            this.txtReply0.Name = "txtReply0";
            this.txtReply0.Size = new System.Drawing.Size(151, 20);
            this.txtReply0.TabIndex = 69;
            this.txtReply0.Leave += new System.EventHandler(this.txtReply0_Leave_1);
            // 
            // txtParam
            // 
            this.txtParam.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParam.Location = new System.Drawing.Point(12, 13);
            this.txtParam.MaxLength = 1024;
            this.txtParam.Multiline = true;
            this.txtParam.Name = "txtParam";
            this.txtParam.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtParam.Size = new System.Drawing.Size(549, 108);
            this.txtParam.TabIndex = 68;
            this.txtParam.TextChanged += new System.EventHandler(this.txtParam_TextChanged);
            // 
            // txtDlgLength
            // 
            this.txtDlgLength.AutoSize = true;
            this.txtDlgLength.Location = new System.Drawing.Point(12, 236);
            this.txtDlgLength.Name = "txtDlgLength";
            this.txtDlgLength.Size = new System.Drawing.Size(0, 13);
            this.txtDlgLength.TabIndex = 89;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 266);
            this.Controls.Add(this.txtDlgLength);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbFace);
            this.Controls.Add(this.cmbReplyType7);
            this.Controls.Add(this.cmbReplyType5);
            this.Controls.Add(this.cmbReplyType3);
            this.Controls.Add(this.cmbReplyType1);
            this.Controls.Add(this.cmbReplyType6);
            this.Controls.Add(this.cmbReplyType4);
            this.Controls.Add(this.cmbReplyType2);
            this.Controls.Add(this.cmbReplyType0);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.txtReply7);
            this.Controls.Add(this.txtReply6);
            this.Controls.Add(this.txtReply5);
            this.Controls.Add(this.txtReply4);
            this.Controls.Add(this.txtReply3);
            this.Controls.Add(this.txtReply2);
            this.Controls.Add(this.txtReply1);
            this.Controls.Add(this.txtReply0);
            this.Controls.Add(this.txtParam);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMenu";
            this.Text = "FrmMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbFace;
        private System.Windows.Forms.ComboBox cmbReplyType7;
        private System.Windows.Forms.ComboBox cmbReplyType5;
        private System.Windows.Forms.ComboBox cmbReplyType3;
        private System.Windows.Forms.ComboBox cmbReplyType1;
        private System.Windows.Forms.ComboBox cmbReplyType6;
        private System.Windows.Forms.ComboBox cmbReplyType4;
        private System.Windows.Forms.ComboBox cmbReplyType2;
        private System.Windows.Forms.ComboBox cmbReplyType0;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtReply7;
        private System.Windows.Forms.TextBox txtReply6;
        private System.Windows.Forms.TextBox txtReply5;
        private System.Windows.Forms.TextBox txtReply4;
        private System.Windows.Forms.TextBox txtReply3;
        private System.Windows.Forms.TextBox txtReply2;
        private System.Windows.Forms.TextBox txtReply1;
        private System.Windows.Forms.TextBox txtReply0;
        private System.Windows.Forms.TextBox txtParam;
        private System.Windows.Forms.Label txtDlgLength;
    }
}