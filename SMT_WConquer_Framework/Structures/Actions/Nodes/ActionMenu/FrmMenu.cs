﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmMenu.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SMT_WConquer_Framework.MyControls;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionMenu
{
    public partial class FrmMenu : MyForm
    {
        public FrmMenu()
        {
            InitializeComponent();
            IsLoginRequired = true;
        }

        public FrmMenu(List<CqAction> actions)
        {
            InitializeComponent();
            IsLoginRequired = true;

            int i = 0;
            foreach (CqAction action in actions)
            {
                if (action.Type == GameAction.TaskActionType.ActionMenutext)
                {
                    txtParam.Text += action.Param;
                }
                else if (action.Type == GameAction.TaskActionType.ActionMenulink
                         || action.Type == GameAction.TaskActionType.ActionMenuedit)
                {
                    TextBox ctrlReply = Controls.Find($"txtReply{i}", true)[0] as TextBox;
                    ComboBox ctrlType = Controls.Find($"cmbReplyType{i}", true)[0] as ComboBox;

                    if (ctrlType == null || ctrlReply == null)
                        continue;

                    ctrlReply.Text = action.Param;

                    if (action.Type == GameAction.TaskActionType.ActionMenulink)
                        ctrlType.SelectedIndex = 0;
                    else
                        ctrlType.SelectedIndex = 1;

                    i++;
                }
                else if (action.Type == GameAction.TaskActionType.ActionMenupic)
                {
                    for (int a = 0; a < cmbFace.Items.Count; a++)
                    {
                        if (cmbFace.Items[a].ToString().EndsWith($"{int.Parse(action.Param.Split(' ')[2]):000}",
                            StringComparison.InvariantCultureIgnoreCase))
                        {
                            cmbFace.SelectedIndex = a;
                            break;
                        }
                    }
                }
            }
        }

        public List<CqAction> Result = new List<CqAction>();

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            string param = txtParam.Text;
            int turns = (int) Math.Ceiling(txtParam.TextLength / (float) Kernel.MAX_PARAM_LENGTH);
            for (int i = 0; i < turns; i++)
            {
                int max = Kernel.MAX_PARAM_LENGTH;
                if (i == turns - 1 && txtParam.TextLength % Kernel.MAX_PARAM_LENGTH != 0)
                    max = txtParam.TextLength % Kernel.MAX_PARAM_LENGTH;

                string temp = param.Substring(i * Kernel.MAX_PARAM_LENGTH, Math.Min(128, max));
                Result.Add(new CqAction
                {
                    Type = GameAction.TaskActionType.ActionMenutext,
                    Param = temp
                });
            }

            for (int i = 0; i < 8; i++)
            {
                TextBox ctrlReply = Controls.Find($"txtReply{i}", true)[0] as TextBox;
                ComboBox ctrlType = Controls.Find($"cmbReplyType{i}", true)[0] as ComboBox;

                if (ctrlReply == null || ctrlType == null)
                    continue;

                bool last = false;
                int type = 0;

                if (i < 7)
                {
                    TextBox ctrlNextReply = Controls.Find($"txtReply{i + 1}", true)[0] as TextBox;
                    if (string.IsNullOrEmpty(ctrlNextReply?.Text))
                    {
                        last = true;
                    }
                }
                else
                {
                    last = true;
                }

                string text = "";
                if (!string.IsNullOrEmpty(ctrlReply.Text))
                {
                    type = ctrlType.SelectedIndex > 0 ? 103 : 102;
                    if (type == 103)
                    {
                        string[] split = ctrlReply.Text.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
                        int length = 16;
                        string description = "";

                        switch (split.Length)
                        {
                            case 1:
                                description = split[0];
                                break;
                            case 2:
                                length = int.Parse(split[0]);
                                description = split[1];
                                break;
                        }

                        text = string.Concat(length, " ", description);
                    }
                    else
                    {
                        text = ctrlReply.Text;
                    }
                }

                Result.Add(new CqAction
                {
                    Type = (GameAction.TaskActionType) type,
                    Param = text
                });

                if (last)
                    break;
            }

            int lookface = int.Parse(cmbFace.Items[Math.Max(0, cmbFace.SelectedIndex)].ToString());
            Result.Add(new CqAction
            {
                Type = GameAction.TaskActionType.ActionMenupic,
                Param = $"10 10 {lookface%100}"
            });
            Result.Add(new CqAction
            {
                Type = GameAction.TaskActionType.ActionMenucreate,
                Param = ""
            });

            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            Close();
        }

        private void txtParam_TextChanged(object sender, EventArgs e)
        {
            txtDlgLength.Text = $@"{txtParam.TextLength}/{txtParam.MaxLength} characters. {txtParam.TextLength/Kernel.MAX_PARAM_LENGTH+1} actions.";
        }

        private void UpdateRepliesBoxes()
        {
            bool last = false;
            bool previousIsEnbaled = false;
            for (int i = 0; i < 8; i++)
            {
                TextBox ctrlReply = Controls.Find($"txtReply{i}", true)[0] as TextBox;
                ComboBox ctrlType = Controls.Find($"cmbReplyType{i}", true)[0] as ComboBox;

                if (ctrlReply == null || ctrlType == null)
                    continue;
                
                if (ctrlReply.TextLength == 0)
                {
                    last = true;
                } 
                else if (i < 7)
                {
                    TextBox ctrlNextReply = Controls.Find($"txtReply{i + 1}", true)[0] as TextBox;
                    if (string.IsNullOrEmpty(ctrlNextReply?.Text))
                    {
                        last = true;
                    }
                }
                else
                {
                    last = true;
                }

                if (!last || previousIsEnbaled)
                {
                    if (!ctrlReply.Enabled)
                        ctrlReply.Enabled = true;

                    if (!ctrlType.Enabled)
                        ctrlType.Enabled = true;

                    if (ctrlType.SelectedIndex < 0 && ctrlReply.TextLength > 0)
                        ctrlType.SelectedIndex = 0;
                    else if (ctrlReply.TextLength == 0)
                        ctrlType.SelectedIndex = -1;

                    previousIsEnbaled = false;
                }
                else if (!previousIsEnbaled && i > 0)
                {
                    if (ctrlReply.Enabled)
                        ctrlReply.Enabled = false;

                    if (ctrlType.Enabled)
                        ctrlType.Enabled = false;

                    if (ctrlType.SelectedIndex >= 0)
                        ctrlType.SelectedIndex = -1;
                }

                if (ctrlReply.TextLength > 0)
                    previousIsEnbaled = true;
            }
        }

        private void txtReply0_Leave(object sender, EventArgs e)
        {
            UpdateRepliesBoxes();
        }

        private void txtReply0_Leave_1(object sender, EventArgs e)
        {
            UpdateRepliesBoxes();
        }
    }
}