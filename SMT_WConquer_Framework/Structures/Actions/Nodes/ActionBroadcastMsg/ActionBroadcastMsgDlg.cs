﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - ActionBroadcastMsgDlg.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MindFusion.Diagramming;
using SMT_WConquer_Framework.ChildrenForms;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionBroadcastMsg
{
    public sealed class ActionBroadcastMsgDlg : MyActionNode
    {
        public ActionBroadcastMsgDlg(Diagram diagram)
            : base(diagram, GameAction.TaskActionType.ActionBrocastmsg.ToString())
        {
        }

        public override void Create(GameAction.TaskActionType type, RectangleF? rect = null)
        {
            base.Create(type, rect);

            m_node[0, 1].Text = "Channel";
            m_node[0, 2].Text = "Message";
        }

        public override void Edit()
        {
            if (!Created)
                return;

            FrmBroadcastMsg form = null;

            form = Childrens.Count == 0 ? new FrmBroadcastMsg() : new FrmBroadcastMsg(Childrens[0].Data, Childrens[0].Param);

            if (form.ShowDialog() != DialogResult.OK)
                return;

            Fill(new List<CqAction>
            {
                form.Result
            });
        }

        public override void Fill(List<CqAction> result)
        {
            m_node[1, 1].Text = ((GameAction.TalkChannel)result[0].Data).ToString();
            m_node[1, 2].Text = result[0].Param;

            Childrens = new List<CqAction>(result);
        }
    }
}