﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmBroadcastMsg.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Windows.Forms;
using SMT_WConquer_Framework.MyControls;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionBroadcastMsg
{
    public partial class FrmBroadcastMsg : MyForm
    {
        public FrmBroadcastMsg()
        {
            InitializeComponent();
            IsLoginRequired = true;
        }

        public FrmBroadcastMsg(uint channel, string param)
        {
            InitializeComponent();
            IsLoginRequired = true;

            string strChannel = ((GameAction.TalkChannel) channel).ToString();
            for (int i = 0; i < cmbChatChannel.Items.Count; i++)
            {
                if (cmbChatChannel.Items[i].ToString().Equals(strChannel, StringComparison.InvariantCultureIgnoreCase))
                {
                    cmbChatChannel.SelectedIndex = i;
                    break;
                }
            }

            txtMessage.Text = param;
        }

        public CqAction Result { get; set; }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (cmbChatChannel.SelectedIndex < 0 || cmbChatChannel.SelectedIndex >= cmbChatChannel.Items.Count)
            {
                MessageBox.Show(this, @"You need to select a valid Channel to continue.");
                return;
            }

            if (!Enum.TryParse(cmbChatChannel.Items[cmbChatChannel.SelectedIndex].ToString(),
                out GameAction.TalkChannel channel))
            {
                MessageBox.Show(this, @"You need to select a valid Channel to continue.");
                return;
            }

            if (string.IsNullOrEmpty(txtMessage.Text))
            {
                MessageBox.Show(this, @"You cannot transmit an empty message.");
                return;
            }

            Result = new CqAction
            {
                Type = GameAction.TaskActionType.ActionBrocastmsg,
                Data = (uint) channel,
                Param = txtMessage.Text.Substring(0, Math.Min(127, txtMessage.TextLength))
            };

            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            Close();
        }

        private void FrmBroadcastMsg_Load(object sender, EventArgs e)
        {
            if (cmbChatChannel.SelectedIndex < 0)
                cmbChatChannel.SelectedIndex = 0;
        }
    }
}