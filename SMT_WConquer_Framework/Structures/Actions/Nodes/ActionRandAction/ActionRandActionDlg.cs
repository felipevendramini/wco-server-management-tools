﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - ActionRandActionDlg.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MindFusion.Diagramming;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionRandAction
{
    public sealed class ActionRandActionDlg : MyActionNode
    {
        public ActionRandActionDlg(Diagram diagram)
            : base(diagram, GameAction.TaskActionType.ActionRandaction.ToString())
        {
        }

        public override void Create(GameAction.TaskActionType type, RectangleF? rect = null)
        {
            base.Create(type, rect);

            m_node[0, 2].Text = "Option 1";
            m_node.Rows[2].AnchorPattern = new AnchorPattern(new[]
            {
                new AnchorPoint(95, 50, false, true, MarkStyle.Circle, Color.Green)
                    {ToolTip = "If action result is success", Tag = 1}
            });
        }

        public override void Fill(List<CqAction> result)
        {
            int num = result[0].Param.Split(' ').Length;
            int check = 2 + num;
            if (m_node.RowCount > 2 + num)
            {
                for (int i = m_node.RowCount - 1; i > check - 1; i--)
                {
                    m_node.Rows.RemoveAt(i);
                }
            }

            for (int row = 2; row < num + 2; row++)
            {
                if (m_node.RowCount <= row)
                {
                    m_node.AddRow();
                    m_node[0, row].Text = $"Option {row - 1}";
                    m_node.Rows[row].AnchorPattern = new AnchorPattern(new[]
                    {
                        new AnchorPoint(95, 50, false, true, MarkStyle.Circle, Color.Green)
                            {ToolTip = "If action result is success", Tag = 1}
                    });
                }
            }

            m_node.ReassignAnchorPoints();
            Childrens = new List<CqAction>(result);
        }

        public override void Edit()
        {
            if (!Created)
                return;

            FrmRandAction form = Childrens.Count == 0 ? new FrmRandAction() : new FrmRandAction(Childrens[0]);
            if (form.ShowDialog() != DialogResult.OK)
                return;

            Fill(form.Result);
        }
    }
}