﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmRandAction.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SMT_WConquer_Framework.MyControls;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions.Nodes.ActionRandAction
{
    public partial class FrmRandAction : MyForm
    {
        public FrmRandAction()
        {
            InitializeComponent();
            IsLoginRequired = true;
        }
        
        public FrmRandAction(CqAction action)
        {
            InitializeComponent();
            IsLoginRequired = true;

            numChoices.Value = Math.Max(1, action.Param.Split(' ').Length);
        }

        public List<CqAction> Result = new List<CqAction>();

        private void btnApply_Click(object sender, EventArgs e)
        {
            string result = "";
            for (int i = 0; i < numChoices.Value; i++)
                result += $"%task{i} ";
            result = result.Trim();

            Result.Add(new CqAction
            {
                Type = GameAction.TaskActionType.ActionRandaction,
                Param = result
            });

            DialogResult = DialogResult.OK;
            Close();
        }

        private void FrmRandAction_Load(object sender, EventArgs e)
        {

        }
    }
}