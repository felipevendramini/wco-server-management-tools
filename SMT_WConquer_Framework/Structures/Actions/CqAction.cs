﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - CqAction.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace SMT_WConquer_Framework.Structures.Actions
{
    public class CqAction
    {
        public int Identity { get; set; }

        public MyActionNode Node { get; set; }
        public MyActionNode Next { get; set; }
        public int NextIdx { get; set; } = -1; // only for rebuilding
        public MyActionNode Fail { get; set; }
        public int FailIdx { get; set; } = -1; // only for rebuilding
        public GameAction.TaskActionType Type { get; set; }
        public uint Data { get; set; }
        public string Param { get; set; }
    }
}