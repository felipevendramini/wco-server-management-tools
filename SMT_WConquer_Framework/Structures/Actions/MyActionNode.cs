﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - MyActionNode.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MindFusion.Diagramming;
using SMT_WConquer_Framework.ChildrenForms;
using SolidBrush = MindFusion.Drawing.SolidBrush;

#endregion

namespace SMT_WConquer_Framework.Structures.Actions
{
    public class MyActionNode
    {
        public static readonly RectangleF DefaultContainerSize = new RectangleF(10, 10, 66, 76);
        public static readonly RectangleF DefaultSize = new RectangleF(10, 10, 60, 70);

        protected Diagram m_diagram;

        protected ContainerNode m_container;
        protected TableNode m_node;

        protected string m_title;

        public List<CqAction> Childrens = new List<CqAction>();

        public MyActionNode(Diagram diagram, string title)
        {
            m_title = title ?? string.Empty;
            m_diagram = diagram;
            Identity = FrmActionTask.CurrentActionIdx++;
        }

        protected static AnchorPattern DefaultAnchorPattern =>
            new AnchorPattern(new[]
            {
                new AnchorPoint(0, 10, true, false, MarkStyle.Circle, Color.Yellow)
                    {ToolTip = "Incoming action", Tag = 0},
                new AnchorPoint(100, 20, false, true, MarkStyle.Circle, Color.Green)
                    {ToolTip = "If action result is success", Tag = 1},
                new AnchorPoint(100, 40, false, true, MarkStyle.Circle, Color.Red)
                    {ToolTip = "If action result is fail", Tag = 2}
            });

        protected static AnchorPattern DefaultOptionAnchorPattern =>
            new AnchorPattern(new[]
            {
                new AnchorPoint(95, 50, false, true, MarkStyle.Circle, Color.Green)
                    {ToolTip = "If action result is success", Tag = 1}
            });

        public TableNode Table => m_node;

        public ContainerNode Container => m_container;

        public int Identity { get; set; }
        public int ParentIdentity { get; set; } = -1;

        public bool Created { get; protected set; }

        public GameAction.TaskActionType ActionType { get; private set; }

        public int NextIdx { get; set; } = -1;
        public MyActionNode Next { get; set; }
        public int FailIdx { get; set; } = -1;
        public MyActionNode Fail { get; set; }

        public bool IsEntryPoint = false;

        public virtual void Create(GameAction.TaskActionType type, RectangleF? rect = null)
        {
            m_container = m_diagram.Factory.CreateContainerNode(rect ?? DefaultSize);
            m_container.Margin = 3;
            m_container.Caption = type.ToString();
            m_container.CaptionBackBrush = new SolidBrush(Color.DodgerBlue);
            m_container.Shape = SimpleShape.RoundedRectangle;
            m_container.AutoGrow = true;
            m_container.AutoShrink = true;

            m_node = m_diagram.Factory.CreateTableNode(rect ?? DefaultSize, 2, 3);

            m_container.Add(m_node);

            ActionType = type;

            m_node.AllowResizeRows = true;
            m_node[0, 0].ColumnSpan = 2;
            m_node[0, 0].Text = GameAction.GetDescription(type);
            m_node[0, 1].Text = "Data";
            m_node.Rows[0].Height = 25;
            m_node.ToolTip = m_title;
            m_node.Obstacle = true;

            m_node.AllowIncomingLinks = false;

            m_node.Caption = null;
            m_node.CaptionHeight = 0;
            m_node.Brush = new SolidBrush(Color.White);
            m_node.TextBrush = new SolidBrush(Color.Black);

            Created = true;
        }

        public virtual void Edit()
        {
            if (!Created)
                return;

            MessageBox.Show($@"Unhandled edit box for {ActionType}.");
        }

        public virtual void Fill(List<CqAction> result)
        {

        }

        public virtual void Render()
        {
            if (!Created)
                return;

            m_container.AnchorPattern = DefaultAnchorPattern;
            if (ActionType == GameAction.TaskActionType.ActionMenutext)
            {
                m_container.AnchorPattern.Points.RemoveAt(2);
                m_container.AnchorPattern.Points.RemoveAt(1);
            }

            m_container.Tag = this;
            m_node.Tag = this;

            m_container.SubordinateGroup.AutoDeleteItems = true;

            m_container.ReassignAnchorPoints();
            m_node.ReassignAnchorPoints();
        }
    }
}