﻿
namespace SMT_WConquer_Framework.Structures.Maps
{
    partial class FrmGameMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn7z = new System.Windows.Forms.Button();
            this.btmDMAP = new System.Windows.Forms.Button();
            this.btnPack = new System.Windows.Forms.Button();
            this.lbxGameMap = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPuzzle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSaveGameMap = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtFilename
            // 
            this.txtFilename.Enabled = false;
            this.txtFilename.Location = new System.Drawing.Point(12, 12);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(347, 20);
            this.txtFilename.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(365, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Load";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn7z
            // 
            this.btn7z.Location = new System.Drawing.Point(446, 10);
            this.btn7z.Name = "btn7z";
            this.btn7z.Size = new System.Drawing.Size(75, 23);
            this.btn7z.TabIndex = 1;
            this.btn7z.Text = "All to 7z";
            this.btn7z.UseVisualStyleBackColor = true;
            this.btn7z.Click += new System.EventHandler(this.btn7z_Click);
            // 
            // btmDMAP
            // 
            this.btmDMAP.Location = new System.Drawing.Point(527, 10);
            this.btmDMAP.Name = "btmDMAP";
            this.btmDMAP.Size = new System.Drawing.Size(75, 23);
            this.btmDMAP.TabIndex = 1;
            this.btmDMAP.Text = "All to DMAP";
            this.btmDMAP.UseVisualStyleBackColor = true;
            this.btmDMAP.Click += new System.EventHandler(this.btmDMAP_Click);
            // 
            // btnPack
            // 
            this.btnPack.Location = new System.Drawing.Point(608, 10);
            this.btnPack.Name = "btnPack";
            this.btnPack.Size = new System.Drawing.Size(81, 23);
            this.btnPack.TabIndex = 2;
            this.btnPack.Text = "Pack to DAT";
            this.btnPack.UseVisualStyleBackColor = true;
            this.btnPack.Click += new System.EventHandler(this.btnPack_Click);
            // 
            // lbxGameMap
            // 
            this.lbxGameMap.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbxGameMap.FormattingEnabled = true;
            this.lbxGameMap.Location = new System.Drawing.Point(12, 38);
            this.lbxGameMap.Name = "lbxGameMap";
            this.lbxGameMap.Size = new System.Drawing.Size(393, 589);
            this.lbxGameMap.TabIndex = 3;
            this.lbxGameMap.SelectedIndexChanged += new System.EventHandler(this.lbxGameMap_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnSaveGameMap);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtPuzzle);
            this.panel1.Controls.Add(this.txtPath);
            this.panel1.Controls.Add(this.txtID);
            this.panel1.Location = new System.Drawing.Point(411, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(460, 595);
            this.panel1.TabIndex = 4;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(18, 27);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(195, 20);
            this.txtID.TabIndex = 0;
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(18, 66);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(195, 20);
            this.txtPath.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Path";
            // 
            // txtPuzzle
            // 
            this.txtPuzzle.Location = new System.Drawing.Point(18, 105);
            this.txtPuzzle.Name = "txtPuzzle";
            this.txtPuzzle.Size = new System.Drawing.Size(195, 20);
            this.txtPuzzle.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Puzzle";
            // 
            // btnSaveGameMap
            // 
            this.btnSaveGameMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveGameMap.Location = new System.Drawing.Point(379, 566);
            this.btnSaveGameMap.Name = "btnSaveGameMap";
            this.btnSaveGameMap.Size = new System.Drawing.Size(75, 23);
            this.btnSaveGameMap.TabIndex = 2;
            this.btnSaveGameMap.Text = "Save";
            this.btnSaveGameMap.UseVisualStyleBackColor = true;
            this.btnSaveGameMap.Click += new System.EventHandler(this.btnSaveGameMap_Click);
            // 
            // FrmGameMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 646);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbxGameMap);
            this.Controls.Add(this.btnPack);
            this.Controls.Add(this.btmDMAP);
            this.Controls.Add(this.btn7z);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtFilename);
            this.Name = "FrmGameMap";
            this.Text = "FrmGameMap";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn7z;
        private System.Windows.Forms.Button btmDMAP;
        private System.Windows.Forms.Button btnPack;
        private System.Windows.Forms.ListBox lbxGameMap;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPuzzle;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button btnSaveGameMap;
    }
}