﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace SMT_WConquer_Framework.Structures.Maps
{
    public partial class FrmGameMap : Form
    {
        private Dictionary<uint, GameMapDat> m_mapDatas = new Dictionary<uint,GameMapDat>();

        public FrmGameMap()
        {
            InitializeComponent();
        }

        public Task LoadFileAsync()
        {
            lbxGameMap.Items.Clear();
            m_mapDatas.Clear();

            BinaryReader reader = new BinaryReader(File.OpenRead(txtFilename.Text), Encoding.ASCII);

            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                uint idMap = reader.ReadUInt32();
                int length = reader.ReadInt32();
                string name = (new string(reader.ReadChars(length))).Trim('\0');
                uint puzzle = reader.ReadUInt32();

                if (!m_mapDatas.ContainsKey(idMap))
                {
                    m_mapDatas.Add(idMap, new GameMapDat
                    {
                        MapIdentity = idMap,
                        Name = name,
                        Puzzle = puzzle
                    });
                }
            }

            foreach (var map in m_mapDatas.Values.OrderBy(x => x.MapIdentity))
            {
                lbxGameMap.Items.Add($"{map.MapIdentity} - {map.Name} (Puzzle: {map.Puzzle})");
            }

            return Task.CompletedTask;
        }

        public struct GameMapDat
        {
            public uint MapIdentity { get; set; }
            public string Name { get; set; }
            public uint Puzzle { get; set; }
        }

        private void btn7z_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < m_mapDatas.Count; i++)
            {
                GameMapDat map = m_mapDatas.Values.ElementAt(i);
                m_mapDatas[map.MapIdentity] = new GameMapDat
                {
                    MapIdentity = map.MapIdentity,
                    Name = map.Name.Replace(".dmap", ".7z"),
                    Puzzle = map.Puzzle
                };
            }
        }

        private void btmDMAP_Click(object sender, System.EventArgs e)
        {
            for (int i = 0; i < m_mapDatas.Count; i++)
            {
                GameMapDat map = m_mapDatas.Values.ElementAt(i);
                m_mapDatas[map.MapIdentity] = new GameMapDat
                {
                    MapIdentity = map.MapIdentity,
                    Name = map.Name.Replace(".7z", ".dmap"),
                    Puzzle = map.Puzzle
                };
            }
        }

        private void btnPack_Click(object sender, System.EventArgs e)
        {
            if (m_mapDatas.Count == 0)
            {
                MessageBox.Show(this, @"Nothing to save here.");
                return;
            }

            using StreamWriter stream = new StreamWriter(txtFilename.Text + ".new", false);
            BinaryWriter writer = new BinaryWriter(stream.BaseStream);
            writer.Write(BitConverter.GetBytes(m_mapDatas.Count));

            foreach (var map in m_mapDatas.Values)
            {
                writer.Write(BitConverter.GetBytes(map.MapIdentity));
                writer.Write(BitConverter.GetBytes(map.Name.Length));
                writer.Write(Encoding.ASCII.GetBytes(map.Name));
                writer.Write(BitConverter.GetBytes(map.Puzzle));
            }

            writer.Flush();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (dlg.ShowDialog(this) != DialogResult.OK)
                return;

            if (!File.Exists(dlg.FileName))
                return;

            txtFilename.Text = dlg.FileName;
            await LoadFileAsync();
        }

        private void lbxGameMap_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = lbxGameMap.SelectedIndex;
            if (index < 0 || index >= lbxGameMap.Items.Count)
            {
                txtID.Text = string.Empty;
                txtPath.Text = string.Empty;
                txtPuzzle.Text = string.Empty;
                return;
            }

            var mapData = m_mapDatas.Values.FirstOrDefault(x => x.MapIdentity == int.Parse(lbxGameMap.Items[index].ToString().Split('-')[0].Trim()));
            if (mapData.Equals(default))
                return;

            txtID.Text = mapData.MapIdentity.ToString();
            txtPath.Text = mapData.Name;
            txtPuzzle.Text = mapData.Puzzle.ToString();
        }

        private void btnSaveGameMap_Click(object sender, EventArgs e)
        {
            if (!uint.TryParse(txtID.Text, out var idMap))
                return;

            if (!m_mapDatas.ContainsKey(idMap))
                return;

            m_mapDatas[idMap] = new GameMapDat
            {
                MapIdentity = idMap,
                Name = txtPath.Text,
                Puzzle = uint.Parse(txtPuzzle.Text)
            };
        }
    }
}