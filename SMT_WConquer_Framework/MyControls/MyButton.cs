﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - MyButton.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Windows.Forms;

#endregion

namespace SMT_WConquer_Framework.MyControls
{
    public class MyButton : Button
    {
        public void SetProperty(Event e, object value)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<Event, object>(SetProperty));
                return;
            }

            switch (e)
            {
                case Event.Text:
                    Text = value.ToString();
                    break;
                case Event.Enabled:
                    Enabled = (bool)value;
                    break;
            }
        }

        public enum Event
        {
            Text,
            Enabled
        }
    }
}