﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - MyForm.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Threading.Tasks;
using System.Windows.Forms;

#endregion

namespace SMT_WConquer_Framework.MyControls
{
    public class MyForm : Form
    {
        public bool AllowMultiple { get; set; } = false;
        public bool IsLoginRequired { get; set; }

        public virtual Task OnTargetDatabaseChangeAsync(DatabaseServerConfiguration config)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Procedure to be executed when the window is exiting.
        /// </summary>
        public virtual Task SaveAsync(CloseReason reason)
        {
            return Task.CompletedTask;
        }
    }
}