﻿#region References

using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

#endregion

namespace SMT_WConquer_Framework.Database
{
    public static class BaseRepository
    {
        public static async Task<DataTable> SelectAsync(string query, DatabaseServerConfiguration config)
        {
            await using DatabaseContext ctx = new DatabaseContext(config);
            var result = new DataTable();
            var connection = ctx.Database.GetDbConnection();
            var state = connection.State;

            try
            {
                if (state != ConnectionState.Open)
                    await connection.OpenAsync();

                var command = connection.CreateCommand();
                command.CommandText = query;
                command.CommandType = CommandType.Text;

                using var reader = await command.ExecuteReaderAsync();
                result.Load(reader);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (state != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }
    }
}