﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - DbGoods.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

#endregion

namespace SMT_WConquer_Framework.Database.Entities
{
    [Table("cq_goods")]
    public class DbGoods
    {
        [Key] [Column("id")] public virtual uint Identity { get; set; }
        [Column("ownerid")] public virtual uint OwnerIdentity { get; set; }
        [Column("itemtype")] public virtual uint Itemtype { get; set; }
        [Column("moneytype")] public virtual uint Moneytype { get; set; }
        [Column("monopoly")] public virtual uint Monopoly { get; set; }

        public static async Task<List<DbGoods>> GetAsync(GameServerConfiguration config)
        {
            await using var db = new DatabaseContext(config ?? ServerConfiguration.SelectedServerDatabase);
            return db.Goods.ToList();
        }
    }
}