﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - DbItemtype.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace SMT_WConquer_Framework.Database.Entities
{
    [Table("cq_itemtype")]
    public class DbItemtype
    {
        [Key] public virtual uint id { get; set; }

        public virtual string name { get; set; }
        public virtual uint req_profession { get; set; }
        public virtual byte req_weaponskill { get; set; }
        public virtual byte req_level { get; set; }
        public virtual byte req_sex { get; set; }
        public virtual ushort req_force { get; set; }
        public virtual ushort req_speed { get; set; }
        public virtual ushort req_health { get; set; }
        public virtual ushort req_soul { get; set; }
        public virtual uint monopoly { get; set; }
        public virtual uint price { get; set; }
        public virtual uint id_action { get; set; }
        public virtual ushort attack_max { get; set; }
        public virtual ushort attack_min { get; set; }
        public virtual short defense { get; set; }
        public virtual short dexterity { get; set; }
        public virtual short dodge { get; set; }
        public virtual short life { get; set; }
        public virtual short mana { get; set; }
        public virtual ushort amount { get; set; }
        public virtual ushort amount_limit { get; set; }
        public virtual byte ident { get; set; }
        public virtual byte gem1 { get; set; }
        public virtual byte gem2 { get; set; }
        public virtual uint magic1 { get; set; }
        public virtual byte magic2 { get; set; }
        public virtual byte magic3 { get; set; }
        public virtual uint data { get; set; }
        public virtual ushort magic_atk { get; set; }
        public virtual ushort magic_def { get; set; }
        public virtual ushort atk_range { get; set; }
        public virtual ushort atk_speed { get; set; }
        public virtual byte fray_mode { get; set; }
        public virtual byte repair_mode { get; set; }
        public virtual byte type_mask { get; set; }
        public virtual uint emoney_price { get; set; }
        public virtual uint emoney_mono_price { get; set; }
        public virtual int save_time { get; set; }
        public virtual ushort critical_rate { get; set; }
        public virtual ushort magic_critical_rate { get; set; }
        public virtual ushort anti_critical_rate { get; set; }
        public virtual ushort magic_penetration { get; set; }
        public virtual uint shield_block { get; set; }
        public virtual ushort crash_attack { get; set; }
        public virtual ushort stable_defence { get; set; }
        public virtual uint accumulate_limit { get; set; }
        public virtual uint attr_metal { get; set; }
        public virtual uint attr_wood { get; set; }
        public virtual uint attr_water { get; set; }
        public virtual uint attr_fire { get; set; }
        public virtual uint attr_earth { get; set; }
        public virtual string type_desc { get; set; }
        public virtual string item_desc { get; set; }
        public virtual uint color_index { get; set; }
        public virtual byte godsoullev { get; set; }
        public virtual byte meteor_count { get; set; }
        public virtual uint recover_energy { get; set; }
        public virtual ushort auction_class { get; set; }
        public virtual ushort final_dmg_add { get; set; }
        public virtual ushort final_dmg_add_mgc { get; set; }
        public virtual ushort final_dmg_reduce { get; set; }
        public virtual ushort final_dmg_reduce_mgc { get; set; }
        public virtual uint sort { get; set; }
    }
}