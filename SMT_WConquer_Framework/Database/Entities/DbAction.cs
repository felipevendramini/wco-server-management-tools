﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - DbAction.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
// ReSharper disable NonReadonlyMemberInGetHashCode

#endregion

namespace SMT_WConquer_Framework.Database.Entities
{
    [Table("cq_action")]
    public class DbAction
    {
        [Key] [DatabaseGenerated(DatabaseGeneratedOption.None)] [Column("id")]  public virtual uint Identity { get; set; }
        [Column("id_next")] public virtual uint IdNext { get; set; }
        [Column("id_nextfail")] public virtual uint IdNextfail { get; set; }
        [Column("type")] public virtual uint Type { get; set; }
        [Column("data")] public virtual uint Data { get; set; }
        [Column("param")] public virtual string Param { get; set; }

        public static async Task<List<DbAction>> GetAsync(GameServerConfiguration config)
        {
            await using var db = new DatabaseContext(config ?? ServerConfiguration.SelectedServerDatabase);
            if (!await db.TableExistsAsync("cq_newaction"))
                return db.Actions.ToList();
            return db.Actions.FromSqlRaw("SELECT * FROM cq_action UNION ALL SELECT * FROM cq_newaction ORDER BY id ASC").ToList();
        }

        public override bool Equals(object obj)
        {
            return obj is DbAction action && action.Identity == Identity
                                          && action.IdNext == IdNext
                                          && action.IdNextfail == IdNextfail
                                          && action.Type == Type
                                          && action.Data == Data
                                          && action.Param.Equals(Param, StringComparison.InvariantCulture);
        }

        protected bool Equals(DbAction other)
        {
            return Identity == other.Identity && IdNext == other.IdNext && IdNextfail == other.IdNextfail && Type == other.Type && Data == other.Data && Param == other.Param;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Identity, IdNext, IdNextfail, Type, Data, Param);
        }

        public async Task<bool> CreateAsync()
        {
            try
            {
                await using var db = new DatabaseContext(ServerConfiguration.SelectedServerDatabase);
                await db.Actions.AddAsync(this);
                await db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> SaveAsync()
        {
            try
            {
                await using var db = new DatabaseContext(ServerConfiguration.SelectedServerDatabase);
                db.Update(this);
                await db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //The guild war this week will start from 12:00 Sunday and end on 15:00 Sunday (GMT-3).
    }
}