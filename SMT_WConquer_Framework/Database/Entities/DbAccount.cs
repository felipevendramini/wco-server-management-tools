﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - DbAccount.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Utilities.Encoders;

#endregion

namespace SMT_WConquer_Framework.Database.Entities
{
    /// <summary>
    ///     Account information for a registered player. The account server uses this information
    ///     to authenticate the player on login, and track permissions and player access to the
    ///     server. Passwords are hashed using a salted SHA-1 for user protection.
    /// </summary>
    [Table("conquer_account")]
    public class DbAccount
    {
        // Column Properties

        [Key] public uint Id { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public ushort AuthorityID { get; set; }
        public string IPAddress { get; set; }
        public DateTime Created { get; set; }

        /// <summary>
        ///     Fetches an account record from the database using the player's username as a
        ///     unique key for selecting a single record.
        /// </summary>
        /// <param name="username">Username to pull account info for</param>
        /// <returns>Returns account details from the database.</returns>
        public static async Task<DbAccount> FindAsync(string username)
        {
            await using var db = new DatabaseContext(ServerConfiguration.AccountServer);
            return await db.Accounts
                .Where(x => x.Username == username)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        ///     Fetches an account record from the database using the player's username as a
        ///     unique key for selecting a single record.
        /// </summary>
        /// <param name="username">Username to pull account info for</param>
        /// <returns>Returns account details from the database.</returns>
        public static async Task<DbAccount> FindAsync(uint idAccount)
        {
            await using var db = new DatabaseContext(ServerConfiguration.AccountServer);
            return await db.Accounts
                .Where(x => x.Id == idAccount)
                .SingleOrDefaultAsync();
        }

        /// <summary>
        ///     Validates the user's inputted password, which has been decrypted from the client
        ///     request decode method, and is ready to be hashed and compared with the SHA-1
        ///     hash in the database.
        /// </summary>
        /// <param name="input">Inputted password from the client's request</param>
        /// <param name="hash">Hashed password in the database</param>
        /// <param name="salt">Salt for the hashed password in the databse</param>
        /// <returns>Returns true if the password is correct.</returns>
        public static bool CheckPassword(string input, string hash, string salt)
        {
            return HashPassword(input, salt).Equals(hash);
        }

        public static string HashPassword(string password, string salt)
        {
            byte[] inputHashed;
            using (var sha256 = SHA256.Create())
                inputHashed = sha256.ComputeHash(Encoding.ASCII.GetBytes(password + salt));
            var final = Hex.ToHexString(inputHashed);
            return final;
        }

        public static string GenerateSalt()
        {
            const string UPPER_S = "QWERTYUIOPASDFGHJKLZXCVBNM";
            const string LOWER_S = "qwertyuioplkjhgfdsazxcvbnm";
            const string NUMBER_S = "1236547890";
            const string POOL_S = UPPER_S + LOWER_S + NUMBER_S;
            const int SIZE_I = 30;

            Random random = new Random();
            string output = "";
            for (int i = 0; i < SIZE_I; i++)
            {
                output += POOL_S[random.Next() % POOL_S.Length];
            }

            return output;
        }
    }
}