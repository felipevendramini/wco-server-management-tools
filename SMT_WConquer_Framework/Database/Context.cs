﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - Context.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SMT_WConquer_Framework.Database.Entities;

#endregion

namespace SMT_WConquer_Framework.Database
{
    public class DatabaseContext : DbContext
    {
        private DatabaseServerConfiguration m_config;

        public DatabaseContext(DatabaseServerConfiguration config)
        {
            m_config = config;
        }

        public virtual DbSet<DbAccount> Accounts { get; set; }
        public virtual DbSet<DbAction> Actions { get; set; }
        public virtual DbSet<DbTask> Tasks { get; set; }
        public virtual DbSet<DbNpc> Npcs { get; set; }
        public virtual DbSet<DbMap> Maps { get; set; }
        public virtual DbSet<DbGoods> Goods { get; set; }

        /// <summary>
        ///     Configures the database to be used for this context. This method is called
        ///     for each instance of the context that is created. For this project, the MySQL
        ///     connector will be initialized with a connection string from the server's
        ///     configuration file.
        /// </summary>
        /// <param name="options">Builder to create the context</param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseLazyLoadingProxies(false);
            options.UseMySql($"server={m_config.Hostname};database={m_config.Database};user={m_config.Username};password={m_config.Password}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbAction>()
                .Property(x => x.Identity)
                .ValueGeneratedNever();

            base.OnModelCreating(modelBuilder);
        }

        public static async Task<bool> TableExistsAsync(string name, GameServerConfiguration config = null)
        {
            config ??= ServerConfiguration.SelectedServerDatabase;
            await using var ctx = new DatabaseContext(config);
            return await ctx.TableExistsAsync(name);
        }

        public async Task<bool> TableExistsAsync(string name)
        {
            return int.Parse((await SelectAsync(
                           $"(SELECT COUNT(*) FROM information_schema.TABLES WHERE TABLE_SCHEMA='{m_config.Database}' AND TABLE_NAME='{name}' LIMIT 1)")
                       ).Rows[0][0].ToString()) > 0;
        }

        public async Task<DataTable> SelectAsync(string query)
        {
            var result = new DataTable();
            var connection = Database.GetDbConnection();
            var state = connection.State;

            try
            {
                if (state != ConnectionState.Open)
                    await connection.OpenAsync();

                var command = connection.CreateCommand();
                command.CommandText = query;
                command.CommandType = CommandType.Text;

                using var reader = await command.ExecuteReaderAsync();
                result.Load(reader);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (state != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

        public static async Task<DataTable> SelectAsync(string query, DatabaseServerConfiguration config)
        {
            config ??= ServerConfiguration.SelectedServerDatabase;
            await using DatabaseContext ctx = new DatabaseContext(config);
            return await ctx.SelectAsync(query);
        }

        /// <summary>
        ///     Tests that the database connection is alive and configured.
        /// </summary>
        public static async Task<bool> PingAsync(DatabaseServerConfiguration config)
        {
            try
            {
                await using DatabaseContext ctx = new DatabaseContext(config);
                return ctx.Database.CanConnect();
            }
            catch
            {
                return false;
            }
        }
    }
}