﻿namespace SMT_WConquer_Framework
{
    partial class FrmMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.topMenuStrip = new System.Windows.Forms.MenuStrip();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extractSQLScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventsCreatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemtypeManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.magictypeManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goodsManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emoneyShopV2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.honorShopManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gameMapdatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapDBEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leechingToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actionsGrabberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemtypeEncryptDescryptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.magicTypeOperationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tscbServer = new System.Windows.Forms.ToolStripComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.racePointShopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // topMenuStrip
            // 
            this.topMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountToolStripMenuItem,
            this.fileToolStripMenuItem,
            this.servicesToolStripMenuItem,
            this.leechingToolsToolStripMenuItem,
            this.tscbServer});
            this.topMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.topMenuStrip.Name = "topMenuStrip";
            this.topMenuStrip.Size = new System.Drawing.Size(830, 27);
            this.topMenuStrip.TabIndex = 1;
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(64, 23);
            this.accountToolStripMenuItem.Text = "&Account";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.extractSQLScriptToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 23);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // extractSQLScriptToolStripMenuItem
            // 
            this.extractSQLScriptToolStripMenuItem.Name = "extractSQLScriptToolStripMenuItem";
            this.extractSQLScriptToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.extractSQLScriptToolStripMenuItem.Text = "&Extract SQL Script";
            // 
            // servicesToolStripMenuItem
            // 
            this.servicesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eventsCreatorToolStripMenuItem,
            this.itemtypeManagerToolStripMenuItem,
            this.magictypeManagerToolStripMenuItem,
            this.goodsManagerToolStripMenuItem,
            this.emoneyShopV2ToolStripMenuItem,
            this.honorShopManagerToolStripMenuItem,
            this.gameMapdatToolStripMenuItem,
            this.mapDBEditorToolStripMenuItem,
            this.racePointShopToolStripMenuItem});
            this.servicesToolStripMenuItem.Name = "servicesToolStripMenuItem";
            this.servicesToolStripMenuItem.Size = new System.Drawing.Size(61, 23);
            this.servicesToolStripMenuItem.Text = "&Services";
            // 
            // eventsCreatorToolStripMenuItem
            // 
            this.eventsCreatorToolStripMenuItem.Name = "eventsCreatorToolStripMenuItem";
            this.eventsCreatorToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.eventsCreatorToolStripMenuItem.Text = "Events Creator";
            this.eventsCreatorToolStripMenuItem.Click += new System.EventHandler(this.eventsCreatorToolStripMenuItem_Click);
            // 
            // itemtypeManagerToolStripMenuItem
            // 
            this.itemtypeManagerToolStripMenuItem.Name = "itemtypeManagerToolStripMenuItem";
            this.itemtypeManagerToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.itemtypeManagerToolStripMenuItem.Text = "Itemtype Manager";
            // 
            // magictypeManagerToolStripMenuItem
            // 
            this.magictypeManagerToolStripMenuItem.Name = "magictypeManagerToolStripMenuItem";
            this.magictypeManagerToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.magictypeManagerToolStripMenuItem.Text = "Magictype Manager";
            this.magictypeManagerToolStripMenuItem.Click += new System.EventHandler(this.magictypeManagerToolStripMenuItem_Click);
            // 
            // goodsManagerToolStripMenuItem
            // 
            this.goodsManagerToolStripMenuItem.Name = "goodsManagerToolStripMenuItem";
            this.goodsManagerToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.goodsManagerToolStripMenuItem.Text = "Goods Manager";
            this.goodsManagerToolStripMenuItem.Click += new System.EventHandler(this.goodsManagerToolStripMenuItem_Click);
            // 
            // emoneyShopV2ToolStripMenuItem
            // 
            this.emoneyShopV2ToolStripMenuItem.Name = "emoneyShopV2ToolStripMenuItem";
            this.emoneyShopV2ToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.emoneyShopV2ToolStripMenuItem.Text = "EmoneyShopV2";
            this.emoneyShopV2ToolStripMenuItem.Click += new System.EventHandler(this.emoneyShopV2ToolStripMenuItem_Click);
            // 
            // honorShopManagerToolStripMenuItem
            // 
            this.honorShopManagerToolStripMenuItem.Name = "honorShopManagerToolStripMenuItem";
            this.honorShopManagerToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.honorShopManagerToolStripMenuItem.Text = "Honor Shop Manager";
            this.honorShopManagerToolStripMenuItem.Click += new System.EventHandler(this.honorShopManagerToolStripMenuItem_Click);
            // 
            // gameMapdatToolStripMenuItem
            // 
            this.gameMapdatToolStripMenuItem.Name = "gameMapdatToolStripMenuItem";
            this.gameMapdatToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.gameMapdatToolStripMenuItem.Text = "GameMap.dat";
            this.gameMapdatToolStripMenuItem.Click += new System.EventHandler(this.gameMapdatToolStripMenuItem_Click);
            // 
            // mapDBEditorToolStripMenuItem
            // 
            this.mapDBEditorToolStripMenuItem.Name = "mapDBEditorToolStripMenuItem";
            this.mapDBEditorToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.mapDBEditorToolStripMenuItem.Text = "Map DB Editor";
            this.mapDBEditorToolStripMenuItem.Click += new System.EventHandler(this.mapDBEditorToolStripMenuItem_Click);
            // 
            // leechingToolsToolStripMenuItem
            // 
            this.leechingToolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionsGrabberToolStripMenuItem,
            this.itemtypeEncryptDescryptToolStripMenuItem,
            this.magicTypeOperationToolStripMenuItem});
            this.leechingToolsToolStripMenuItem.Name = "leechingToolsToolStripMenuItem";
            this.leechingToolsToolStripMenuItem.Size = new System.Drawing.Size(97, 23);
            this.leechingToolsToolStripMenuItem.Text = "Leeching Tools";
            // 
            // actionsGrabberToolStripMenuItem
            // 
            this.actionsGrabberToolStripMenuItem.Name = "actionsGrabberToolStripMenuItem";
            this.actionsGrabberToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.actionsGrabberToolStripMenuItem.Text = "Actions Grabber";
            this.actionsGrabberToolStripMenuItem.Click += new System.EventHandler(this.actionsGrabberToolStripMenuItem_Click);
            // 
            // itemtypeEncryptDescryptToolStripMenuItem
            // 
            this.itemtypeEncryptDescryptToolStripMenuItem.Name = "itemtypeEncryptDescryptToolStripMenuItem";
            this.itemtypeEncryptDescryptToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.itemtypeEncryptDescryptToolStripMenuItem.Text = "Itemtype Encrypt/Decrypt";
            this.itemtypeEncryptDescryptToolStripMenuItem.Click += new System.EventHandler(this.itemtypeEncryptDescryptToolStripMenuItem_Click);
            // 
            // magicTypeOperationToolStripMenuItem
            // 
            this.magicTypeOperationToolStripMenuItem.Name = "magicTypeOperationToolStripMenuItem";
            this.magicTypeOperationToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.magicTypeOperationToolStripMenuItem.Text = "MagicTypeOperation";
            this.magicTypeOperationToolStripMenuItem.Click += new System.EventHandler(this.magicTypeOperationToolStripMenuItem_Click);
            // 
            // tscbServer
            // 
            this.tscbServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tscbServer.Name = "tscbServer";
            this.tscbServer.Size = new System.Drawing.Size(121, 23);
            this.tscbServer.SelectedIndexChanged += new System.EventHandler(this.tscbServer_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(830, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // racePointShopToolStripMenuItem
            // 
            this.racePointShopToolStripMenuItem.Name = "racePointShopToolStripMenuItem";
            this.racePointShopToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.racePointShopToolStripMenuItem.Text = "Race Point Shop";
            this.racePointShopToolStripMenuItem.Click += new System.EventHandler(this.racePointShopToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.topMenuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.topMenuStrip;
            this.Name = "FrmMain";
            this.Text = "World Conquer Online - Server Management Tool - v1.0.0.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.topMenuStrip.ResumeLayout(false);
            this.topMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip topMenuStrip;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extractSQLScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eventsCreatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemtypeManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem magictypeManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goodsManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leechingToolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actionsGrabberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemtypeEncryptDescryptToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox tscbServer;
        private System.Windows.Forms.ToolStripMenuItem gameMapdatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emoneyShopV2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem honorShopManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapDBEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem magicTypeOperationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem racePointShopToolStripMenuItem;
    }
}

