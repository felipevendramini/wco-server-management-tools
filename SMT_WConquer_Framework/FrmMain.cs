﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmMain.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Linq;
using System.Windows.Forms;
using SMT_WConquer_Framework.ChildrenForms;
using SMT_WConquer_Framework.MyControls;
using SMT_WConquer_Framework.Structures.Leecher;
using SMT_WConquer_Framework.Structures.Magics;
using SMT_WConquer_Framework.Structures.Maps;

#endregion

namespace SMT_WConquer_Framework
{
    public partial class FrmMain : Form
    {
        private bool m_forceClose = false;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;

            Kernel.Configuration = new ServerConfiguration();

            foreach (var server in ServerConfiguration.GameServersDict.Values)
            {
                tscbServer.Items.Add($"{server.ServerName}");
            }

            if (tscbServer.Items.Count > 0)
            {
                tscbServer.SelectedIndex = 0;
                ServerConfiguration.SelectedServerDatabase = ServerConfiguration.GameServersDict.Values.FirstOrDefault();
            }
        }

        #region Dialog Management

        public void OpenDialog(Form form, bool isMdiChild = true)
        {
            if (form == null)
                return;

            if (HasDialog(form))
            {
                Form temp = GetDialog(form.GetType());
                if (temp is MyForm frm && !frm.AllowMultiple)
                {
                    temp?.Show();
                    return;
                }
            }

            if (isMdiChild)
                form.MdiParent = this;

            form.Show();
        }

        public bool HasDialog(Form form)
        {
            return MdiChildren.Any(x => x.GetType() == form.GetType());
        }

        public Form GetDialog(Type formType)
        {
            return MdiChildren.FirstOrDefault(x => x.GetType() == formType);
        }

        #endregion

        #region Form Events

        private async void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!m_forceClose && CloseFormDlg() == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            foreach (var form in MdiChildren.Where(x => x is MyForm).Cast<MyForm>())
            {
                await form.SaveAsync(e.CloseReason);
            }
        }

        #endregion

        #region Dialogs

        private DialogResult CloseFormDlg()
        {
            return MessageBox.Show(this, @"Are you sure you want to close the tool?", @"Closing",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        #endregion

        private void eventsCreatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmActionTask());
        }

        private void actionsGrabberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmActionLeecher());
        }

        private void itemtypeEncryptDescryptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmItemtypeLeecher());
        }

        private void goodsManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmGoods());
        }

        private void tscbServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tscbServer.SelectedIndex < 0 || tscbServer.SelectedIndex >= tscbServer.Items.Count)
                return;

            ServerConfiguration.SelectedServerDatabase = ServerConfiguration.GameServersDict.Values.FirstOrDefault(x =>
                x.ServerName.Equals(tscbServer.Items[tscbServer.SelectedIndex].ToString(),
                    StringComparison.InvariantCultureIgnoreCase));

            foreach (var children in MdiChildren.Where(x => x is MyForm).Cast<MyForm>())
            {
                _ = children.OnTargetDatabaseChangeAsync(ServerConfiguration.SelectedServerDatabase).ConfigureAwait(false);
            }
        }

        private void gameMapdatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmGameMap());
        }

        private void emoneyShopV2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmEmoneyShop());
        }

        private void honorShopManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmHonorShop());
        }

        private void magictypeManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmMagictypeLeecher());
        }

        private void mapDBEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmMapEditDatabase());
        }

        private void magicTypeOperationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmMagicTypeOp());
        }

        private void racePointShopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenDialog(new FrmRacePointShop());
        }
    }
}