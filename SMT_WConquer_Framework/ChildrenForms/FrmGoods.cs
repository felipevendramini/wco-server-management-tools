﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmGoods.cs
// 
// Description: <File Description>
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SMT_WConquer_Framework.Database;
using SMT_WConquer_Framework.Database.Entities;
using SMT_WConquer_Framework.Models;
using SMT_WConquer_Framework.Structures;

#endregion

namespace SMT_WConquer_Framework.ChildrenForms
{
    public partial class FrmGoods : Form
    {
        private Dictionary<int, GoodsShopModel> m_dicGoods = new();
        private Dictionary<int, GoodsShopModel.GoodsItemType> m_dicItemtypes = new();
        private Dictionary<int, NpcInfo> m_dicNpcs = new();

        private struct NpcInfo
        {
            public uint Identity { get; set; }
            public string Name { get; set; }
        }

        public FrmGoods()
        {
            InitializeComponent();
        }

        private async void FrmGoods_Load(object sender, EventArgs e)
        {
            await LoadDataAsync();

            btnItemtypeSearch_Click(null, null);
        }

        private async Task LoadDataAsync()
        {
            m_dicItemtypes.Clear();
            m_dicNpcs.Clear();

            var itemtypes = await DatabaseContext.SelectAsync("SELECT id, `name` FROM `cq_itemtype` ORDER BY id ASC", null);
            foreach (DataRow row in itemtypes.Rows)
            {
                GoodsShopModel.GoodsItemType item = new GoodsShopModel.GoodsItemType
                {
                    Type = (int)row.Field<uint>("id"),
                    Name = row.Field<string>("name")
                };
                if (!m_dicItemtypes.ContainsKey(item.Type))
                    m_dicItemtypes.Add(item.Type, item);
            }

            await using DatabaseContext ctx = new DatabaseContext(ServerConfiguration.SelectedServerDatabase);
            var dbNpcs = await ctx.SelectAsync("SELECT `id`,`name` FROM cq_npc");
            foreach (DataRow row in dbNpcs.Rows)
            {
                NpcInfo info = new NpcInfo
                {
                    Identity = row.Field<uint>("id"),
                    Name = row.Field<string>("name")
                };

                if (!m_dicNpcs.ContainsKey((int) info.Identity))
                    m_dicNpcs.Add((int) info.Identity, info);
            }
        }

        private async void btnLoadGoodsDb_Click(object sender, EventArgs e)
        {
            m_dicGoods.Clear();
            lbxShops.Items.Clear();
            lbxItems.Items.Clear();

            btnItemDown.Enabled = false;
            btnItemUp.Enabled = false;

            var dbGoods = await DbGoods.GetAsync(ServerConfiguration.SelectedServerDatabase);
            foreach (var goods in dbGoods)
            {
                if (!m_dicGoods.TryGetValue((int) goods.OwnerIdentity, out var model))
                {
                    model = new GoodsShopModel
                    {
                        Moneytype = (GoodsShopModel.MoneyType)goods.Moneytype,
                        ShopIdentity = (int)goods.OwnerIdentity,
                        ShopName = m_dicNpcs.Values.FirstOrDefault(x => x.Identity == goods.OwnerIdentity).Name,
                        Type = 1,
                        Items = new List<GoodsShopModel.GoodsItemType>()
                    };

                    m_dicGoods.Add(model.ShopIdentity, model);
                    lbxShops.Items.Add($"{model.ShopIdentity} - {model.ShopName}");
                }

                model.Items.Add(new GoodsShopModel.GoodsItemType
                {
                    Type = (int) goods.Itemtype,
                    Name = m_dicItemtypes.Values.FirstOrDefault(x => x.Type == goods.Itemtype).Name
                });
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                AddExtension = false,
#if !DEBUG
                InitialDirectory = Environment.CurrentDirectory,
#else
                InitialDirectory = @"D:\World Conquer\NewConquer\ini",
#endif
                Multiselect = false
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;
            
            txtGoodsFile.Text = open.FileName;

            btnItemDown.Enabled = false;
            btnItemUp.Enabled = false;

            m_dicGoods.Clear();
            lbxShops.Items.Clear();
            lbxItems.Items.Clear();

            IniParser ini = new IniParser(txtGoodsFile.Text);
            int amount = ini.GetInt("Header", "Amount");
            for (int i = 0; i < amount; i++)
            {
                string section = $"Shop{i}";
                GoodsShopModel model = new GoodsShopModel
                {
                    Moneytype = (GoodsShopModel.MoneyType)ini.GetInt(section, "MoneyType"),
                    ShopIdentity = ini.GetInt(section, "ID"),
                    ShopName = ini.GetString(section, "Name"),
                    Type = ini.GetInt(section, "Type")
                };
                int itemAmount = ini.GetInt(section, "ItemAmount");

                for (int x = 0; x < itemAmount; x++)
                {
                    int type = ini.GetInt(section, $"Item{x}");
                    if (!m_dicItemtypes.TryGetValue(type, out var itemtype))
                    {
                        itemtype = new GoodsShopModel.GoodsItemType
                        {
                            Name = "Invalid",
                            Type = type
                        };
                    }
                    model.Items.Add(itemtype);
                }

                if (!m_dicGoods.ContainsKey(model.ShopIdentity))
                {
                    m_dicGoods.Add(model.ShopIdentity, model);
                    lbxShops.Items.Add($"{model.ShopIdentity} - {model.ShopName}");
                }
            }
        }

        private void lbxShops_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxShops.SelectedIndex < 0 || lbxShops.SelectedIndex >= lbxShops.Items.Count)
                return;

            RefreshItemsBox();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex < 0 || lbxItems.SelectedIndex >= lbxItems.Items.Count)
                return;

            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];
            int type = int.Parse(lbxItems.Items[lbxItems.SelectedIndex].ToString().Split('-')[0].Trim());
            shop.Items.RemoveAll(x => x.Type == type);
            lbxItems.Items.RemoveAt(lbxItems.SelectedIndex);
        }

        private void lbxItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex < 0 || lbxItems.SelectedIndex >= lbxItems.Items.Count)
            {
                btnItemDown.Enabled = false;
                btnItemUp.Enabled = false;
            }
            else if (lbxItems.SelectedIndex == 0)
            {
                btnItemDown.Enabled = true;
                btnItemUp.Enabled = false;
            }
            else if (lbxItems.SelectedIndex == lbxItems.Items.Count - 1)
            {
                btnItemDown.Enabled = false;
                btnItemUp.Enabled = true;
            }
            else
            {
                btnItemDown.Enabled = true;
                btnItemUp.Enabled = true;
            }
        }

        private void btnItemUp_Click(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex <= 0)
                return;

            int currentIdx = lbxItems.SelectedIndex;
            string item = lbxItems.Items[lbxItems.SelectedIndex].ToString();
            int type = int.Parse(item.Split('-')[0].Trim());
            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];

            GoodsShopModel.GoodsItemType itemtype = shop.Items.FirstOrDefault(x => x.Type == type);
            if (itemtype.Type == 0)
                return;

            shop.Items.Remove(itemtype);
            shop.Items.Insert(currentIdx - 1, itemtype);

            RefreshItemsBox();

            lbxItems.SelectedIndex = currentIdx - 1;
        }

        private void btnItemDown_Click(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex >= lbxItems.Items.Count - 1)
                return;

            int currentIdx = lbxItems.SelectedIndex;
            string item = lbxItems.Items[lbxItems.SelectedIndex].ToString();
            int type = int.Parse(item.Split('-')[0].Trim());
            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];

            GoodsShopModel.GoodsItemType itemtype = shop.Items.FirstOrDefault(x => x.Type == type);
            if (itemtype.Type == 0)
                return;

            shop.Items.Remove(itemtype);
            shop.Items.Insert(currentIdx + 1, itemtype);

            RefreshItemsBox();

            lbxItems.SelectedIndex = currentIdx + 1;
        }

        private void RefreshItemsBox()
        {
            lbxItems.Items.Clear();
            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];
            foreach (var item in shop.Items)
            {
                lbxItems.Items.Add($"{item.Type} - {item.Name}");
            }

            btnItemDown.Enabled = false;
            btnItemUp.Enabled = false;
        }

        private void btnItemtypeSearch_Click(object sender, EventArgs e)
        {
            lbxItemtype.Items.Clear();

            if (string.IsNullOrEmpty(txtItemtypeSearch.Text.Trim()))
            {
                foreach (var itemtype in m_dicItemtypes.Values)
                {
                    lbxItemtype.Items.Add($"{itemtype.Type} - {itemtype.Name}");
                }

                return;
            }

            foreach (var itemtype in m_dicItemtypes.Values
                .Where(x => x.Type.ToString().ToLowerInvariant().Contains(txtItemtypeSearch.Text.ToLowerInvariant()) ||
                            x.Name.ToLowerInvariant().Contains(txtItemtypeSearch.Text.ToLowerInvariant())))
            {
                lbxItemtype.Items.Add($"{itemtype.Type} - {itemtype.Name}");
            }
        }

        private void btnExportSQL_Click(object sender, EventArgs e)
        {
            var open = new SaveFileDialog
            {
                AddExtension = false,
                InitialDirectory = Environment.CurrentDirectory,
                DefaultExt = "sql"
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            using StreamWriter writer = new StreamWriter(open.OpenFile());
            
            try
            {
                foreach (var shop in m_dicGoods.Values)
                {
                    foreach (GoodsShopModel.GoodsItemType item in shop.Items)
                    {
                        writer.WriteLine($"INSERT INTO `cq_goods` (`ownerid`,`itemtype`,`moneytype`,`monopoly`) VALUES ({shop.ShopIdentity}, {item.Type}, {(int)shop.Moneytype}, 0);");
                    }

                    writer.WriteLine("");
                }
            }
            finally
            {
                writer.Flush();
                writer.Close();
            }
        }

        private void btnExportDAT_Click(object sender, EventArgs e)
        {
            var open = new SaveFileDialog
            {
                AddExtension = false,
                InitialDirectory = Environment.CurrentDirectory,
                DefaultExt = "dat"
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            using StreamWriter writer = new StreamWriter(open.OpenFile());
            writer.WriteLine("[Header]");
            writer.WriteLine($"Amount={m_dicGoods.Values.Count}");
            writer.WriteLine("");
            try
            {
                int idx = 0;
                foreach (var shop in m_dicGoods.Values)
                {
                    writer.WriteLine($"[Shop{idx++}]");
                    writer.WriteLine($"ID={shop.ShopIdentity}");
                    writer.WriteLine($"Name={shop.ShopName}");
                    writer.WriteLine($"Type={shop.Type}");
                    writer.WriteLine($"MoneyType={(int)shop.Moneytype}");
                    writer.WriteLine($"ItemAmount={shop.Items.Count}");

                    int i = 0;
                    foreach (GoodsShopModel.GoodsItemType item in shop.Items)
                    {
                        writer.WriteLine($"Item{i++}={item.Type}");
                    }

                    writer.WriteLine("");
                }
            }
            finally
            {
                writer.Flush();
                writer.Close();
            }
        }

        private void btnItemAdd_Click(object sender, EventArgs e)
        {

        }

        private void lbxItems_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void lbxItems_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                btnRemove_Click(sender, e);
        }
    }
}