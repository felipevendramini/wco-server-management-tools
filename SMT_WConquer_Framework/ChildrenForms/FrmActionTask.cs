﻿// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) FTW! Masters
// Keep the headers and the patterns adopted by the project. If you changed anything in the file just insert
// your name below, but don't remove the names of who worked here before.
// 
// Server Management Tools - SMT_WConquer_Framework - FrmActionTask.cs
// Description:
// 
// Creator: FELIPEVIEIRAVENDRAMI [FELIPE VIEIRA VENDRAMINI]
// 
// Developed by:
// Felipe Vieira Vendramini <felipevendramini@live.com>
// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using MindFusion.Diagramming;
using SMT_WConquer_Framework.MyControls;
using SMT_WConquer_Framework.Structures;
using SMT_WConquer_Framework.Structures.Actions;
using SMT_WConquer_Framework.Structures.Actions.Nodes.ActionBroadcastMsg;
using SMT_WConquer_Framework.Structures.Actions.Nodes.ActionMenu;
using SMT_WConquer_Framework.Structures.Actions.Nodes.ActionRand;
using SMT_WConquer_Framework.Structures.Actions.Nodes.ActionRandAction;

#endregion

namespace SMT_WConquer_Framework.ChildrenForms
{
    public partial class FrmActionTask : MyForm
    {
        public static int CurrentActionIdx = 0;

        private string m_saveFileName = string.Empty;

        private readonly List<MyActionNode> m_currentNodes = new();

        public FrmActionTask()
        {
            CurrentActionIdx = 0;
            InitializeComponent();
        }

        private void FrmActionTask_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;

            actionDiagram.AllowUnanchoredLinks = false;
            actionDiagram.AllowSelfLoops = false;
            actionDiagram.AllowLinksRepeat = true;
            actionDiagram.AllowMultipleResize = false;
        }

        private void ActionDiagram_LinkCreated(object sender, LinkEventArgs e)
        {
            if (e.Link.Origin.Tag is MyActionNode senderNode && e.Link.Destination.Tag is MyActionNode targetNode)
            {
                e.Link.Tag = targetNode;

                int index = (int) Math.Ceiling(senderNode.Table[1, 2].Text.Length / 128f) + e.Link.OriginIndex - 3;
                if (e.Link.Origin is TableNode)
                {
                    senderNode.Childrens[index].Next = targetNode;
                }
                else
                {
                    if (e.Link.OriginIndex == 1)
                    {
                        senderNode.Next = senderNode.Childrens[0].Next = targetNode;
                    }
                    else
                    {
                        senderNode.Fail = senderNode.Childrens[0].Fail = targetNode;
                    }
                }
            }
        }

        private void ActionDiagram_LinkDeleted(object sender, LinkEventArgs e)
        {
            if (e.Link.Origin.Tag is MyActionNode senderNode && e.Link.Destination.Tag is MyActionNode)
            {
                e.Link.Tag = null;

                int index = (int)Math.Ceiling(senderNode.Table[1, 2].Text.Length / 128f) + e.Link.OriginIndex - 3;
                if (e.Link.Origin is TableNode)
                {
                    senderNode.Childrens[index].Next = null;
                }
                else if (e.Link.Origin is ContainerNode)
                {
                    if (e.Link.OriginIndex == 1)
                    {
                        senderNode.Next = senderNode.Childrens[0].Next = null;
                    }
                    else
                    {
                        senderNode.Fail = senderNode.Childrens[0].Fail = null;
                    }
                }
            }
        }

        private void ActionDiagram_NodeDeleted(object sender, NodeEventArgs e)
        {
            if (e.Node is ContainerNode container && container.Tag is MyActionNode node)
            {
                m_currentNodes.RemoveAll(x => x.Identity == node.Identity);
            }
        }

        private void ActionDiagram_NodeDoubleClicked(object sender, NodeEventArgs e)
        {
            if (e.Node?.Tag is MyActionNode node)
            {
                node.Edit();
            }
        }
        
        private void ExportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (m_currentNodes.Count == 0)
            {
                MessageBox.Show(this, @"You doesn't have any actions to export.");
                return;
            }

            ContainerNode containerNode = (ContainerNode)actionDiagram.Nodes.FirstOrDefault(x => x is ContainerNode);
            MyActionNode container = (MyActionNode)containerNode?.Tag;

            if (container == null)
            {
                MessageBox.Show(this, @"Error trying to cast object type.");
                return;
            }

            actionDiagram.ActiveItem = containerNode;

            string currentDirectory = Environment.CurrentDirectory + @"\ExportedActions";
            if (!Directory.Exists(currentDirectory))
            {
                Directory.CreateDirectory(currentDirectory);
            }

            SaveFileDialog saveFileDialog = new()
            {
                AddExtension = true,
                InitialDirectory = currentDirectory
            };

            if (saveFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            new FrmActionPreviewSql(saveFileDialog.FileName, m_currentNodes).ShowDialog(this);
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(m_saveFileName))
            {
                SaveAsToolStripMenuItem_Click(sender, e);
                return;
            }

            SaveToXml(m_saveFileName);
            MessageBox.Show(this, @"Saved!");
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new()
            {
                Filter = @"Action XML Document (.axml)|*.axml",
                FilterIndex = 0,
                AddExtension = true,
                DefaultExt = ".axml"
            };

            if (saveFile.ShowDialog() != DialogResult.OK)
                return;

            SaveToXml(m_saveFileName = saveFile.FileName);
        }

        private void SaveToXml(string path)
        {
            MyXml xml = new(path);
            foreach (var node in m_currentNodes)
            {
                string currentNode = $"Node[@id='{node.Identity}']";
                xml.AddNewNode("", node.Identity.ToString(), "Node", "Nodes");
                xml.AddNewNode(node.Identity, null, "Identity", "Nodes", currentNode);
                xml.AddNewNode((int)node.ActionType, null, "ActionType", "Nodes", currentNode);
                xml.AddNewNode(node.Next?.Identity ?? -1, null, "Next", "Nodes", currentNode);
                xml.AddNewNode(node.Fail?.Identity ?? -1, null, "Fail", "Nodes", currentNode);
                xml.AddNewNode(node.IsEntryPoint ? 1 : 0, null, "EntryPoint", "Nodes", currentNode);

                xml.AddNewNode(node.Container.Bounds.Size.Width, null, "Width", "Nodes", currentNode);
                xml.AddNewNode(node.Container.Bounds.Size.Height, null, "Height", "Nodes", currentNode);
                xml.AddNewNode(node.Container.Bounds.Location.X, null, "X", "Nodes", currentNode);
                xml.AddNewNode(node.Container.Bounds.Location.Y, null, "Y", "Nodes", currentNode);

                xml.AddNewNode("", null, "Childs", "Nodes", currentNode);
                int i = 0;
                foreach (var child in node.Childrens)
                {
                    string currentChild = $"Child[@id='{i}']";
                    xml.AddNewNode("", i.ToString(), "Child", "Nodes", currentNode, "Childs");
                    xml.AddNewNode(child.Identity, null, "Identity", "Nodes", currentNode, "Childs", currentChild);
                    xml.AddNewNode(child.Next?.Identity ?? -1, null, "Next", "Nodes", currentNode, "Childs", currentChild);
                    xml.AddNewNode(child.Fail?.Identity ?? -1, null, "Fail", "Nodes", currentNode, "Childs", currentChild);
                    xml.AddNewNode((int)child.Type, null, "Type", "Nodes", currentNode, "Childs", currentChild);
                    xml.AddNewNode(child.Data, null, "Data", "Nodes", currentNode, "Childs", currentChild);
                    xml.AddNewNode(child.Param, null, "Param", "Nodes", currentNode, "Childs", currentChild);
                    i++;
                }
            }
            xml.Save();
        }

        private void OpenFromXml(string path)
        {
            actionDiagram.ClearAll();

            MyXml xml = new(path);
            foreach (XmlNode node in xml.GetAllNodes("Nodes"))
            {
                int id = int.Parse(node.Attributes["id"].Value);
                string currentNode = $"Node[@id='{id}']";

                float w = float.Parse(xml.GetValue("Nodes", currentNode, "Width")) - 6,
                    h = float.Parse(xml.GetValue("Nodes", currentNode, "Height")) - 6,
                    x = float.Parse(xml.GetValue("Nodes", currentNode, "X")),
                    y = float.Parse(xml.GetValue("Nodes", currentNode, "Y"));
                
                RectangleF rect = new(x, y, w, h);
                MyActionNode dlg;
                var actionType = (GameAction.TaskActionType) int.Parse(xml.GetValue("Nodes", $"Node[@id='{id}']", "ActionType"));
                switch (actionType)
                {
                    case GameAction.TaskActionType.ActionMenutext:
                    {
                        dlg = new ActionMenuDlg(actionDiagram);
                        break;
                    }
                    case GameAction.TaskActionType.ActionRand:
                    {
                        dlg = new ActionRandDlg(actionDiagram);
                        break;
                    }
                    case GameAction.TaskActionType.ActionBrocastmsg:
                    {
                        dlg = new ActionBroadcastMsgDlg(actionDiagram);
                        break;
                    }
                    default:
                        continue;
                }

                List<CqAction> actions = new();
                foreach (XmlNode child in xml.GetAllNodes("Nodes", currentNode, "Childs"))
                {
                    string currentChild = $"Child[@id='{child.Attributes["id"].Value}']";
                    actions.Add(new CqAction
                    {
                        Identity = int.Parse(xml.GetValue("Nodes", currentNode, "Childs", currentChild, "Identity")),
                        NextIdx = int.Parse(xml.GetValue("Nodes", currentNode, "Childs", currentChild, "Next")),
                        FailIdx = int.Parse(xml.GetValue("Nodes", currentNode, "Childs", currentChild, "Fail")),
                        Type = (GameAction.TaskActionType)int.Parse(xml.GetValue("Nodes", currentNode, "Childs", currentChild, "Type")),
                        Data = uint.Parse(xml.GetValue("Nodes", currentNode, "Childs", currentChild, "Data")),
                        Param = xml.GetValue("Nodes", currentNode, "Childs", currentChild, "Param"),
                        Node = dlg
                    });
                }

                dlg.Identity = id;
                dlg.NextIdx = int.Parse(xml.GetValue("Nodes", currentNode, "Next"));
                dlg.FailIdx = int.Parse(xml.GetValue("Nodes", currentNode, "Fail"));
                dlg.IsEntryPoint = int.Parse(xml.GetValue("Nodes", currentNode, "EntryPoint")) != 0;

                dlg.Create(actionType, rect);
                dlg.Fill(actions);
                dlg.Render();
                m_currentNodes.Add(dlg);
            }

            CurrentActionIdx = m_currentNodes.Max(x => x.Identity) + 1;

            // re-building links
            foreach (var node in m_currentNodes)
            {
                if (node.NextIdx > -1
                    && node.ActionType != GameAction.TaskActionType.ActionMenutext)
                {
                    node.Next = m_currentNodes.FirstOrDefault(x => x.Identity == node.NextIdx);
                    if (node.Next != null)
                    {
                        actionDiagram.Links.Add(actionDiagram.Factory.CreateDiagramLink(node.Container, node.Next.Container));
                    }
                }

                if (node.FailIdx > -1
                    && node.ActionType != GameAction.TaskActionType.ActionMenutext)
                {
                    node.Fail = m_currentNodes.FirstOrDefault(x => x.Identity == node.FailIdx);
                    if (node.Fail != null)
                    {
                        actionDiagram.Links.Add(actionDiagram.Factory.CreateDiagramLink(node.Container, node.Fail.Container));
                    }
                }

                if (node.ActionType == GameAction.TaskActionType.ActionMenutext)
                {
                    int idx = 3;
                    foreach (var child in node.Childrens)
                    {
                        if (child.NextIdx > -1 
                         && (child.Type == GameAction.TaskActionType.ActionMenulink
                         || child.Type == GameAction.TaskActionType.ActionMenuedit))
                        {
                            child.Next = m_currentNodes.FirstOrDefault(x => x.Identity == child.NextIdx);
                            if (child.Next != null && idx < node.Table.Rows.Count)
                            {
                                actionDiagram.Links.Add(actionDiagram.Factory.CreateDiagramLink(node.Table.Rows[idx].Table, idx, child.Next.Container));
                            }
                            idx++;
                        }
                    }
                }
            }
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using OpenFileDialog openFile = new()
            {
                Filter = @"Action XML Document (.axml)|*.axml",
                FilterIndex = 0,
                AddExtension = true,
                DefaultExt = ".axml"
            };

            if (openFile.ShowDialog(this) != DialogResult.OK)
                return;

            m_saveFileName = openFile.FileName;

            OpenFromXml(m_saveFileName);
        }

        private PointF GetMousePosition()
        {
            return actionDiagramView.ClientToDoc(contextMenuStrip1.Bounds.Location);
        }

        /// <summary>
        /// 101
        /// </summary>
        private void DialogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionMenuDlg dlg = new(actionDiagram);
            dlg.Create(GameAction.TaskActionType.ActionMenutext, new RectangleF(GetMousePosition(), MyActionNode.DefaultSize.Size));
            dlg.Render();

            m_currentNodes.Add(dlg);
        }

        /// <summary>
        /// 121
        /// </summary>
        private void ChanceCalculationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionRandDlg dlg = new(actionDiagram);
            dlg.Create(GameAction.TaskActionType.ActionRand);
            dlg.Render();

            m_currentNodes.Add(dlg);
        }

        /// <summary>
        /// 122
        /// </summary>
        private void RandomActionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ActionRandActionDlg dlg = new(actionDiagram);
            dlg.Create(GameAction.TaskActionType.ActionRandaction);
            dlg.Render();

            m_currentNodes.Add(dlg);
        }

        /// <summary>
        /// 125
        /// </summary>
        private void SendSystemMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActionBroadcastMsgDlg dlg = new(actionDiagram);
            dlg.Create(GameAction.TaskActionType.ActionBrocastmsg);
            dlg.Render();

            m_currentNodes.Add(dlg);
        }
    }
}