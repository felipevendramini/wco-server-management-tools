﻿using System;
using System.IO;
using System.Windows.Forms;

namespace SMT_WConquer_Framework.ChildrenForms
{
    public partial class FrmHonorShop : Form
    {
        public FrmHonorShop()
        {
            InitializeComponent();
        }

        private void BtnToSql_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                AddExtension = false,
#if !DEBUG
                InitialDirectory = Environment.CurrentDirectory,
#else
                InitialDirectory = @"D:\World Conquer\NewConquer\ini",
#endif
                Multiselect = false
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            using StreamReader reader = new StreamReader(open.OpenFile());
            string directory = Path.GetDirectoryName(open.FileName);
            string fileName = Path.GetFileNameWithoutExtension(open.FileName);
            using StreamWriter writer = new StreamWriter(Path.Combine(directory, $"{fileName}.sql"), false);

            int rowCount = int.Parse(reader.ReadLine().Split('=')[1]);
            for (int i = 0; i < rowCount; i++)
            {
                string[] itemSplit = reader.ReadLine().Split(',');
                int itemType = int.Parse(itemSplit[0]);
                int price = int.Parse(itemSplit[1]);

                writer.WriteLine($"INSERT INTO cq_goods (ownerid, itemtype, honor_price) VALUES (6000, {itemType}, {price});");
            }
        }
    }
}
