﻿using MindFusion.Scheduling;
using SMT_WConquer_Framework.Database.Entities;
using SMT_WConquer_Framework.MyControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SMT_WConquer_Framework.ChildrenForms
{
    public partial class FrmMapEditDatabase : MyForm
    {
        private readonly string[] flagNames =
        {
            "Pk Field",             // 1
            "Change Map Disable",   // 2
            "Record Disable",       // 3
            "Pk Disable",           // 4
            "Booth Enable",         // 5
            "Team Disable",         // 6
            "Teleport Disable",     // 7
            "Guild Map",            // 8
            "Prison Map",           // 9
            "Wing Disable",         // 10
            "Family Map",           // 11
            "Mine Field",           // 12
            "Call Newbie Disable",  // 13
            "Reborn Now Enable",    // 14
            "Newbie Protect",       // 15
            "Unknown16",            // 16
            "Unknown17",            // 17
            "Unknown18",            // 18
            "Unknown19",            // 19
            "Unknown20",            // 20
            "Unknown21",            // 21
            "Unknown22",            // 22
            "Double PK Map",            // 23
            "Unknown24",            // 24
            "Unknown25",            // 25
            "Race Track Map",            // 26
            "Unknown27",            // 27
            "Family Arenic Map",            // 28
            "Faction PK Map",            // 29
            "Elite Map",            // 30
            "Unknown31",            // 31
            "Unknown32",            // 32
            "Team PK Arenic Map",            // 33
            "Unknown34",            // 34
            "Unknown35",            // 35
            "Unknown36",            // 36
            "Unknown37",            // 37
            "Team Arena Map",            // 38
            "Battle Effect Limit Map",            // 39
            "Team Pop PK Map",            // 40
            "No EXP Map",            // 41
            "Golden League Battle Limit",            // 42
            "Unknown43",            // 43
            "Unknown44",            // 44
            "Forbid Camp Map",            // 45
            "Golden League Map",            // 46
            "Jiang Hu Map",            // 47
            "Unknown48",            // 48
            "Unknown49",            // 49
            "Unknown50",            // 50
            "Unknown51",            // 51
            "Unknown52",            // 52
            "Unknown53",            // 53
            "Unknown54",            // 54
            "Unknown55",            // 55
            "Unknown56",            // 56
            "Unknown57",            // 57
            "Unknown58",            // 58
            "Unknown59",            // 59
            "Unknown60",            // 60
            "Unknown61",            // 61
            "Unknown62",            // 62
            "Unknown63",            // 63
            "Unknown64"             // 64
        };

        private Dictionary<uint, DbMap> maps = new();
        private DbMap selectedMap;

        public FrmMapEditDatabase()
        {
            InitializeComponent();
        }

        private async void FrmMapEditDatabase_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < flagNames.Length; i++)
            {
                CheckBox checkBox = new CheckBox
                {
                    Dock = DockStyle.Fill,
                    Name = $"MapTypeFlag{i}",
                    Text = $"0x{(1UL << i):X16} - {flagNames[i]}"
                };

                tableLayoutPanel1.Controls.Add(checkBox, i / 16, i % 16);
            }

            LoadMapsAsync();
        }

        private async void LoadMapsAsync()
        {
            maps.Clear();
            maps = (await DbMap.GetAsync()).ToDictionary(x => x.Identity);

            lbxMaps.Items.Clear();
            foreach (var map in maps.Values.OrderBy(x => x.MapDoc))
            {
                lbxMaps.Items.Add($"{map.Identity} [Doc:{map.MapDoc}] - {map.Name}");
            }
        }

        private uint SelectedMapId()
        {
            if (lbxMaps.SelectedIndex < 0 || lbxMaps.SelectedIndex >= lbxMaps.Items.Count)
            {
                selectedMap = null;
                return 0;
            }

            return uint.Parse(lbxMaps.Items[lbxMaps.SelectedIndex].ToString().Split(' ')[0]);
        }

        private void lbxMaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxMaps.SelectedIndex < 0 || lbxMaps.SelectedIndex >= lbxMaps.Items.Count)
            {
                selectedMap = null;
                return;
            }

            uint mapId = SelectedMapId();
            if (!maps.TryGetValue(mapId, out var map))
            {
                selectedMap = null;
                return;
            }

            selectedMap = map;

            FillMapTypeFlags(map.Type);
        }

        private void FillMapTypeFlags(ulong flag)
        {
            for (int i = 0; i < flagNames.Length; i++)
            {
                CheckBox checkBox = tableLayoutPanel1.Controls[$"MapTypeFlag{i}"] as CheckBox;
                if (checkBox == null)
                {
                    // hummm
                    Console.WriteLine("Unknown check box {}", i);
                    continue;
                }

                ulong fullFlag = (1UL << i);
                checkBox.Checked = (flag & fullFlag) == fullFlag;
            }
        }
    }
}
