﻿namespace SMT_WConquer_Framework.ChildrenForms
{
    partial class FrmActionTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.actionDiagramView = new MindFusion.Diagramming.WinForms.DiagramView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exportImportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.systemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.chanceCalculationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.randomActionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.checkTimeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sendSystemMessageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.executeSQLScriptToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nPCsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.layItemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dynamicNPCsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.syndicateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.familyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monstersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eventToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.trapsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.wantedToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.actionDiagram = new MindFusion.Diagramming.Diagram();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // actionDiagramView
            // 
            this.actionDiagramView.ContextMenuStrip = this.contextMenuStrip1;
            this.actionDiagramView.Diagram = this.actionDiagram;
            this.actionDiagramView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionDiagramView.LicenseKey = null;
            this.actionDiagramView.Location = new System.Drawing.Point(0, 0);
            this.actionDiagramView.Name = "actionDiagramView";
            this.actionDiagramView.Size = new System.Drawing.Size(1105, 670);
            this.actionDiagramView.TabIndex = 1;
            this.actionDiagramView.Text = "diagramView1";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportImportToolStripMenuItem,
            this.toolStripSeparator2,
            this.systemToolStripMenuItem1,
            this.nPCsToolStripMenuItem1,
            this.mapToolStripMenuItem1,
            this.layItemToolStripMenuItem1,
            this.itemToolStripMenuItem1,
            this.dynamicNPCsToolStripMenuItem1,
            this.syndicateToolStripMenuItem1,
            this.familyToolStripMenuItem,
            this.monstersToolStripMenuItem1,
            this.userToolStripMenuItem1,
            this.eventToolStripMenuItem1,
            this.trapsToolStripMenuItem1,
            this.wantedToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 340);
            // 
            // exportImportToolStripMenuItem
            // 
            this.exportImportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem1,
            this.saveToolStripMenuItem1,
            this.saveAsToolStripMenuItem1,
            this.toolStripSeparator3,
            this.exportToolStripMenuItem2});
            this.exportImportToolStripMenuItem.Name = "exportImportToolStripMenuItem";
            this.exportImportToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exportImportToolStripMenuItem.Text = "Export/Import";
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(186, 22);
            this.openToolStripMenuItem1.Text = "Open";
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(186, 22);
            this.saveToolStripMenuItem1.Text = "Save";
            this.saveToolStripMenuItem1.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem1
            // 
            this.saveAsToolStripMenuItem1.Name = "saveAsToolStripMenuItem1";
            this.saveAsToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem1.Size = new System.Drawing.Size(186, 22);
            this.saveAsToolStripMenuItem1.Text = "Save As";
            this.saveAsToolStripMenuItem1.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(183, 6);
            // 
            // exportToolStripMenuItem2
            // 
            this.exportToolStripMenuItem2.Name = "exportToolStripMenuItem2";
            this.exportToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.exportToolStripMenuItem2.Size = new System.Drawing.Size(186, 22);
            this.exportToolStripMenuItem2.Text = "Export";
            this.exportToolStripMenuItem2.Click += new System.EventHandler(this.ExportToolStripMenuItem1_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // systemToolStripMenuItem1
            // 
            this.systemToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.chanceCalculationToolStripMenuItem1,
            this.randomActionToolStripMenuItem1,
            this.checkTimeToolStripMenuItem1,
            this.sendSystemMessageToolStripMenuItem1,
            this.executeSQLScriptToolStripMenuItem1});
            this.systemToolStripMenuItem1.Name = "systemToolStripMenuItem1";
            this.systemToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.systemToolStripMenuItem1.Text = "System";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(219, 22);
            this.toolStripMenuItem2.Text = "[101] Dialog";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.DialogToolStripMenuItem_Click);
            // 
            // chanceCalculationToolStripMenuItem1
            // 
            this.chanceCalculationToolStripMenuItem1.Name = "chanceCalculationToolStripMenuItem1";
            this.chanceCalculationToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.chanceCalculationToolStripMenuItem1.Text = "[121] Chance Calculation";
            this.chanceCalculationToolStripMenuItem1.Click += new System.EventHandler(this.ChanceCalculationToolStripMenuItem_Click);
            // 
            // randomActionToolStripMenuItem1
            // 
            this.randomActionToolStripMenuItem1.Name = "randomActionToolStripMenuItem1";
            this.randomActionToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.randomActionToolStripMenuItem1.Text = "[122] Random Action";
            this.randomActionToolStripMenuItem1.Click += new System.EventHandler(this.RandomActionToolStripMenuItem1_Click);
            // 
            // checkTimeToolStripMenuItem1
            // 
            this.checkTimeToolStripMenuItem1.Name = "checkTimeToolStripMenuItem1";
            this.checkTimeToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.checkTimeToolStripMenuItem1.Text = "[123] Check Time";
            // 
            // sendSystemMessageToolStripMenuItem1
            // 
            this.sendSystemMessageToolStripMenuItem1.Name = "sendSystemMessageToolStripMenuItem1";
            this.sendSystemMessageToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.sendSystemMessageToolStripMenuItem1.Text = "[125] Send System Message";
            this.sendSystemMessageToolStripMenuItem1.Click += new System.EventHandler(this.SendSystemMessageToolStripMenuItem_Click);
            // 
            // executeSQLScriptToolStripMenuItem1
            // 
            this.executeSQLScriptToolStripMenuItem1.Name = "executeSQLScriptToolStripMenuItem1";
            this.executeSQLScriptToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.executeSQLScriptToolStripMenuItem1.Text = "[127] Execute SQL Script";
            // 
            // nPCsToolStripMenuItem1
            // 
            this.nPCsToolStripMenuItem1.Name = "nPCsToolStripMenuItem1";
            this.nPCsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.nPCsToolStripMenuItem1.Text = "NPCs";
            // 
            // mapToolStripMenuItem1
            // 
            this.mapToolStripMenuItem1.Name = "mapToolStripMenuItem1";
            this.mapToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.mapToolStripMenuItem1.Text = "Map";
            // 
            // layItemToolStripMenuItem1
            // 
            this.layItemToolStripMenuItem1.Name = "layItemToolStripMenuItem1";
            this.layItemToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.layItemToolStripMenuItem1.Text = "Lay Item";
            // 
            // itemToolStripMenuItem1
            // 
            this.itemToolStripMenuItem1.Name = "itemToolStripMenuItem1";
            this.itemToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.itemToolStripMenuItem1.Text = "Item";
            // 
            // dynamicNPCsToolStripMenuItem1
            // 
            this.dynamicNPCsToolStripMenuItem1.Name = "dynamicNPCsToolStripMenuItem1";
            this.dynamicNPCsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.dynamicNPCsToolStripMenuItem1.Text = "Dynamic NPCs";
            // 
            // syndicateToolStripMenuItem1
            // 
            this.syndicateToolStripMenuItem1.Name = "syndicateToolStripMenuItem1";
            this.syndicateToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.syndicateToolStripMenuItem1.Text = "Syndicate";
            // 
            // familyToolStripMenuItem
            // 
            this.familyToolStripMenuItem.Name = "familyToolStripMenuItem";
            this.familyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.familyToolStripMenuItem.Text = "Family";
            // 
            // monstersToolStripMenuItem1
            // 
            this.monstersToolStripMenuItem1.Name = "monstersToolStripMenuItem1";
            this.monstersToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.monstersToolStripMenuItem1.Text = "Monsters";
            // 
            // userToolStripMenuItem1
            // 
            this.userToolStripMenuItem1.Name = "userToolStripMenuItem1";
            this.userToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.userToolStripMenuItem1.Text = "User";
            // 
            // eventToolStripMenuItem1
            // 
            this.eventToolStripMenuItem1.Name = "eventToolStripMenuItem1";
            this.eventToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.eventToolStripMenuItem1.Text = "Event";
            // 
            // trapsToolStripMenuItem1
            // 
            this.trapsToolStripMenuItem1.Name = "trapsToolStripMenuItem1";
            this.trapsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.trapsToolStripMenuItem1.Text = "Traps";
            // 
            // wantedToolStripMenuItem1
            // 
            this.wantedToolStripMenuItem1.Name = "wantedToolStripMenuItem1";
            this.wantedToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.wantedToolStripMenuItem1.Text = "Wanted";
            // 
            // actionDiagram
            // 
            this.actionDiagram.AllowSelfLoops = false;
            this.actionDiagram.AutoResize = MindFusion.Diagramming.AutoResize.AllDirections;
            this.actionDiagram.AutoSnapDistance = 20F;
            this.actionDiagram.AutoSnapLinks = true;
            this.actionDiagram.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.actionDiagram.GridSizeX = 5F;
            this.actionDiagram.GridSizeY = 5F;
            this.actionDiagram.GridStyle = MindFusion.Diagramming.GridStyle.Lines;
            this.actionDiagram.RouteLinks = true;
            this.actionDiagram.ShowAnchors = MindFusion.Diagramming.ShowAnchors.Always;
            this.actionDiagram.ShowDisabledHandles = false;
            this.actionDiagram.ShowGrid = true;
            this.actionDiagram.SnapToAnchor = MindFusion.Diagramming.SnapToAnchor.OnCreateOrModify;
            this.actionDiagram.TouchThreshold = 0F;
            this.actionDiagram.LinkCreated += new System.EventHandler<MindFusion.Diagramming.LinkEventArgs>(this.ActionDiagram_LinkCreated);
            this.actionDiagram.LinkDeleted += new System.EventHandler<MindFusion.Diagramming.LinkEventArgs>(this.ActionDiagram_LinkDeleted);
            this.actionDiagram.NodeDeleted += new System.EventHandler<MindFusion.Diagramming.NodeEventArgs>(this.ActionDiagram_NodeDeleted);
            this.actionDiagram.NodeDoubleClicked += new System.EventHandler<MindFusion.Diagramming.NodeEventArgs>(this.ActionDiagram_NodeDoubleClicked);
            // 
            // FrmActionTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1105, 670);
            this.Controls.Add(this.actionDiagramView);
            this.Name = "FrmActionTask";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmActionTask";
            this.Load += new System.EventHandler(this.FrmActionTask_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MindFusion.Diagramming.WinForms.DiagramView actionDiagramView;
        private MindFusion.Diagramming.Diagram actionDiagram;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportImportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem chanceCalculationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem randomActionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem checkTimeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sendSystemMessageToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem executeSQLScriptToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nPCsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem layItemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dynamicNPCsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem syndicateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem familyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monstersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem eventToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem trapsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem wantedToolStripMenuItem1;
    }
}