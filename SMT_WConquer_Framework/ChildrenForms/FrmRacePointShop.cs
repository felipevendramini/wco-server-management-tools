﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMT_WConquer_Framework.ChildrenForms
{
    public partial class FrmRacePointShop : Form
    {
        public FrmRacePointShop()
        {
            InitializeComponent();
        }

        private void BtnToSql_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                AddExtension = false,
#if !DEBUG
                InitialDirectory = Environment.CurrentDirectory,
#else
                InitialDirectory = @"D:\World Conquer\NewConquer\ini",
#endif
                Multiselect = false
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            using StreamReader reader = new StreamReader(open.OpenFile());
            string directory = Path.GetDirectoryName(open.FileName);
            string fileName = Path.GetFileNameWithoutExtension(open.FileName);
            using StreamWriter writer = new StreamWriter(Path.Combine(directory, $"{fileName}.sql"), false);

            string row;
            while ((row = reader.ReadLine()) != null) 
            {
                if (row.Equals("[Normal]", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }
                if (row.Equals("[recommend]", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }
                if (string.IsNullOrEmpty(row))
                {
                    continue;
                }
                    
                string[] itemSplit = row.Split(' ');
                int itemType = int.Parse(itemSplit[0]);
                int price = int.Parse(itemSplit[1]);

                writer.WriteLine($"INSERT INTO cq_goods (ownerid, itemtype, riding_price) VALUES (6001, {itemType}, {price});");
            }
        }
    }
}
