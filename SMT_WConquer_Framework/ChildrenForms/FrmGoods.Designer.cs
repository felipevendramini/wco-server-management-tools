﻿namespace SMT_WConquer_Framework.ChildrenForms
{
    partial class FrmGoods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxShops = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtGoodsFile = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLoadGoodsDb = new System.Windows.Forms.Button();
            this.lbxItems = new System.Windows.Forms.ListBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnItemUp = new System.Windows.Forms.Button();
            this.btnItemDown = new System.Windows.Forms.Button();
            this.lbxItemtype = new System.Windows.Forms.ListBox();
            this.btnItemAdd = new System.Windows.Forms.Button();
            this.txtItemtypeSearch = new System.Windows.Forms.TextBox();
            this.btnItemtypeSearch = new System.Windows.Forms.Button();
            this.btnExportDAT = new System.Windows.Forms.Button();
            this.btnExportSQL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbxShops
            // 
            this.lbxShops.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbxShops.FormattingEnabled = true;
            this.lbxShops.Location = new System.Drawing.Point(12, 38);
            this.lbxShops.Name = "lbxShops";
            this.lbxShops.Size = new System.Drawing.Size(195, 394);
            this.lbxShops.TabIndex = 0;
            this.lbxShops.SelectedIndexChanged += new System.EventHandler(this.lbxShops_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "From File";
            // 
            // txtGoodsFile
            // 
            this.txtGoodsFile.Enabled = false;
            this.txtGoodsFile.Location = new System.Drawing.Point(72, 8);
            this.txtGoodsFile.Name = "txtGoodsFile";
            this.txtGoodsFile.Size = new System.Drawing.Size(308, 20);
            this.txtGoodsFile.TabIndex = 2;
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(386, 6);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 3;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(487, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "From Database";
            // 
            // btnLoadGoodsDb
            // 
            this.btnLoadGoodsDb.Location = new System.Drawing.Point(572, 6);
            this.btnLoadGoodsDb.Name = "btnLoadGoodsDb";
            this.btnLoadGoodsDb.Size = new System.Drawing.Size(75, 23);
            this.btnLoadGoodsDb.TabIndex = 6;
            this.btnLoadGoodsDb.Text = "Load";
            this.btnLoadGoodsDb.UseVisualStyleBackColor = true;
            this.btnLoadGoodsDb.Click += new System.EventHandler(this.btnLoadGoodsDb_Click);
            // 
            // lbxItems
            // 
            this.lbxItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbxItems.FormattingEnabled = true;
            this.lbxItems.Location = new System.Drawing.Point(213, 38);
            this.lbxItems.Name = "lbxItems";
            this.lbxItems.Size = new System.Drawing.Size(195, 394);
            this.lbxItems.TabIndex = 0;
            this.lbxItems.SelectedIndexChanged += new System.EventHandler(this.lbxItems_SelectedIndexChanged);
            this.lbxItems.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lbxItems_KeyPress);
            this.lbxItems.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lbxItems_KeyUp);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(414, 69);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 48);
            this.btnRemove.TabIndex = 7;
            this.btnRemove.Text = "&Remove Item";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnItemUp
            // 
            this.btnItemUp.Location = new System.Drawing.Point(414, 38);
            this.btnItemUp.Name = "btnItemUp";
            this.btnItemUp.Size = new System.Drawing.Size(75, 23);
            this.btnItemUp.TabIndex = 8;
            this.btnItemUp.Text = "Up";
            this.btnItemUp.UseVisualStyleBackColor = true;
            this.btnItemUp.Click += new System.EventHandler(this.btnItemUp_Click);
            // 
            // btnItemDown
            // 
            this.btnItemDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnItemDown.Location = new System.Drawing.Point(414, 409);
            this.btnItemDown.Name = "btnItemDown";
            this.btnItemDown.Size = new System.Drawing.Size(75, 23);
            this.btnItemDown.TabIndex = 8;
            this.btnItemDown.Text = "Down";
            this.btnItemDown.UseVisualStyleBackColor = true;
            this.btnItemDown.Click += new System.EventHandler(this.btnItemDown_Click);
            // 
            // lbxItemtype
            // 
            this.lbxItemtype.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxItemtype.FormattingEnabled = true;
            this.lbxItemtype.Location = new System.Drawing.Point(495, 64);
            this.lbxItemtype.Name = "lbxItemtype";
            this.lbxItemtype.Size = new System.Drawing.Size(388, 368);
            this.lbxItemtype.TabIndex = 9;
            // 
            // btnItemAdd
            // 
            this.btnItemAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnItemAdd.Location = new System.Drawing.Point(889, 64);
            this.btnItemAdd.Name = "btnItemAdd";
            this.btnItemAdd.Size = new System.Drawing.Size(44, 23);
            this.btnItemAdd.TabIndex = 10;
            this.btnItemAdd.Text = "Add";
            this.btnItemAdd.UseVisualStyleBackColor = true;
            this.btnItemAdd.Click += new System.EventHandler(this.btnItemAdd_Click);
            // 
            // txtItemtypeSearch
            // 
            this.txtItemtypeSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItemtypeSearch.Location = new System.Drawing.Point(495, 38);
            this.txtItemtypeSearch.Name = "txtItemtypeSearch";
            this.txtItemtypeSearch.Size = new System.Drawing.Size(388, 20);
            this.txtItemtypeSearch.TabIndex = 11;
            // 
            // btnItemtypeSearch
            // 
            this.btnItemtypeSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnItemtypeSearch.Location = new System.Drawing.Point(889, 38);
            this.btnItemtypeSearch.Name = "btnItemtypeSearch";
            this.btnItemtypeSearch.Size = new System.Drawing.Size(59, 23);
            this.btnItemtypeSearch.TabIndex = 12;
            this.btnItemtypeSearch.Text = "Search";
            this.btnItemtypeSearch.UseVisualStyleBackColor = true;
            this.btnItemtypeSearch.Click += new System.EventHandler(this.btnItemtypeSearch_Click);
            // 
            // btnExportDAT
            // 
            this.btnExportDAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportDAT.Location = new System.Drawing.Point(789, 7);
            this.btnExportDAT.Name = "btnExportDAT";
            this.btnExportDAT.Size = new System.Drawing.Size(75, 23);
            this.btnExportDAT.TabIndex = 13;
            this.btnExportDAT.Text = "Export .DAT";
            this.btnExportDAT.UseVisualStyleBackColor = true;
            this.btnExportDAT.Click += new System.EventHandler(this.btnExportDAT_Click);
            // 
            // btnExportSQL
            // 
            this.btnExportSQL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportSQL.Location = new System.Drawing.Point(870, 7);
            this.btnExportSQL.Name = "btnExportSQL";
            this.btnExportSQL.Size = new System.Drawing.Size(75, 23);
            this.btnExportSQL.TabIndex = 13;
            this.btnExportSQL.Text = "Export .SQL";
            this.btnExportSQL.UseVisualStyleBackColor = true;
            this.btnExportSQL.Click += new System.EventHandler(this.btnExportSQL_Click);
            // 
            // FrmGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 450);
            this.Controls.Add(this.btnExportSQL);
            this.Controls.Add(this.btnExportDAT);
            this.Controls.Add(this.btnItemtypeSearch);
            this.Controls.Add(this.txtItemtypeSearch);
            this.Controls.Add(this.btnItemAdd);
            this.Controls.Add(this.lbxItemtype);
            this.Controls.Add(this.btnItemDown);
            this.Controls.Add(this.btnItemUp);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnLoadGoodsDb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.txtGoodsFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbxItems);
            this.Controls.Add(this.lbxShops);
            this.Name = "FrmGoods";
            this.Text = "FrmGoods";
            this.Load += new System.EventHandler(this.FrmGoods_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxShops;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtGoodsFile;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnLoadGoodsDb;
        private System.Windows.Forms.ListBox lbxItems;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnItemUp;
        private System.Windows.Forms.Button btnItemDown;
        private System.Windows.Forms.ListBox lbxItemtype;
        private System.Windows.Forms.Button btnItemAdd;
        private System.Windows.Forms.TextBox txtItemtypeSearch;
        private System.Windows.Forms.Button btnItemtypeSearch;
        private System.Windows.Forms.Button btnExportDAT;
        private System.Windows.Forms.Button btnExportSQL;
    }
}