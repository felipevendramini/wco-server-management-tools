﻿using SMT_WConquer_Framework.Database;
using SMT_WConquer_Framework.Database.Entities;
using SMT_WConquer_Framework.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMT_WConquer_Framework.ChildrenForms
{
    public partial class FrmEmoneyShop : Form
    {
        private Dictionary<int, GoodsShopModel> m_dicGoods = new();
        private Dictionary<int, GoodsShopModel.GoodsItemType> m_dicItemtypes = new();
        private Dictionary<int, NpcInfo> m_dicNpcs = new();

        private struct NpcInfo
        {
            public uint Identity { get; set; }
            public string Name { get; set; }
        }

        public FrmEmoneyShop()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog
            {
                AddExtension = false,
#if !DEBUG
                InitialDirectory = Environment.CurrentDirectory,
#else
                InitialDirectory = @"D:\World Conquer\NewConquer\ini",
#endif
                Multiselect = false
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            txtGoodsFile.Text = open.FileName;

            btnItemDown.Enabled = false;
            btnItemUp.Enabled = false;

            m_dicGoods.Clear();
            lbxShops.Items.Clear();
            lbxItems.Items.Clear();

            using StreamReader reader = new StreamReader(open.FileName);
            string line;

            GoodsShopModel goods = null;
            while((line = reader.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line.Trim()))
                    continue;

                if (line.Contains("="))
                {
                    string[] parameters = line.Split('=');
                    if (parameters.Length < 2)
                    {
                        continue;
                    }

                    string paramName = parameters[0];
                    string paramValue = parameters[1];

                    if (paramName.Equals("ID"))
                    {
                        int npcId = int.Parse(paramValue.Trim());
                        var npcInfo = m_dicNpcs.FirstOrDefault(x => x.Key == npcId).Value;
                        goods = new GoodsShopModel
                        {
                            ShopIdentity = npcId,
                            ShopName = npcInfo.Name,
                            Moneytype = GoodsShopModel.MoneyType.ConquerPoints,
                            Items = new List<GoodsShopModel.GoodsItemType>(),
                            Type = 0
                        };

                        if (!m_dicGoods.ContainsKey(goods.ShopIdentity))
                        {
                            m_dicGoods.Add(goods.ShopIdentity, goods);
                            lbxShops.Items.Add($"{goods.ShopIdentity} - {goods.ShopName}");
                        }
                    }
                    else if (paramName.Equals("MoneyType", StringComparison.InvariantCultureIgnoreCase))
                    {
                        goods.Moneytype = (GoodsShopModel.MoneyType)int.Parse(paramValue.Trim());
                    }
                    continue;
                }

                if (goods == null)
                    continue;

                if (line.Trim().Equals("[recommend]", StringComparison.InvariantCultureIgnoreCase))
                    continue;

                string[] itemParams = line.Trim().Split(' ');

                if (!int.TryParse(itemParams[0], out int itemId))
                    continue;

                if (goods.Items.Any(x => x.Type == itemId))
                    continue;

                GoodsShopModel.GoodsItemType goodsItemType;
                if (!m_dicItemtypes.TryGetValue(itemId, out var itemType))
                {
                    goodsItemType = new GoodsShopModel.GoodsItemType 
                    { 
                        Type = itemId,
                        Name = "Invalid"
                    };
                }
                else
                {
                    goodsItemType = new GoodsShopModel.GoodsItemType
                    {
                        Type = itemId,
                        Name = itemType.Name
                    };
                }

                goods.Items.Add(goodsItemType);
            }

        }

        private async void FrmEmoneyShop_Load(object sender, EventArgs e)
        {
            await LoadDataAsync();

            //btnItemtypeSearch_Click(null, null);
        }

        private async Task LoadDataAsync()
        {
            m_dicItemtypes.Clear();
            m_dicNpcs.Clear();

            var itemtypes = await DatabaseContext.SelectAsync("SELECT id, `name` FROM `cq_itemtype` ORDER BY id ASC", null);
            foreach (DataRow row in itemtypes.Rows)
            {
                GoodsShopModel.GoodsItemType item = new GoodsShopModel.GoodsItemType
                {
                    Type = (int)row.Field<uint>("id"),
                    Name = row.Field<string>("name")
                };
                if (!m_dicItemtypes.ContainsKey(item.Type))
                    m_dicItemtypes.Add(item.Type, item);
            }

            await using DatabaseContext ctx = new DatabaseContext(ServerConfiguration.SelectedServerDatabase);
            var dbNpcs = await ctx.SelectAsync("SELECT `id`,`name` FROM cq_npc");
            foreach (DataRow row in dbNpcs.Rows)
            {
                NpcInfo info = new NpcInfo
                {
                    Identity = row.Field<uint>("id"),
                    Name = row.Field<string>("name")
                };

                if (!m_dicNpcs.ContainsKey((int)info.Identity))
                    m_dicNpcs.Add((int)info.Identity, info);
            }
        }

        private void RefreshItemsBox()
        {
            lbxItems.Items.Clear();
            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];
            foreach (var item in shop.Items)
            {
                lbxItems.Items.Add($"{item.Type} - {item.Name}");
            }

            btnItemDown.Enabled = false;
            btnItemUp.Enabled = false;
        }

        private void lbxShops_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxShops.SelectedIndex < 0 || lbxShops.SelectedIndex >= lbxShops.Items.Count)
                return;

            RefreshItemsBox();
        }

        private void btnItemtypeSearch_Click(object sender, EventArgs e)
        {
            lbxItemtype.Items.Clear();

            if (string.IsNullOrEmpty(txtItemtypeSearch.Text.Trim()))
            {
                foreach (var itemtype in m_dicItemtypes.Values)
                {
                    lbxItemtype.Items.Add($"{itemtype.Type} - {itemtype.Name}");
                }

                return;
            }

            foreach (var itemtype in m_dicItemtypes.Values
                .Where(x => x.Type.ToString().ToLowerInvariant().Contains(txtItemtypeSearch.Text.ToLowerInvariant()) ||
                            x.Name.ToLowerInvariant().Contains(txtItemtypeSearch.Text.ToLowerInvariant())))
            {
                lbxItemtype.Items.Add($"{itemtype.Type} - {itemtype.Name}");
            }
        }

        private void btnItemUp_Click(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex <= 0)
                return;

            int currentIdx = lbxItems.SelectedIndex;
            string item = lbxItems.Items[lbxItems.SelectedIndex].ToString();
            int type = int.Parse(item.Split('-')[0].Trim());
            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];

            GoodsShopModel.GoodsItemType itemtype = shop.Items.FirstOrDefault(x => x.Type == type);
            if (itemtype.Type == 0)
                return;

            shop.Items.Remove(itemtype);
            shop.Items.Insert(currentIdx - 1, itemtype);

            RefreshItemsBox();

            lbxItems.SelectedIndex = currentIdx - 1;
        }

        private void btnItemDown_Click(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex >= lbxItems.Items.Count - 1)
                return;

            int currentIdx = lbxItems.SelectedIndex;
            string item = lbxItems.Items[lbxItems.SelectedIndex].ToString();
            int type = int.Parse(item.Split('-')[0].Trim());
            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];

            GoodsShopModel.GoodsItemType itemtype = shop.Items.FirstOrDefault(x => x.Type == type);
            if (itemtype.Type == 0)
                return;

            shop.Items.Remove(itemtype);
            shop.Items.Insert(currentIdx + 1, itemtype);

            RefreshItemsBox();

            lbxItems.SelectedIndex = currentIdx + 1;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex < 0 || lbxItems.SelectedIndex >= lbxItems.Items.Count)
                return;

            var shop = m_dicGoods[int.Parse(lbxShops.Items[lbxShops.SelectedIndex].ToString().Split('-')[0].Trim())];
            int type = int.Parse(lbxItems.Items[lbxItems.SelectedIndex].ToString().Split('-')[0].Trim());
            shop.Items.RemoveAll(x => x.Type == type);
            lbxItems.Items.RemoveAt(lbxItems.SelectedIndex);
        }

        private void lbxItems_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                btnRemove_Click(sender, e);
        }

        private void btnItemAdd_Click(object sender, EventArgs e)
        {

        }

        private void lbxItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxItems.SelectedIndex < 0 || lbxItems.SelectedIndex >= lbxItems.Items.Count)
            {
                btnItemDown.Enabled = false;
                btnItemUp.Enabled = false;
            }
            else if (lbxItems.SelectedIndex == 0)
            {
                btnItemDown.Enabled = true;
                btnItemUp.Enabled = false;
            }
            else if (lbxItems.SelectedIndex == lbxItems.Items.Count - 1)
            {
                btnItemDown.Enabled = false;
                btnItemUp.Enabled = true;
            }
            else
            {
                btnItemDown.Enabled = true;
                btnItemUp.Enabled = true;
            }
        }

        private void btnExportSQL_Click(object sender, EventArgs e)
        {
            var open = new SaveFileDialog
            {
                AddExtension = false,
                InitialDirectory = Environment.CurrentDirectory,
                DefaultExt = "sql"
            };

            if (open.ShowDialog(this) != DialogResult.OK)
                return;

            using StreamWriter writer = new StreamWriter(open.OpenFile());

            try
            {
                foreach (var shop in m_dicGoods.Values)
                {
                    foreach (GoodsShopModel.GoodsItemType item in shop.Items)
                    {
                        writer.WriteLine($"INSERT INTO `cq_goods` (`ownerid`,`itemtype`,`moneytype`,`monopoly`) VALUES ({shop.ShopIdentity}, {item.Type}, {(int)shop.Moneytype}, 0);");
                    }

                    writer.WriteLine("");
                }
            }
            finally
            {
                writer.Flush();
                writer.Close();
            }
        }
    }
}
